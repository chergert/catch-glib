/* catch-log.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef __linux__
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/syscall.h>
#endif /* __linux__ */

#include <glib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "catch-glib/catch-debug.h"

static GPtrArray *channels     = NULL;
static gchar      hostname[64] = "";
static GLogFunc   last_handler = NULL;

G_LOCK_DEFINE(channels_lock);

/**
 * catch_log_get_thread:
 *
 * Retrieves task id for the current thread. This is only supported on Linux.
 * On other platforms, the current current thread pointer is retrieved.
 *
 * Returns: The task id.
 */
static inline gint
catch_log_get_thread (void)
{
#ifdef __linux__
   return (gint)syscall(SYS_gettid);
#else
   return GPOINTER_TO_INT(g_thread_self());
#endif /* __linux__ */
}

/**
 * catch_log_level_str:
 * @log_level: A #GLogLevelFlags.
 *
 * Retrieves the log level as a string.
 *
 * Returns: A string which shouldn't be modified or freed.
 * Side effects: None.
 */
static inline const gchar *
catch_log_level_str (GLogLevelFlags log_level)
{
   #define CASE_LEVEL_STR(_l) case G_LOG_LEVEL_##_l: return #_l
   switch ((long)log_level) {
   CASE_LEVEL_STR(ERROR);
   CASE_LEVEL_STR(CRITICAL);
   CASE_LEVEL_STR(WARNING);
   CASE_LEVEL_STR(MESSAGE);
   CASE_LEVEL_STR(INFO);
   CASE_LEVEL_STR(DEBUG);
   CASE_LEVEL_STR(TRACE);
   default:
      return "UNKNOWN";
   }
   #undef CASE_LEVEL_STR
}

/**
 * catch_log_write_to_channel:
 * @channel: A #GIOChannel.
 * @message: A string log message.
 *
 * Writes @message to @channel and flushes the channel.
 */
static void
catch_log_write_to_channel (GIOChannel  *channel,
                            const gchar *message)
{
   g_io_channel_write_chars(channel, message, -1, NULL, NULL);
   g_io_channel_flush(channel, NULL);
}

/**
 * catch_log_handler:
 * @log_domain: A string containing the log section.
 * @log_level: A #GLogLevelFlags.
 * @message: The string message.
 * @user_data: User data supplied to g_log_set_default_handler().
 *
 * Default log handler that will dispatch log messages to configured logging
 * destinations.
 */
static void
catch_log_handler (const gchar    *log_domain,
                   GLogLevelFlags  log_level,
                   const gchar    *message,
                   gpointer        user_data)
{
   struct tm tt;
   time_t t;
   const gchar *level;
   gchar ftime[32];
   gchar *buffer;
   guint msec;

   if (G_LIKELY(channels->len)) {
      level = catch_log_level_str(log_level);
#ifdef __linux__
      {
         struct timespec ts;
         clock_gettime(CLOCK_REALTIME, &ts);
         t = (time_t)ts.tv_sec;
         msec = ts.tv_nsec / 100000;
      }
#else
      {
         struct timeval tv;
         gettimeofday(&tv, NULL);
         t = tv.tv_sec;
         msec = tv.tv_usec / 1000;
      }
#endif
      tt = *localtime(&t);
      strftime(ftime, sizeof(ftime), "%Y/%m/%d %H:%M:%S", &tt);
      buffer = g_strdup_printf("%s.%04u  %s: %12s[%d]: %8s: %s\n",
                               ftime, msec,
                               hostname, log_domain,
                               catch_log_get_thread(), level, message);
      G_LOCK(channels_lock);
      g_ptr_array_foreach(channels, (GFunc)catch_log_write_to_channel, buffer);
      G_UNLOCK(channels_lock);
      g_free(buffer);
   }
}

/**
 * catch_log_init:
 * @stdout_: Indicates logging should be written to stdout.
 * @filename: An optional file in which to store logs.
 *
 * Initializes the logging subsystem.
 */
void
catch_log_init (gboolean     stdout_,
                const gchar *filename)
{
   static gsize initialized = FALSE;
   GIOChannel *channel;

   if (g_once_init_enter(&initialized)) {
      channels = g_ptr_array_new();
      if (filename) {
         channel = g_io_channel_new_file(filename, "a", NULL);
         g_ptr_array_add(channels, channel);
      }
      if (stdout_) {
         channel = g_io_channel_unix_new(STDOUT_FILENO);
         g_ptr_array_add(channels, channel);
      }

#ifdef __linux__
      {
         struct utsname u;
         uname(&u);
         memcpy(hostname, u.nodename, sizeof(hostname));
      }
#else
#ifdef __APPLE__
      gethostname(hostname, sizeof (hostname));
#else
#error "Target platform not supported"
#endif /* __APPLE__ */
#endif /* __linux__ */

      g_log_set_default_handler(catch_log_handler, NULL);
      g_once_init_leave(&initialized, TRUE);
   }
}

/**
 * catch_log_shutdown:
 *
 * Cleans up after the logging subsystem.
 */
void
catch_log_shutdown (void)
{
   if (last_handler) {
      g_log_set_default_handler(last_handler, NULL);
      last_handler = NULL;
   }
}
