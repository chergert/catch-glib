/* catch-list-objects.c
 *
 * Copyright (C) 2011 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <catch-glib/catch-glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <stdlib.h>

static CatchSession *gSession;
static GMainLoop    *gMainLoop;
static gchar        *gBearerToken;
static gboolean      gInsecure;
static gchar        *gServer = (gchar *)"api.catch.com";
static gint          gPort;
static gchar        *gSpace;
static gchar        *gModifiedSince;
static gchar        *gQuery;

static GOptionEntry entries[] = {
   { "bearer-token", 'b', 0, G_OPTION_ARG_STRING, &gBearerToken, "The OAuth2 bearer token." },
   { "server", 's', 0, G_OPTION_ARG_STRING, &gServer, "Connect to SERVER", "SERVER" },
   { "space", 'm', 0, G_OPTION_ARG_STRING, &gSpace, "List objects in SPACE", "SPACE" },
   { "port", 'p', 0, G_OPTION_ARG_INT, &gPort, "Connect to PORT", "PORT" },
   { "http", 'h', 0, G_OPTION_ARG_NONE, &gInsecure, "Use HTTP instead of HTTPS" },
   { "modified-since", 'm', 0, G_OPTION_ARG_STRING, &gModifiedSince, "Only show objects with server_modified_at >= SMA", "SMA" },
   { "query", 'q', 0, G_OPTION_ARG_STRING, &gQuery, "Search for notes matching QUERY.", "QUERY" },
   { NULL }
};

static void
fetch_objects_cb (GObject      *group,
                  GAsyncResult *result,
                  gpointer      user_data)
{
   CatchResourceGroup *objects = (CatchResourceGroup *)group;
   CatchResource *object;
   const gchar *typestr;
   GDateTime *created_at;
   GDateTime *modified_at;
   GError *error = NULL;
   GType type;
   gchar *id;
   gchar *text;
   gchar *server_modified_at;
   gchar *created_str;
   gchar *modified_str;
   guint length;
   guint i;

   if (!catch_resource_group_fetch_finish(objects, result, &error)) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      g_main_loop_quit(gMainLoop);
      return;
   }

   g_print(" %-23s | %-17s | %-17s | %-13s | %-10s | %-15s\n",
           "Remote ID", "Created At", "Modified At",
           "Version", "Type", "Text");
   g_print("------------------------------------"
           "------------------------------------"
           "------------------------------------\n");

   /*
    * TODO: This should all be async, force load the entire range.
    */

   length = catch_resource_group_get_count(objects);
   for (i = 0; i < length; i++) {
      object = catch_resource_group_get_resource(objects, i);
      g_object_get(object,
                   "created-at", &created_at,
                   "modified-at", &modified_at,
                   "remote-id", &id,
                   "server_modified_at", &server_modified_at,
                   "text", &text,
                   NULL);
      if (text) {
         g_strdelimit(text, "\n", ' ');
         if (strlen(text) > 30) {
            text[30] = '\0';
         }
      }
      type = G_TYPE_FROM_INSTANCE(object);
      if (type == CATCH_TYPE_NOTE) {
         typestr = "note";
      } else if (type == CATCH_TYPE_COMMENT) {
         typestr = "comment";
      } else if (type == CATCH_TYPE_IMAGE) {
         typestr = "image";
      } else if (type == CATCH_TYPE_ATTACHMENT) {
         typestr = "attachment";
      } else if (type == CATCH_TYPE_AUDIO) {
         typestr = "audio";
      } else {
         typestr = "unknown";
      }
      created_str = g_date_time_format(created_at, "%x %X");
      modified_str = g_date_time_format(modified_at, "%x %X");
      g_printf("%-24s | %-17s | %-17s | %-13s | %-10s | %s\n",
               id, created_str, modified_str,
               server_modified_at, typestr, text);
      g_free(id);
      g_free(text);
      g_free(server_modified_at);
      g_free(created_str);
      g_free(modified_str);
   }

   g_object_unref(objects);
   g_main_loop_quit(gMainLoop);
}

static void
get_objects_cb (GObject      *session_obj,
                GAsyncResult *result,
                gpointer      user_data)
{
   CatchResourceGroup *group;
   CatchSession *session = (CatchSession *)session_obj;
   GError *error = NULL;
   guint count;

   if (!(group = catch_session_get_objects_finish(session, result, &error))) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      g_main_loop_quit(gMainLoop);
      return;
   }

   count = catch_resource_group_get_count(group);
   catch_resource_group_fetch_async(group, 0, count, fetch_objects_cb, NULL);
}

gint
main (gint   argc,
      gchar *argv[])
{
   GOptionContext *context;
   GomFilter *filter;
   GError *error = NULL;

   context = g_option_context_new(_("- List a users objects from catch."));
   g_option_context_add_main_entries(context, entries, PACKAGE);
   if (!g_option_context_parse(context, &argc, &argv, &error)) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      return EXIT_FAILURE;
   }

   g_type_init();

   gMainLoop = g_main_loop_new(NULL, FALSE);
   gSession = g_object_new(CATCH_TYPE_SESSION,
                           "bearer-token", gBearerToken,
                           "port", gPort,
                           "secure", !gInsecure,
                           "server", gServer,
                           NULL);

#if 0
   filter = g_object_new(CATCH_TYPE_RESOURCE_FILTER, NULL);

   if (gSpace) {
      gchar *objects[] = { gSpace, NULL };
      catch_resource_filter_set_objects(filter, objects);
   }

   if (gModifiedSince) {
      catch_resource_filter_set_server_modified_at(filter, gModifiedSince);
   }

   if (gQuery) {
      catch_resource_filter_set_search_query(filter, gQuery);
   }
#else
   filter = NULL;
#endif

   catch_session_get_objects_async(gSession, filter, get_objects_cb, NULL);

   g_main_loop_run(gMainLoop);
   g_main_loop_unref(gMainLoop);

   return EXIT_SUCCESS;
}
