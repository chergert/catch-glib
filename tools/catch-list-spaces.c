/* catch-list-spaces.c
 *
 * Copyright (C) 2011 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include "config.h"
#endif

#include <catch-glib/catch-glib.h>
#include <glib/gi18n.h>
#include <glib/gprintf.h>
#include <stdlib.h>

static CatchSession *gSession;
static GMainLoop    *gMainLoop;
static gchar        *gBearerToken;
static gboolean      gInsecure;
static gchar        *gServer = (gchar *)"api.catch.com";
static gint          gPort = 0;
static gchar        *gQuery;

static GOptionEntry entries[] = {
   { "bearer-token", 'b', 0, G_OPTION_ARG_STRING, &gBearerToken, "The OAuth2 bearer token." },
   { "server", 's', 0, G_OPTION_ARG_STRING, &gServer },
   { "port", 'p', 0, G_OPTION_ARG_INT, &gPort },
   { "http", 'h', 0, G_OPTION_ARG_NONE, &gInsecure, "Use HTTP instead of HTTPS." },
   { "query", 'q', 0, G_OPTION_ARG_STRING, &gQuery, "A search term to query" },
   { NULL }
};

static void
get_spaces_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
   CatchResourceGroup *spaces;
   CatchResource *space;
   CatchSession *session = (CatchSession *)object;
   GDateTime *created_at;
   GDateTime *modified_at;
   GError *error = NULL;
   gchar *id;
   gchar *name;
   gchar *server_modified_at;
   gchar *created_str;
   gchar *modified_str;
   guint length;
   guint i;

   g_return_if_fail(CATCH_IS_SESSION(session));

   if (!(spaces = catch_session_get_spaces_finish(session, result, &error))) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      g_main_loop_quit(gMainLoop);
      return;
   }

   if (!catch_resource_group_get_count(spaces)) {
      g_print("No spaces found.\n");
      g_main_loop_quit(gMainLoop);
      return;
   }

   g_print(" %-24s | %-17s | %-17s | %-13s | %-20s\n",
           "Remote ID", "Created At", "Modified At",
           "Version", "Name");
   g_print("-------------------------------"
           "-------------------------------"
           "-------------------------------\n");

   /*
    * TODO: This should all be async, force load the entire range.
    */

   length = catch_resource_group_get_count(spaces);
   for (i = 0; i < length; i++) {
      space = catch_resource_group_get_resource(spaces, i);
      g_object_get(space,
                   "created-at", &created_at,
                   "modified-at", &modified_at,
                   "remote-id", &id,
                   "server_modified_at", &server_modified_at,
                   "name", &name,
                   NULL);
      created_str = g_date_time_format(created_at, "%x %X");
      modified_str = g_date_time_format(modified_at, "%x %X");
      g_printf(" %-24s | %-17s | %-17s | %-13s | %s\n",
               id, created_str, modified_str, server_modified_at, name);
      g_free(id);
      g_free(name);
      g_free(server_modified_at);
      g_free(created_str);
      g_free(modified_str);
   }

   g_object_unref(spaces);
   g_main_loop_quit(gMainLoop);
}

gint
main (gint   argc,
      gchar *argv[])
{
   GOptionContext *context;
   GomFilter *filter;
   GError *error = NULL;

   context = g_option_context_new(_("- Catch, List a users spaces."));
   g_option_context_add_main_entries(context, entries, PACKAGE);
   if (!g_option_context_parse(context, &argc, &argv, &error)) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      return EXIT_FAILURE;
   }

   g_type_init();

   gMainLoop = g_main_loop_new(NULL, FALSE);
   gSession = g_object_new(CATCH_TYPE_SESSION,
                           "bearer-token", gBearerToken,
                           "port", gPort,
                           "secure", !gInsecure,
                           "server", gServer,
                           NULL);

#if 0
   filter = gom_filter_new_eq();

   if (gQuery) {
      catch_resource_filter_set_search_query(filter, gQuery);
   }
#else
   filter = NULL;
#endif

   catch_session_get_spaces_async(gSession, filter, get_spaces_cb, NULL);

   g_main_loop_run(gMainLoop);
   g_main_loop_unref(gMainLoop);

   return EXIT_SUCCESS;
}
