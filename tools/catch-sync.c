/* catch-sync.c
 *
 * Copyright (C) 2011 Catch.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <catch-glib/catch-glib.h>
#include <glib/gi18n.h>
#include <stdlib.h>

#include "catch-log.h"

static GMainLoop    *gMainLoop;
static CatchSession *gSession;
static gchar        *gBearerToken;
static gchar        *gDataDir;
static gchar        *gServer = (gchar *)"api.catch.com";
static gint          gPort;
static gboolean      gHttp;
static gint          gExitCode = EXIT_SUCCESS;

static GOptionEntry entries[] = {
   { "bearer-token", 'b', 0, G_OPTION_ARG_STRING, &gBearerToken, "OAuth2 Bearer Token" },
   { "data-dir", 'd', 0, G_OPTION_ARG_FILENAME, &gDataDir, "The directory containing the SQLite database." },
   { "server", 's', 0, G_OPTION_ARG_STRING, &gServer, NULL },
   { "port", 'p', 0, G_OPTION_ARG_INT, &gPort, NULL },
   { "http", 'h', 0, G_OPTION_ARG_NONE, &gHttp, NULL },
   { NULL }
};

static void
sync_cb (GObject      *object,
         GAsyncResult *result,
         gpointer      user_data)
{
   CatchSession *session = (CatchSession *)object;
   GError *error = NULL;

   g_assert(CATCH_IS_SESSION(session));

   if (!catch_session_sync_finish(session, result, &error)) {
      g_printerr("Failed.\nReason: %s\n", error->message);
      g_error_free(error);
   } else {
      g_print("Success.\n");
   }

   g_main_loop_quit(gMainLoop);
}

static void
load_cb (GObject      *object,
         GAsyncResult *result,
         gpointer      user_data)
{
   CatchSession *session = (CatchSession *)object;
   GError *error = NULL;

   g_return_if_fail(CATCH_IS_SESSION(session));

   if (!catch_session_set_data_dir_finish(session, result, &error)) {
      g_printerr("Failed.\nReason: %s\n", error->message);
      g_error_free(error);
      g_main_loop_quit(gMainLoop);
      gExitCode = 255;
      return;
   }

   catch_session_sync_async(gSession, sync_cb, NULL);
}


gint
main (gint   argc,
      gchar *argv[])
{
   GOptionContext *context;
   GError *error = NULL;

   context = g_option_context_new(_("- Synchronize your catch.com notes."));
   g_option_context_add_main_entries(context, entries, NULL);
   if (!g_option_context_parse(context, &argc, &argv, &error)) {
      g_printerr("%s\n", error->message);
      g_error_free(error);
      return EXIT_FAILURE;
   }

   if (!gBearerToken) {
      g_printerr("Please provide an OAuth2 bearer token with -b.\n");
      return EXIT_FAILURE;
   }

   if (!gDataDir) {
      /*
       * TODO: Discover for active user.
       */
      g_printerr("Please provide the data-dir with -d.\n");
      return EXIT_FAILURE;
   }

   catch_log_init(TRUE, NULL);

   g_type_init();

   gMainLoop = g_main_loop_new(NULL, FALSE);
   gSession = g_object_new(CATCH_TYPE_SESSION,
                           "bearer-token", gBearerToken,
                           "port", gPort,
                           "secure", !gHttp,
                           "server", gServer,
                           NULL);
   catch_session_set_data_dir_async(gSession, gDataDir, load_cb, NULL);

   g_main_loop_run(gMainLoop);

   g_clear_object(&gSession);
   g_main_loop_unref(gMainLoop);
   g_free(gBearerToken);
   g_free(gDataDir);

   return gExitCode;
}
