/* catch-comment.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_COMMENT_H
#define CATCH_COMMENT_H

#include "catch-object.h"

G_BEGIN_DECLS

#define CATCH_TYPE_COMMENT            (catch_comment_get_type())
#define CATCH_COMMENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_COMMENT, CatchComment))
#define CATCH_COMMENT_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_COMMENT, CatchComment const))
#define CATCH_COMMENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_COMMENT, CatchCommentClass))
#define CATCH_IS_COMMENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_COMMENT))
#define CATCH_IS_COMMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_COMMENT))
#define CATCH_COMMENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_COMMENT, CatchCommentClass))

typedef struct _CatchComment        CatchComment;
typedef struct _CatchCommentClass   CatchCommentClass;
typedef struct _CatchCommentPrivate CatchCommentPrivate;

struct _CatchComment
{
   CatchObject parent;

   /*< private >*/
   CatchCommentPrivate *priv;
};

struct _CatchCommentClass
{
   CatchObjectClass parent_class;
};

GType catch_comment_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CATCH_COMMENT_H */
