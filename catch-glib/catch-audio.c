/* catch-audio.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-audio.h"

G_DEFINE_TYPE(CatchAudio, catch_audio, CATCH_TYPE_ATTACHMENT)

/**
 * SECTION:catch-audio
 * @title: CatchAudio
 * @short_description: Represents an audio attachment.
 *
 * #CatchAudio represents an audio file that has been attached to
 * a note. #CatchAttachment provides some common utilities to work with
 * file attachments that may be useful.
 *
 * If the duration of the audio attachment is known, the "duration"
 * property will be set.
 */

struct _CatchAudioPrivate
{
   gdouble duration;
};

enum
{
   PROP_0,
   PROP_DURATION,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_audio_get_duration:
 * @audio: (in): A #CatchAudio.
 *
 * Fetches the duration of the #CatchAudio if it is known. If the duration
 * is not known, then zero is returned.
 *
 * Returns: The duration in seconds.
 */
gdouble
catch_audio_get_duration (CatchAudio *audio)
{
   g_return_val_if_fail(CATCH_IS_AUDIO(audio), 0.0);
   return audio->priv->duration;
}

/**
 * catch_audio_set_duration:
 * @audio: (in): A #CatchAudio.
 * @duration: (in): The duration of the audio in seconds.
 *
 * Sets the duration of @audio in seconds.
 */
void
catch_audio_set_duration (CatchAudio *audio,
                          gdouble     duration)
{
   g_return_if_fail(CATCH_IS_AUDIO(audio));

   audio->priv->duration = duration;
   g_object_notify_by_pspec(G_OBJECT(audio), gParamSpecs[PROP_DURATION]);
}

/**
 * catch_audio_finalize:
 * @object: (in): A #CatchAudio.
 *
 * Finalizer for a #CatchAudio instance.  Frees any resources held by
 * the instance.
 */
static void
catch_audio_finalize (GObject *object)
{
   G_OBJECT_CLASS(catch_audio_parent_class)->finalize(object);
}

/**
 * catch_audio_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
catch_audio_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
   CatchAudio *audio = CATCH_AUDIO(object);

   switch (prop_id) {
   case PROP_DURATION:
      g_value_set_double(value, catch_audio_get_duration(audio));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_audio_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
catch_audio_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
   CatchAudio *audio = CATCH_AUDIO(object);

   switch (prop_id) {
   case PROP_DURATION:
      catch_audio_set_duration(audio, g_value_get_double(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_audio_class_init:
 * @klass: (in): A #CatchAudioClass.
 *
 * Initializes the #CatchAudioClass and prepares the vtable.
 */
static void
catch_audio_class_init (CatchAudioClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_audio_finalize;
   object_class->get_property = catch_audio_get_property;
   object_class->set_property = catch_audio_set_property;
   g_type_class_add_private(object_class, sizeof(CatchAudioPrivate));

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "audio");

   /**
    * CatchAudio:duration:
    *
    * The "duration" of the #CatchAudio. This may be zero if the duration
    * is not yet known.
    */
   gParamSpecs[PROP_DURATION] =
      g_param_spec_double("duration",
                          _("Duration"),
                          _("The duration of the audio."),
                          0,
                          G_MAXDOUBLE,
                          0,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_DURATION,
                                   gParamSpecs[PROP_DURATION]);
}

/**
 * catch_audio_init:
 * @audio: (in): A #CatchAudio.
 *
 * Initializes the newly created #CatchAudio instance.
 */
static void
catch_audio_init (CatchAudio *audio)
{
   audio->priv = G_TYPE_INSTANCE_GET_PRIVATE(audio,
                                             CATCH_TYPE_AUDIO,
                                             CatchAudioPrivate);
}
