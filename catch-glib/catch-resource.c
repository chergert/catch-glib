/* catch-resource.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-resource.h"

G_DEFINE_TYPE(CatchResource, catch_resource, GOM_TYPE_RESOURCE)

/**
 * SECTION:catch-resource
 * @title: CatchResource
 * @short_description: Base object for all remote objects.
 *
 * #CatchResource is a base object that is used for objects that may
 * live on the remote Catch hosted service. The resource contains
 * the common properties that are used throughout the service.
 *
 * For example, remote objects contain the following:
 *
 *   "created-at" is the time the object was created by the client.
 *   "created-by" is the id of the user who created the object.
 *   "created-by-name" is the name of the user who created the object.
 *   "deleted" is if the object has been deleted locally.
 *   "local-id" is the 64-bit id used to address the object locally.
 *   "modified-at" is the time the object was last modified anywhere.
 *   "remote-id" is the id of the object on the hosted service.
 *   "server-deleted-at" is a token of when the object was deleted.
 *   "server-modified-at" is a token of when the object was modified.
 *   "pending" are the remote operations pending on an object.
 *
 * #CatchSpace and #CatchObject build on #CatchResource to provide
 * the semantics for spaces and notes.
 */

struct _CatchResourcePrivate
{
   GDateTime    *created_at;
   gchar        *created_by;
   gchar        *created_by_name;
   gboolean      deleted;
   gint64        local_id;
   GDateTime    *modified_at;
   gchar        *remote_id;
   gchar        *server_deleted_at;
   gchar        *server_modified_at;
   CatchPending  pending;
};

enum
{
   PROP_0,
   PROP_CREATED_AT,
   PROP_CREATED_BY,
   PROP_CREATED_BY_NAME,
   PROP_DELETED,
   PROP_LOCAL_ID,
   PROP_MODIFIED_AT,
   PROP_PENDING,
   PROP_REMOTE_ID,
   PROP_SERVER_DELETED_AT,
   PROP_SERVER_MODIFIED_AT,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_resource_get_created_at:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the time that @resource was created by the client.
 *
 * Returns: (transfer none): A #GDateTime.
 */
GDateTime *
catch_resource_get_created_at (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->created_at;
}

/**
 * catch_resource_set_created_at:
 * @resource: (in): A #CatchResource.
 * @created_at: (in): A #GDateTime.
 *
 * Sets the "created-at" property for @resource.
 */
void
catch_resource_set_created_at (CatchResource *resource,
                               GDateTime     *created_at)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_return_if_fail(created_at != NULL);

   if (resource->priv->created_at) {
      g_date_time_unref(resource->priv->created_at);
   }
   resource->priv->created_at = g_date_time_ref(created_at);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_CREATED_AT]);
}

/**
 * catch_resource_get_created_by:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "created-by" property of @resource, the id of the user
 * who created the resource.
 *
 * Returns: A string.
 */
const gchar *
catch_resource_get_created_by (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->created_by;
}

/**
 * catch_resource_set_created_by:
 * @resource: (in): A #CatchResource.
 * @created_by: (in): The owner of the resource.
 *
 * Sets the "created-by" property of @resource. This is the id of the
 * user who created @resource.
 */
void
catch_resource_set_created_by (CatchResource *resource,
                               const gchar   *created_by)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_free(resource->priv->created_by);
   resource->priv->created_by = g_strdup(created_by);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_CREATED_BY]);
}

/**
 * catch_resource_get_created_by_name:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "created-by-name" property. This is the human readable name
 * of resources creator.
 *
 * Returns: A string.
 */
const gchar *
catch_resource_get_created_by_name (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->created_by_name;
}

/**
 * catch_resource_set_created_by_name:
 * @resource: (in): A #CatchResource.
 * @created_by_name: (in): The name of the creator.
 *
 * Sets the name of the user who created @resource.
 */
void
catch_resource_set_created_by_name (CatchResource *resource,
                                    const gchar   *created_by_name)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_free(resource->priv->created_by_name);
   resource->priv->created_by_name = g_strdup(created_by_name);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_CREATED_BY_NAME]);
}

/**
 * catch_resource_get_local_id:
 * @resource: (in): A #CatchResource.
 *
 * Sets the "local-id" property. This is the identifier to locate @resource
 * in the local database. If @resource has not been stored to the local
 * database, this will be -1.
 *
 * Returns: None.
 * Side effects: None.
 */
gint64
catch_resource_get_local_id (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), 0);
   return resource->priv->local_id;
}

/**
 * catch_resource_set_local_id:
 * @resource: (in): A #CatchResource.
 * @local_id: (in): The local identifier for the resource.
 *
 * Sets the local identifier for the resource. This should be the id of
 * the row in the database that can be used to load the resource.
 */
void
catch_resource_set_local_id (CatchResource *resource,
                             gint64         local_id)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_return_if_fail(local_id >= -1);
   g_return_if_fail(local_id <= G_MAXINT64);

   resource->priv->local_id = local_id;
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_LOCAL_ID]);
}

/**
 * catch_resource_get_modified_at:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "modified-at" property of @resource. This is the time the
 * resource was last modified.
 *
 * Returns: (transfer none): A #GDateTime.
 */
GDateTime *
catch_resource_get_modified_at (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->modified_at;
}

/**
 * catch_resource_set_modified_at:
 * @resource: (in): A #CatchResource.
 * @modified_at: (in): A #GDateTime.
 *
 * Sets the "modified-at" property for @resource.
 */
void
catch_resource_set_modified_at (CatchResource *resource,
                                GDateTime     *modified_at)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_return_if_fail(modified_at != NULL);

   if (resource->priv->modified_at) {
      g_date_time_unref(resource->priv->modified_at);
   }
   resource->priv->modified_at = g_date_time_ref(modified_at);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_MODIFIED_AT]);
}

/**
 * catch_resource_get_pending:
 * @resource: (in): A #CatchResource.
 *
 * Get the "pending" property of @resource. This is a #GFlags representing
 * the sync operations that are pending to be completed on the resource.
 *
 * Returns: A #CatchPending.
 */
CatchPending
catch_resource_get_pending (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), 0);
   return resource->priv->pending;
}

/**
 * catch_resource_set_pending:
 * @resource: (in): A #CatchResource.
 *
 * Set the "pending" property. This is a #GFlags indicating the sync
 * operations that need to be performed.
 */
void
catch_resource_set_pending (CatchResource *resource,
                            CatchPending   pending)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   resource->priv->pending = pending;
   g_object_notify_by_pspec(G_OBJECT(resource), gParamSpecs[PROP_PENDING]);
}

/**
 * catch_resource_get_remote_id:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "remote-id" property. This is the id used to locate the
 * resource on the Catch hosted service.
 *
 * Returns: A string containing the remote id, or %NULL.
 */
const gchar *
catch_resource_get_remote_id (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->remote_id;
}

/**
 * catch_resource_set_remote_id:
 * @resource: (in): A #CatchResource.
 * @remote_id: (in): A string.
 *
 * Sets the "remote-id" property. The "remote-id" is used to locate the
 * resource on the Catch hosted service.
 */
void
catch_resource_set_remote_id (CatchResource *resource,
                               const gchar  *remote_id)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_free(resource->priv->remote_id);
   resource->priv->remote_id = g_strdup(remote_id);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_REMOTE_ID]);
}

/**
 * catch_resource_get_server_modified_at:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "server-modified-at" property. This is an opaque token used
 * to track the version of the resource. It is used to prevent collisions
 * when synchronizing the local database with the Catch hosted service.
 *
 * Returns: A string containing the "server-modified-at".
 */
const gchar *
catch_resource_get_server_modified_at (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->server_modified_at;
}

/**
 * catch_resource_set_server_modified_at:
 * @resource: (in): A #CatchResource.
 * @server_modified_at: (in): The "server-modified-at" string.
 *
 * Sets the "server-modified-at" property. See
 * catch_resource_get_server_modified_at() for more information on this
 * property.
 */
void
catch_resource_set_server_modified_at (CatchResource *resource,
                                       const gchar  *server_modified_at)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   g_free(resource->priv->server_modified_at);
   resource->priv->server_modified_at = g_strdup(server_modified_at);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_SERVER_MODIFIED_AT]);
}

/**
 * catch_resource_get_server_deleted_at:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "server-deleted-at" property of @resource. This is an opaque
 * token representing the version of the resource that has been deleted by
 * the Catch hosted service.
 *
 * Returns: A string containing the "server-deleted-at".
 */
const gchar *
catch_resource_get_server_deleted_at (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), NULL);
   return resource->priv->server_deleted_at;
}

/**
 * catch_resource_set_server_deleted_at:
 * @resource: (in): A #CatchResource.
 * @server_deleted_at: (in): A string.
 *
 * Sets the "server-deleted-at" property of @resource. See
 * catch_resource_get_server_deleted_at() for more information.
 */
void
catch_resource_set_server_deleted_at (CatchResource *resource,
                                      const gchar   *server_deleted_at)
{
   CatchResourcePrivate *priv;

   g_return_if_fail(CATCH_IS_RESOURCE(resource));

   priv = resource->priv;

   /*
    * Update the server-deleted-at field.
    */
   g_free(priv->server_deleted_at);
   priv->server_deleted_at = g_strdup(server_deleted_at);
   g_object_notify_by_pspec(G_OBJECT(resource),
                            gParamSpecs[PROP_SERVER_DELETED_AT]);

   /*
    * Mark the resource as deleted if server_deleted_at is non-zero.
    */
   if (!!g_strcmp0(server_deleted_at, "0") && !priv->deleted) {
      priv->deleted = TRUE;
      g_object_notify_by_pspec(G_OBJECT(resource), gParamSpecs[PROP_DELETED]);
   }
}

/**
 * catch_resource_get_deleted:
 * @resource: (in): A #CatchResource.
 *
 * Fetches the "deleted" property. This will be %TRUE if the resource has been
 * marked for deletion or has been deleted by the Catch hosted service.
 *
 * Returns: %TRUE if @resource has been deleted; otherwise %FALSE.
 */
gboolean
catch_resource_get_deleted (CatchResource *resource)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), FALSE);
   return resource->priv->deleted;
}

/**
 * catch_resource_set_deleted:
 * @resource: (in): A #CatchResource.
 * @deleted: (in): If the resource has been deleted.
 *
 * Sets the "deleted" property of @resource. See catch_resource_get_deleted()
 * for more information.
 */
void
catch_resource_set_deleted (CatchResource *resource,
                            gboolean       deleted)
{
   g_return_if_fail(CATCH_IS_RESOURCE(resource));
   resource->priv->deleted = deleted;
   g_object_notify_by_pspec(G_OBJECT(resource), gParamSpecs[PROP_DELETED]);
}

/**
 * catch_resource_load_from_json:
 * @resource: (in): A #CatchResource.
 * @node: (in): A #JsonNode.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Loads @resource using the resource described in @node. The properties
 * of the resource are parsed from fields in @node.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
catch_resource_load_from_json (CatchResource  *resource,
                               JsonNode       *node,
                               GError        **error)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), FALSE);
   g_return_val_if_fail(node != NULL, FALSE);
   g_return_val_if_fail(JSON_NODE_HOLDS_OBJECT(node), FALSE);

   return CATCH_RESOURCE_GET_CLASS(resource)->
      load_from_json(resource, node, error);
}

static gboolean
catch_resource_real_load_from_json (CatchResource  *resource,
                                    JsonNode       *node,
                                    GError        **error)
{
   const gchar *id = NULL;
   const gchar *server_modified_at = NULL;
   const gchar *server_deleted_at = NULL;
   const gchar *rfc3339;
   JsonObject *obj;
   GDateTime *dt;
   GTimeVal tv;

   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), FALSE);
   g_return_val_if_fail(node, FALSE);

   if (!JSON_NODE_HOLDS_OBJECT(node) ||
       !(obj = json_node_get_object(node)) ||
       !JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "id")) ||
       !json_object_has_member(obj, "id") ||
       !(id = json_object_get_string_member(obj, "id")) ||
       !json_object_has_member(obj, "server_modified_at") ||
       !JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "server_modified_at")) ||
       !(server_modified_at = json_object_get_string_member(obj, "server_modified_at"))) {
      g_set_error(error, CATCH_RESOURCE_ERROR,
                  CATCH_RESOURCE_ERROR_INVALID_JSON,
                  _("The JSON object provided is not valid."));
      return FALSE;
   }

   /*
    * Try to extract "created_at", "modified_at", and "server_deleted_at".
    */
   if (json_object_has_member(obj, "created_at") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "created_at"))) {
      rfc3339 = json_object_get_string_member(obj, "created_at");
      g_time_val_from_iso8601(rfc3339, &tv);
      dt = g_date_time_new_from_timeval_utc(&tv);
      catch_resource_set_created_at(resource, dt);
      g_date_time_unref(dt);
   }
   if (json_object_has_member(obj, "modified_at") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "modified_at"))) {
      rfc3339 = json_object_get_string_member(obj, "modified_at");
      g_time_val_from_iso8601(rfc3339, &tv);
      dt = g_date_time_new_from_timeval_utc(&tv);
      catch_resource_set_modified_at(resource, dt);
      g_date_time_unref(dt);
   }
   if (json_object_has_member(obj, "server_deleted_at") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "server_deleted_at"))) {
      server_deleted_at = json_object_get_string_member(obj, "server_deleted_at");
      catch_resource_set_server_deleted_at(resource, server_deleted_at);
   }

   catch_resource_set_remote_id(resource, id);
   catch_resource_set_server_modified_at(resource, server_modified_at);

   return TRUE;
}

static void
catch_resource_finalize (GObject *object)
{
   CatchResourcePrivate *priv = CATCH_RESOURCE(object)->priv;

   if (priv->created_at) {
      g_date_time_unref(priv->created_at);
   }

   if (priv->modified_at) {
      g_date_time_unref(priv->modified_at);
   }

   g_free(priv->created_by);
   g_free(priv->created_by_name);
   g_free(priv->remote_id);
   g_free(priv->server_modified_at);

   G_OBJECT_CLASS(catch_resource_parent_class)->finalize(object);
}

static void
catch_resource_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
   CatchResource *resource = CATCH_RESOURCE(object);

   switch (prop_id) {
   case PROP_CREATED_AT:
      g_value_set_boxed(value, catch_resource_get_created_at(resource));
      break;
   case PROP_CREATED_BY:
      g_value_set_string(value, catch_resource_get_created_by(resource));
      break;
   case PROP_CREATED_BY_NAME:
      g_value_set_string(value, catch_resource_get_created_by_name(resource));
      break;
   case PROP_DELETED:
      g_value_set_boolean(value, catch_resource_get_deleted(resource));
      break;
   case PROP_MODIFIED_AT:
      g_value_set_boxed(value, catch_resource_get_modified_at(resource));
      break;
   case PROP_PENDING:
      g_value_set_flags(value, catch_resource_get_pending(resource));
      break;
   case PROP_LOCAL_ID:
      g_value_set_int64(value, catch_resource_get_local_id(resource));
      break;
   case PROP_REMOTE_ID:
      g_value_set_string(value, catch_resource_get_remote_id(resource));
      break;
   case PROP_SERVER_DELETED_AT:
      g_value_set_string(value, catch_resource_get_server_deleted_at(resource));
      break;
   case PROP_SERVER_MODIFIED_AT:
      g_value_set_string(value, catch_resource_get_server_modified_at(resource));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_resource_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
   CatchResource *resource = CATCH_RESOURCE(object);

   switch (prop_id) {
   case PROP_CREATED_AT:
      catch_resource_set_created_at(resource, g_value_get_boxed(value));
      break;
   case PROP_CREATED_BY:
      catch_resource_set_created_by(resource, g_value_get_string(value));
      break;
   case PROP_CREATED_BY_NAME:
      catch_resource_set_created_by_name(resource, g_value_get_string(value));
      break;
   case PROP_DELETED:
      catch_resource_set_deleted(resource, g_value_get_boolean(value));
      break;
   case PROP_MODIFIED_AT:
      catch_resource_set_modified_at(resource, g_value_get_boxed(value));
      break;
   case PROP_PENDING:
      catch_resource_set_pending(resource, g_value_get_flags(value));
      break;
   case PROP_LOCAL_ID:
      catch_resource_set_local_id(resource, g_value_get_int64(value));
      break;
   case PROP_REMOTE_ID:
      catch_resource_set_remote_id(resource, g_value_get_string(value));
      break;
   case PROP_SERVER_DELETED_AT:
      catch_resource_set_server_deleted_at(resource, g_value_get_string(value));
      break;
   case PROP_SERVER_MODIFIED_AT:
      catch_resource_set_server_modified_at(resource,
                                            g_value_get_string(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_resource_class_init (CatchResourceClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_resource_finalize;
   object_class->get_property = catch_resource_get_property;
   object_class->set_property = catch_resource_set_property;
   g_type_class_add_private(object_class, sizeof(CatchResourcePrivate));

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "resources");
   gom_resource_class_set_primary_key(resource_class, "local-id");

   klass->load_from_json = catch_resource_real_load_from_json;

   /**
    * CatchResource:created-at:
    *
    * The "created-at" property.
    */
   gParamSpecs[PROP_CREATED_AT] =
      g_param_spec_boxed("created-at",
                         _("Created At"),
                         _("The time the resource was created."),
                         G_TYPE_DATE_TIME,
                         G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_CREATED_AT,
                                   gParamSpecs[PROP_CREATED_AT]);

   /**
    * CatchResource:created-by:
    *
    * The "created-by" property.
    */
   gParamSpecs[PROP_CREATED_BY] =
      g_param_spec_string("created-by",
                          _("Created By"),
                          _("The creator of the resource."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_CREATED_BY,
                                   gParamSpecs[PROP_CREATED_BY]);

   /**
    * CatchResource:created-by-name:
    *
    * The "created-by-name" property.
    */
   gParamSpecs[PROP_CREATED_BY_NAME] =
      g_param_spec_string("created-by-name",
                          _("Created By Name"),
                          _("The name of the creator of the resource."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_CREATED_BY_NAME,
                                   gParamSpecs[PROP_CREATED_BY_NAME]);

   /**
    * CatchResource:deleted:
    *
    * The "deleted" property.
    */
   gParamSpecs[PROP_DELETED] =
      g_param_spec_boolean("deleted",
                          _("Deleted"),
                          _("If the resource has been marked for deletion."),
                          FALSE,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_DELETED,
                                   gParamSpecs[PROP_DELETED]);

   /**
    * CatchResource:modified-at:
    *
    * The "modified-at" property.
    */
   gParamSpecs[PROP_MODIFIED_AT] =
      g_param_spec_boxed("modified-at",
                         _("Created At"),
                         _("The time the resource was modified."),
                         G_TYPE_DATE_TIME,
                         G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_MODIFIED_AT,
                                   gParamSpecs[PROP_MODIFIED_AT]);

   /**
    * CatchResource:pending:
    *
    * The "pending" property.
    */
   gParamSpecs[PROP_PENDING] =
      g_param_spec_flags("pending",
                         _("Pending"),
                         _("The pending operations."),
                         CATCH_TYPE_PENDING,
                         CATCH_PENDING_NONE,
                         G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_PENDING,
                                   gParamSpecs[PROP_PENDING]);

   /**
    * CatchResource:local-id:
    *
    * The "local-id" property.
    */
   gParamSpecs[PROP_LOCAL_ID] =
      g_param_spec_int64("local-id",
                         _("Local ID"),
                         _("The local identifier for the resource."),
                         -1,
                         G_MAXINT64,
                         -1,
                         G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_LOCAL_ID,
                                   gParamSpecs[PROP_LOCAL_ID]);

   /**
    * CatchResource:remote-id:
    *
    * The "remote-id" property.
    */
   gParamSpecs[PROP_REMOTE_ID] =
      g_param_spec_string("remote-id",
                          _("Remote ID"),
                          _("The remote identifier for the resource."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_REMOTE_ID,
                                   gParamSpecs[PROP_REMOTE_ID]);

   /**
    * CatchResource:server-deleted-at:
    *
    * The "server-deleted-at" property.
    */
   gParamSpecs[PROP_SERVER_DELETED_AT] =
      g_param_spec_string("server-deleted-at",
                          _("Server Deleted At"),
                          _("When the resource was deleted on the server."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SERVER_DELETED_AT,
                                   gParamSpecs[PROP_SERVER_DELETED_AT]);

   /**
    * CatchResource:server-modified-at:
    *
    * The "server-modified-at" property.
    */
   gParamSpecs[PROP_SERVER_MODIFIED_AT] =
      g_param_spec_string("server-modified-at",
                          _("Server Modified At"),
                          _("The version token for the resource."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SERVER_MODIFIED_AT,
                                   gParamSpecs[PROP_SERVER_MODIFIED_AT]);
}

static void
catch_resource_init (CatchResource *resource)
{
   resource->priv = G_TYPE_INSTANCE_GET_PRIVATE(resource,
                                                CATCH_TYPE_RESOURCE,
                                                CatchResourcePrivate);
   resource->priv->created_at = g_date_time_new_now_utc();
   resource->priv->modified_at = g_date_time_new_now_utc();
}

GQuark
catch_resource_error_quark (void)
{
   return g_quark_from_static_string("catch_resource_error_quark");
}

GType
catch_pending_get_type (void)
{
   static GType type_id = 0;
   static gsize initialized = FALSE;
   static GFlagsValue values[] = {
      { CATCH_PENDING_NONE, "CATCH_PENDING_NONE", "NONE" },
      { CATCH_PENDING_CREATE, "CATCH_PENDING_CREATE", "CREATE" },
      { CATCH_PENDING_DELETE, "CATCH_PENDING_DELETE", "DELETE" },
      { CATCH_PENDING_UPDATE, "CATCH_PENDING_UPDATE", "UPDATE" },
      { CATCH_PENDING_IN_SYNC, "CATCH_PENDING_IN_SYNC", "IN_SYNC" },
   };

   if (g_once_init_enter(&initialized)) {
      type_id = g_flags_register_static("CatchPending", values);
      g_once_init_leave(&initialized, TRUE);
   }

   return type_id;
}
