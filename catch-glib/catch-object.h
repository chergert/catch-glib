/* catch-object.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_OBJECT_H
#define CATCH_OBJECT_H

#include "catch-resource.h"

G_BEGIN_DECLS

#define CATCH_TYPE_OBJECT            (catch_object_get_type())
#define CATCH_OBJECT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_OBJECT, CatchObject))
#define CATCH_OBJECT_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_OBJECT, CatchObject const))
#define CATCH_OBJECT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_OBJECT, CatchObjectClass))
#define CATCH_IS_OBJECT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_OBJECT))
#define CATCH_IS_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_OBJECT))
#define CATCH_OBJECT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_OBJECT, CatchObjectClass))

typedef struct _CatchObject        CatchObject;
typedef struct _CatchObjectClass   CatchObjectClass;
typedef struct _CatchObjectPrivate CatchObjectPrivate;

struct _CatchObject
{
   CatchResource parent;

   /*< private >*/
   CatchObjectPrivate *priv;
};

struct _CatchObjectClass
{
   CatchResourceClass parent_class;
};

GType        catch_object_get_type (void) G_GNUC_CONST;
const gchar *catch_object_get_text (CatchObject *object);
void         catch_object_set_text (CatchObject *object,
                                    const gchar *text);

G_END_DECLS

#endif /* CATCH_OBJECT_H */
