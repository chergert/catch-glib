/* catch-message-builder.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "catch-message-builder.h"

struct _CatchMessageBuilder
{
   volatile gint ref_count;
   GPtrArray *params;
   gchar *method;
   gchar *path;
   gchar *scheme;
   gchar *server;
   guint port;
};

/**
 * catch_message_builder_dispose:
 * @builder: A #CatchMessageBuilder.
 *
 * Cleans up the #CatchMessageBuilder instance and frees any allocated
 * resources.
 */
static void
catch_message_builder_dispose (CatchMessageBuilder *builder)
{
   g_free(builder->method);
   g_free(builder->path);
   g_free(builder->server);
   g_free(builder->scheme);
   g_ptr_array_unref(builder->params);
}

/**
 * catch_message_builder_new:
 * @method: (in): The HTTP method such as "GET", "PUT", "POST", "DELETE".
 *
 * Creates a new instance of #CatchMessageBuilder.
 *
 * Returns: the newly created instance which should be freed with
 *   catch_message_builder_unref().
 */
CatchMessageBuilder*
catch_message_builder_new (const gchar *method)
{
   CatchMessageBuilder *builder;

   builder = g_slice_new0(CatchMessageBuilder);
   builder->ref_count = 1;
   builder->method = g_strdup(method ? method : "GET");
   builder->server = g_strdup("api.catch.com");
   builder->scheme = g_strdup("https");
   builder->params = g_ptr_array_new();
   builder->path = g_strdup("");
   g_ptr_array_set_free_func(builder->params, g_free);
   return builder;
}

/**
 * CatchMessageBuilder_ref:
 * @builder: A #CatchMessageBuilder.
 *
 * Atomically increments the reference count of @builder by one.
 *
 * Returns: A reference to @builder.
 */
CatchMessageBuilder *
catch_message_builder_ref (CatchMessageBuilder *builder) /* IN */
{
   g_return_val_if_fail(builder != NULL, NULL);
   g_return_val_if_fail(builder->ref_count > 0, NULL);

   g_atomic_int_inc(&builder->ref_count);
   return builder;
}

/**
 * catch_message_builder_unref:
 * @builder: A CatchMessageBuilder.
 *
 * Atomically decrements the reference count of @builder by one.  When the
 * reference count reaches zero, the structure will be destroyed and
 * freed.
 */
void
catch_message_builder_unref (CatchMessageBuilder *builder) /* IN */
{
   g_return_if_fail(builder != NULL);
   g_return_if_fail(builder->ref_count > 0);

   if (g_atomic_int_dec_and_test(&builder->ref_count)) {
      catch_message_builder_dispose(builder);
      g_slice_free(CatchMessageBuilder, builder);
   }
}

void
catch_message_builder_add_string_param (CatchMessageBuilder *builder,
                                        const gchar         *param,
                                        const gchar         *value)
{
   gchar *encoded;

   g_return_if_fail(builder != NULL);
   g_return_if_fail(param != NULL);
   g_return_if_fail(value != NULL);

   encoded = soup_form_encode(param, value, NULL);
   g_ptr_array_add(builder->params, encoded);
}

void
catch_message_builder_add_bool_param (CatchMessageBuilder *builder,
                                      const gchar         *param,
                                      gboolean             value)
{
   gchar *encoded;

   g_return_if_fail(builder != NULL);
   g_return_if_fail(param != NULL);

   encoded = soup_form_encode(param, value ? "1" : "0", NULL);
   g_ptr_array_add(builder->params, encoded);
}

void
catch_message_builder_add_int_param (CatchMessageBuilder *builder,
                                     const gchar         *param,
                                     gint                 value)
{
   gchar *value_str;
   gchar *encoded;

   g_return_if_fail(builder != NULL);
   g_return_if_fail(param != NULL);

   value_str = g_strdup_printf("%d", value);
   encoded = soup_form_encode(param, value_str, NULL);
   g_ptr_array_add(builder->params, encoded);
   g_free(value_str);
}

void
catch_message_builder_add_filter_param (CatchMessageBuilder *builder,
                                        GomFilter           *filter)
{
}

void
catch_message_builder_set_path (CatchMessageBuilder *builder,
                                const gchar         *first_part,
                                ...)
{
   const gchar *part;
   GPtrArray *array;
   va_list args;

   g_return_if_fail(builder != NULL);
   g_return_if_fail(first_part != NULL);

   array = g_ptr_array_new();
   part = first_part;

   va_start(args, first_part);
   do {
      while (*part && (*part == '/')) {
         part = &part[1];
      }
      g_ptr_array_add(array, (gchar *)part);
   } while ((part = va_arg(args, const gchar *)));
   g_ptr_array_add(array, NULL);
   va_end(args);

   g_free(builder->path);
   builder->path = g_strjoinv("/", (gchar **)array->pdata);

   g_ptr_array_free(array, TRUE);
}

void
catch_message_builder_set_server (CatchMessageBuilder *builder,
                                  const gchar         *server,
                                  const gchar         *scheme,
                                  guint                port)
{
   g_return_if_fail(builder != NULL);

   g_free(builder->server);
   builder->server = g_strdup(server);
   builder->scheme = g_strdup(scheme);
   builder->port = port;
}

/**
 * catch_message_builder_build:
 * @builder: (in): A #CatchMessageBuilder.
 *
 * Generates a SoupMessage representing the built message.
 *
 * Returns: (transfer full): A #SoupMessage.
 */
SoupMessage *
catch_message_builder_build (CatchMessageBuilder *builder)
{
   SoupMessage *message;
   const gchar *path;
   gchar *params;
   gchar *url;

   g_return_val_if_fail(builder != NULL, NULL);

   g_ptr_array_add(builder->params, NULL);
   params = g_strjoinv("&", (gchar **)builder->params->pdata);
   g_ptr_array_remove_index(builder->params, builder->params->len - 1);

   path = builder->path;
   while (*path && (*path == '/')) {
      path = &path[1];
   }

   if (!g_strcmp0(builder->method, "GET")) {
      url = g_strdup_printf("%s://%s:%u/%s?%s", builder->scheme, builder->server, builder->port, path, params);
      message = soup_message_new(builder->method, url);
      g_free(url);
   } else {
      url = g_strdup_printf("%s://%s:%u/%s", builder->scheme, builder->server, builder->port, path);
      message = soup_message_new(builder->method, url);
      soup_message_set_request(message, "application/x-www-form-urlencoded",
                               SOUP_MEMORY_COPY, params, strlen(params));
      g_free(url);
   }

   g_free(params);

   return message;
}

/*

static gchar *
catch_api_get_url_for (CatchApi    *api,
                       const gchar *first_part,
                       ...)
{
   CatchApiPrivate *priv;
   const gchar *part;
   GPtrArray *array;
   va_list args;
   gchar *path;
   gchar *url;

   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   g_return_val_if_fail(first_part != NULL, NULL);

   priv = api->priv;

   array = g_ptr_array_new();
   part = first_part;

   va_start(args, first_part);
   do {
      g_ptr_array_add(array, (gchar *)part);
   } while ((part = va_arg(args, const gchar *)));
   g_ptr_array_add(array, NULL);
   path = g_strjoinv("/", (gchar **)array->pdata);
   va_end(args);

   url = g_strdup_printf("%s://%s:%d/%s",
                         priv->secure ? "https" : "http",
                         priv->server,
                         priv->port ? priv->port : (priv->secure ? 443 : 80),
                         path);

   g_ptr_array_unref(array);
   g_free(path);

   return url;
}
*/

GType
catch_message_builder_get_type (void)
{
   static GType type_id = 0;
   static gsize initialized = FALSE;

   if (g_once_init_enter(&initialized)) {
      type_id = g_boxed_type_register_static("CatchMessageBuilder",
                                             (GBoxedCopyFunc)catch_message_builder_ref,
                                             (GBoxedFreeFunc)catch_message_builder_unref);
      g_once_init_leave(&initialized, TRUE);
   }

   return type_id;
}
