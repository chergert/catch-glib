/* catch-activity.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-activity.h"

G_DEFINE_TYPE(CatchActivity, catch_activity, CATCH_TYPE_RESOURCE)

struct _CatchActivityPrivate
{
   CatchActivityAction action;
   gchar *action_type;
   gchar *object_id;
   gboolean read;
   gchar *stream_id;
};

enum
{
   PROP_0,
   PROP_ACTION,
   PROP_ACTION_TYPE,
   PROP_READ,
   PROP_OBJECT_ID,
   PROP_STREAM_ID,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_activity_action_from_string:
 * @action: (out): A location for a #CatchActivityAction.
 * @action_string: (in): The string to parse.
 *
 * Parses the action name in @action_string and sets the resulting
 * value in @action.
 *
 * Returns: %TRUE if @action is set.
 */
gboolean
catch_activity_action_from_string (CatchActivityAction *action,
                                   const gchar         *action_string)
{
   g_return_val_if_fail(action, FALSE);

   if (!g_strcmp0(action_string, "ADDED")) {
      *action = CATCH_ACTIVITY_ADDED;
   } else if (!g_strcmp0(action_string, "EDITED")) {
      *action = CATCH_ACTIVITY_EDITED;
   } else if (!g_strcmp0(action_string, "REMOVED")) {
      *action = CATCH_ACTIVITY_REMOVED;
   } else if (!g_strcmp0(action_string, "CLAIMED")) {
      *action = CATCH_ACTIVITY_CLAIMED;
   } else {
      return FALSE;
   }

   return TRUE;
}

gboolean
catch_activity_get_read (CatchActivity *activity)
{
   g_return_val_if_fail(CATCH_IS_ACTIVITY(activity), FALSE);
   return activity->priv->read;
}

void
catch_activity_set_read (CatchActivity *activity,
                         gboolean       read_)
{
   g_return_if_fail(CATCH_IS_ACTIVITY(activity));
   activity->priv->read = read_;
   g_object_notify_by_pspec(G_OBJECT(activity), gParamSpecs[PROP_READ]);
}

CatchActivityAction
catch_activity_get_action (CatchActivity *activity)
{
   g_return_val_if_fail(CATCH_IS_ACTIVITY(activity), 0);
   return activity->priv->action;
}

static void
catch_activity_set_action (CatchActivity       *activity,
                           CatchActivityAction  action)
{
   g_return_if_fail(CATCH_IS_ACTIVITY(activity));
   activity->priv->action = action;
   g_object_notify_by_pspec(G_OBJECT(activity), gParamSpecs[PROP_ACTION]);
}

const gchar *
catch_activity_get_action_type (CatchActivity *activity)
{
   g_return_val_if_fail(CATCH_IS_ACTIVITY(activity), NULL);
   return activity->priv->action_type;
}

static void
catch_activity_set_action_type (CatchActivity *activity,
                                const gchar   *action_type)
{
   g_return_if_fail(CATCH_IS_ACTIVITY(activity));
   g_free(activity->priv->action_type);
   activity->priv->action_type = g_strdup(action_type);
   g_object_notify_by_pspec(G_OBJECT(activity), gParamSpecs[PROP_ACTION_TYPE]);
}

const gchar *
catch_activity_get_stream_id (CatchActivity *activity)
{
   g_return_val_if_fail(CATCH_IS_ACTIVITY(activity), 0);
   return activity->priv->stream_id;
}

static void
catch_activity_set_stream_id (CatchActivity *activity,
                              const gchar   *stream_id)
{
   g_return_if_fail(CATCH_IS_ACTIVITY(activity));
   g_free(activity->priv->stream_id);
   activity->priv->stream_id = g_strdup(stream_id);
   g_object_notify_by_pspec(G_OBJECT(activity), gParamSpecs[PROP_STREAM_ID]);
}

const gchar *
catch_activity_get_object_id (CatchActivity *activity)
{
   g_return_val_if_fail(CATCH_IS_ACTIVITY(activity), 0);
   return activity->priv->object_id;
}

static void
catch_activity_set_object_id (CatchActivity *activity,
                              const gchar   *object_id)
{
   g_return_if_fail(CATCH_IS_ACTIVITY(activity));
   g_free(activity->priv->object_id);
   activity->priv->object_id = g_strdup(object_id);
   g_object_notify_by_pspec(G_OBJECT(activity), gParamSpecs[PROP_OBJECT_ID]);
}

static gboolean
catch_activity_load_from_json (CatchResource  *resource,
                               JsonNode       *node,
                               GError        **error)
{
   CatchActivityAction action;
   CatchActivity *activity = (CatchActivity *)resource;
   const gchar *id;
   const gchar *rfc3339;
   JsonObject *obj;
   GDateTime *dt;
   GTimeVal tv;
   gboolean read_;

   g_return_val_if_fail(CATCH_IS_RESOURCE(resource), FALSE);
   g_return_val_if_fail(node, FALSE);

   if (!JSON_NODE_HOLDS_OBJECT(node) ||
       !(obj = json_node_get_object(node)) ||
       !JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "id")) ||
       !json_object_has_member(obj, "id") ||
       !(id = json_object_get_string_member(obj, "id"))) {
      g_set_error(error, CATCH_RESOURCE_ERROR,
                  CATCH_RESOURCE_ERROR_INVALID_JSON,
                  _("The JSON object provided is not valid."));
      return FALSE;
   }

   catch_resource_set_remote_id(resource, id);

   /*
    * Extract the "read" boolean field.
    */
   if (json_object_has_member(obj, "read") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "read"))) {
      read_ = json_object_get_boolean_member(obj, "read");
      catch_activity_set_read(activity, read_);
   }

   /*
    * Extract the "activity_at" date field. We reuse created_at and
    * modified_at of CatchResource for this.
    */
   if (json_object_has_member(obj, "activity_at") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "activity_at")) &&
       (rfc3339 = json_object_get_string_member(obj, "activity_at"))) {
      g_time_val_from_iso8601(rfc3339, &tv);
      dt = g_date_time_new_from_timeval_utc(&tv);
      catch_resource_set_created_at(resource, dt);
      catch_resource_set_modified_at(resource, dt);
      g_date_time_unref(dt);
   }

   /*
    * Extract the "stream_id" field.
    */
   if (json_object_has_member(obj, "stream_id") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "stream_id")) &&
       (id = json_object_get_string_member(obj, "stream_id"))) {
      catch_activity_set_stream_id(activity, id);
   }

   /*
    * Extract the "object_id" field.
    */
   if (json_object_has_member(obj, "object_id") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "object_id")) &&
       (id = json_object_get_string_member(obj, "object_id"))) {
      catch_activity_set_object_id(activity, id);
   }

   /*
    * Extract the "action" field.
    */
   if (json_object_has_member(obj, "action") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "action")) &&
       (id = json_object_get_string_member(obj, "action"))) {
      if (catch_activity_action_from_string(&action, id)) {
         catch_activity_set_action(activity, action);
      }
   }

   /*
    * Extract the "type" field.
    */
   if (json_object_has_member(obj, "type") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "type")) &&
       (id = json_object_get_string_member(obj, "type"))) {
      catch_activity_set_action_type(activity, id);
   }

   return TRUE;
}

static void
catch_activity_finalize (GObject *object)
{
   CatchActivityPrivate *priv;

   priv = CATCH_ACTIVITY(object)->priv;

   g_free(priv->object_id);
   g_free(priv->stream_id);

   G_OBJECT_CLASS(catch_activity_parent_class)->finalize(object);
}

static void
catch_activity_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
   CatchActivity *activity = CATCH_ACTIVITY(object);

   switch (prop_id) {
   case PROP_ACTION:
      g_value_set_enum(value, catch_activity_get_action(activity));
      break;
   case PROP_ACTION_TYPE:
      g_value_set_string(value, catch_activity_get_action_type(activity));
      break;
   case PROP_READ:
      g_value_set_boolean(value, catch_activity_get_read(activity));
      break;
   case PROP_OBJECT_ID:
      g_value_set_string(value, catch_activity_get_object_id(activity));
      break;
   case PROP_STREAM_ID:
      g_value_set_string(value, catch_activity_get_stream_id(activity));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_activity_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
   CatchActivity *activity = CATCH_ACTIVITY(object);

   switch (prop_id) {
   case PROP_READ:
      catch_activity_set_read(activity, g_value_get_boolean(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_activity_class_init (CatchActivityClass *klass)
{
   GObjectClass *object_class;
   CatchResourceClass *resource_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_activity_finalize;
   object_class->get_property = catch_activity_get_property;
   object_class->set_property = catch_activity_set_property;
   g_type_class_add_private(object_class, sizeof(CatchActivityPrivate));

   gom_resource_class_set_table(GOM_RESOURCE_CLASS(klass), "activities");

   resource_class = CATCH_RESOURCE_CLASS(klass);
   resource_class->load_from_json = catch_activity_load_from_json;

   gParamSpecs[PROP_ACTION] =
      g_param_spec_enum("action",
                          _("Action"),
                          _("The activity action."),
                          CATCH_TYPE_ACTIVITY_ACTION,
                          CATCH_ACTIVITY_ADDED,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_ACTION,
                                   gParamSpecs[PROP_ACTION]);

   gParamSpecs[PROP_ACTION_TYPE] =
      g_param_spec_string("action-type",
                          _("Action Type"),
                          _("The type of the target action."),
                          NULL,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_ACTION_TYPE,
                                   gParamSpecs[PROP_ACTION_TYPE]);

   gParamSpecs[PROP_OBJECT_ID] =
      g_param_spec_string("object-id",
                          _("Object Id"),
                          _("The target object (if specified)."),
                          NULL,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_OBJECT_ID,
                                   gParamSpecs[PROP_OBJECT_ID]);

   gParamSpecs[PROP_READ] =
      g_param_spec_boolean("read",
                          _("Read"),
                          _("If the activity has been read."),
                          FALSE,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_READ,
                                   gParamSpecs[PROP_READ]);

   gParamSpecs[PROP_STREAM_ID] =
      g_param_spec_string("stream-id",
                          _("Stream Id"),
                          _("The target stream (if specified)."),
                          NULL,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_STREAM_ID,
                                   gParamSpecs[PROP_STREAM_ID]);
}

static void
catch_activity_init (CatchActivity *activity)
{
   activity->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(activity,
                                  CATCH_TYPE_ACTIVITY,
                                  CatchActivityPrivate);
}

GType
catch_activity_action_get_type (void)
{
   static GType type_id;
   static gsize initialized;
   static const GEnumValue values[] = {
      { CATCH_ACTIVITY_ADDED, "CATCH_ACTIVITY_ADDED", "ADDED" },
      { CATCH_ACTIVITY_EDITED, "CATCH_ACTIVITY_EDITED", "EDITED" },
      { CATCH_ACTIVITY_REMOVED, "CATCH_ACTIVITY_REMOVED", "REMOVED" },
      { CATCH_ACTIVITY_CLAIMED, "CATCH_ACTIVITY_CLAIMED", "CLAIMED" },
      { 0 }
   };

   if (g_once_init_enter(&initialized)) {
      type_id = g_enum_register_static("CatchActivityAction", values);
      g_once_init_leave(&initialized, TRUE);
   }

   return type_id;
}
