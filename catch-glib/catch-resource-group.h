/* catch-resource-group.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_RESOURCE_GROUP_H
#define CATCH_RESOURCE_GROUP_H

#include <gio/gio.h>

#include "catch-resource.h"

G_BEGIN_DECLS

#define CATCH_TYPE_RESOURCE_GROUP            (catch_resource_group_get_type())
#define CATCH_RESOURCE_GROUP(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_RESOURCE_GROUP, CatchResourceGroup))
#define CATCH_RESOURCE_GROUP_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_RESOURCE_GROUP, CatchResourceGroup const))
#define CATCH_RESOURCE_GROUP_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_RESOURCE_GROUP, CatchResourceGroupClass))
#define CATCH_IS_RESOURCE_GROUP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_RESOURCE_GROUP))
#define CATCH_IS_RESOURCE_GROUP_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_RESOURCE_GROUP))
#define CATCH_RESOURCE_GROUP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_RESOURCE_GROUP, CatchResourceGroupClass))

typedef struct _CatchResourceGroup        CatchResourceGroup;
typedef struct _CatchResourceGroupClass   CatchResourceGroupClass;
typedef struct _CatchResourceGroupPrivate CatchResourceGroupPrivate;

struct _CatchResourceGroup
{
   GObject parent;

   /*< private >*/
   CatchResourceGroupPrivate *priv;
};

struct _CatchResourceGroupClass
{
   GObjectClass parent_class;
};

guint          catch_resource_group_get_count    (CatchResourceGroup   *group);
CatchResource *catch_resource_group_get_resource (CatchResourceGroup   *group,
                                                  guint                 index_);
GType          catch_resource_group_get_type     (void) G_GNUC_CONST;
void           catch_resource_group_fetch_async  (CatchResourceGroup   *group,
                                                  guint                 index_,
                                                  guint                 count,
                                                  GAsyncReadyCallback   callback,
                                                  gpointer              user_data);
gboolean       catch_resource_group_fetch_finish (CatchResourceGroup   *group,
                                                  GAsyncResult         *result,
                                                  GError              **error);

G_END_DECLS

#endif /* CATCH_RESOURCE_GROUP_H */
