/* catch-message-builder.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_MESSAGE_BUILDER_H
#define CATCH_MESSAGE_BUILDER_H

#include <glib-object.h>
#include <gom/gom.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

#define CATCH_TYPE_MESSAGE_BUILDER (catch_message_builder_get_type())

typedef struct _CatchMessageBuilder CatchMessageBuilder;

GType                catch_message_builder_get_type         (void) G_GNUC_CONST;
CatchMessageBuilder *catch_message_builder_new              (const gchar *method);
SoupMessage         *catch_message_builder_build            (CatchMessageBuilder *builder);
CatchMessageBuilder *catch_message_builder_ref              (CatchMessageBuilder *builder);
void                 catch_message_builder_unref            (CatchMessageBuilder *builder);
void                 catch_message_builder_add_string_param (CatchMessageBuilder *builder,
                                                             const gchar         *param,
                                                             const gchar         *value);
void                 catch_message_builder_add_int_param    (CatchMessageBuilder *builder,
                                                             const gchar         *param,
                                                             gint                 value);
void                 catch_message_builder_add_bool_param   (CatchMessageBuilder *builder,
                                                             const gchar         *param,
                                                             gboolean             value);
void                 catch_message_builder_add_filter_param (CatchMessageBuilder *builder,
                                                             GomFilter           *filter);
void                 catch_message_builder_set_path         (CatchMessageBuilder *builder,
                                                             const gchar         *first_part,
                                                             ...) G_GNUC_NULL_TERMINATED;
void                 catch_message_builder_set_server       (CatchMessageBuilder *builder,
                                                             const gchar         *server,
                                                             const gchar         *scheme,
                                                             guint                port);

G_END_DECLS

#endif /* CATCH_MESSAGE_BUILDER_H */

