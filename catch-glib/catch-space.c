/* catch-space.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-debug.h"
#include "catch-object.h"
#include "catch-space.h"

G_DEFINE_TYPE(CatchSpace, catch_space, CATCH_TYPE_RESOURCE)

/**
 * SECTION:catch-space
 * @title: CatchSpace
 * @short_description: Spaces of notes and rich objects.
 *
 * #CatchSpace represents a collection of notes. It is similar to a notebook,
 * but is more like a shared space. It is a place where people can gather
 * ideas and notes together, collaborating on creating content.
 *
 * However, a space can also be used by a single person as a "private"
 * space. This allows you to use it as an organizational structure rather
 * than a collaboration environment.
 */

struct _CatchSpacePrivate
{
   gchar *name;
};

enum
{
   PROP_0,
   PROP_NAME,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_space_get_name:
 * @space: (in): A #CatchSpace.
 *
 * Fetches the name of the space.
 *
 * Returns: A string.
 */
const gchar *
catch_space_get_name (CatchSpace *space)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_SPACE(space), NULL);
   RETURN(space->priv->name);
}

/**
 * catch_space_set_name:
 * @space: (in): A #CatchSpace.
 * @name: (in): A string.
 *
 * Sets the name of the space. This is limited to 64 characters.
 */
void
catch_space_set_name (CatchSpace *space,
                       const gchar *name)
{
   ENTRY;

   g_return_if_fail(CATCH_IS_SPACE(space));

   g_free(space->priv->name);

   if (name && g_utf8_strlen(name, -1) > 64) {
      g_warning("%s() name is too long, must be < 64 characters.",
                G_STRFUNC);
      space->priv->name = g_strndup(name, 64);
   } else {
      space->priv->name = g_strdup(name);
   }

   g_object_notify_by_pspec(G_OBJECT(space), gParamSpecs[PROP_NAME]);

   EXIT;
}

static void
catch_space_get_objects_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
   GomResource *resource = (GomResource *)object;
   GSimpleAsyncResult *simple = user_data;
   GomResourceGroup *group;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(GOM_IS_RESOURCE(resource));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (!(group = gom_resource_fetch_m2m_finish(resource, result, &error))) {
      g_simple_async_result_take_error(simple, error);
   } else {
      g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   }

   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

/**
 * catch_space_get_objects_async:
 * @space: (in): A #CatchSpace.
 * @filter: (in): A #GomFilter.
 * @callback: (in): A callback to execute upon completion.
 * @user_data: (in): User data for @callback.
 *
 * Fetches the objects found in a space using the filter provided.
 * @callback is responsible for calling catch_space_get_objects_finish()
 * to complete the request.
 */
void
catch_space_get_objects_async (CatchSpace         *space,
                                GomFilter           *filter,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
   GSimpleAsyncResult *simple;
   GomFilter *map_filter;
   GomFilter *tmp;
   GValue value = { 0 };

   ENTRY;

   g_return_if_fail(CATCH_IS_SPACE(space));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   simple = g_simple_async_result_new(G_OBJECT(space), callback, user_data,
                                      catch_space_get_objects_async);

   g_value_init(&value, G_TYPE_INT64);
   g_object_get_property(G_OBJECT(space), "local-id", &value);
   map_filter = gom_filter_new_eq(CATCH_TYPE_SPACE, "local-id", &value);
   g_value_unset(&value);

   if (filter) {
      tmp = map_filter;
      map_filter = gom_filter_new_and(map_filter, filter);
      g_object_unref(tmp);
   }

   gom_resource_fetch_m2m_async(GOM_RESOURCE(space), CATCH_TYPE_OBJECT,
                                "spaces_objects", map_filter,
                                catch_space_get_objects_cb,
                                simple);

   EXIT;
}

/**
 * catch_space_get_objects_finish:
 * @space: (in): A #CatchSpace.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to fetch a group of objects.
 *
 * Returns: (transfer full): A #GomResourceGroup.
 */
GomResourceGroup *
catch_space_get_objects_finish (CatchSpace   *space,
                                 GAsyncResult  *result,
                                 GError       **error)
{
   GomResourceGroup *group;
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SPACE(space), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple), NULL);

   if (!(group = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   }

   RETURN(group ? g_object_ref(group) : NULL);
}

static gboolean
catch_space_load_from_json (CatchResource  *resource,
                             JsonNode       *node,
                             GError        **error)
{
   CatchSpace *space = (CatchSpace *)resource;
   JsonObject *obj;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SPACE(space), FALSE);
   g_return_val_if_fail(node != NULL, FALSE);

   if (CATCH_RESOURCE_CLASS(catch_space_parent_class)->load_from_json) {
      if (!CATCH_RESOURCE_CLASS(catch_space_parent_class)->
          load_from_json(resource, node, error)) {
         RETURN(FALSE);
      }
   }

   if (JSON_NODE_HOLDS_OBJECT(node) &&
       (obj = json_node_get_object(node))) {
      if (json_object_has_member(obj, "name") &&
          JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "name"))) {
         catch_space_set_name(CATCH_SPACE(resource),
                               json_object_get_string_member(obj, "name"));
      }
   }

   RETURN(TRUE);
}

static void
catch_space_finalize (GObject *object)
{
   ENTRY;
   g_free(CATCH_SPACE(object)->priv->name);
   G_OBJECT_CLASS(catch_space_parent_class)->finalize(object);
   EXIT;
}

static void
catch_space_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
   CatchSpace *space = CATCH_SPACE(object);

   switch (prop_id) {
   case PROP_NAME:
      g_value_set_string(value, catch_space_get_name(space));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_space_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
   CatchSpace *space = CATCH_SPACE(object);

   switch (prop_id) {
   case PROP_NAME:
      catch_space_set_name(space, g_value_get_string(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_space_class_init (CatchSpaceClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;
   CatchResourceClass *catch_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_space_finalize;
   object_class->get_property = catch_space_get_property;
   object_class->set_property = catch_space_set_property;
   g_type_class_add_private(object_class, sizeof(CatchSpacePrivate));

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "spaces");

   catch_class = CATCH_RESOURCE_CLASS(klass);
   catch_class->load_from_json = catch_space_load_from_json;

   /**
    * CatchSpace:name:
    *
    * The "name" property. This is limited to 64 UTF-8 characters.
    */
   gParamSpecs[PROP_NAME] =
      g_param_spec_string("name",
                          _("Name"),
                          _("The spaces name."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_NAME,
                                   gParamSpecs[PROP_NAME]);
}

static void
catch_space_init (CatchSpace *space)
{
   space->priv = G_TYPE_INSTANCE_GET_PRIVATE(space,
                                              CATCH_TYPE_SPACE,
                                              CatchSpacePrivate);
   space->priv->name = g_strdup(_("Untitled"));
}
