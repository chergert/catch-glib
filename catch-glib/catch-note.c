/* catch-note.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "catch-note.h"

G_DEFINE_TYPE(CatchNote, catch_note, CATCH_TYPE_OBJECT)

/**
 * SECTION:catch-note
 * @title: CatchNote
 * @short_description: Represents a note.
 *
 * #CatchNote represents a note in the Catch system. You can add
 * attachments to the note such as #CatchComment, #CatchImage,
 * #CatchAudio, or #CatchAttachment.
 *
 * #CatchNote<!-- -->'s can be added to a #CatchSpace too. A #CatchSpace
 * is a collection of #CatchNote<!-- -->'s. It is an organizational structure
 * that may be used by users to organize or share their notes.
 */

static void
catch_note_class_init (CatchNoteClass *klass)
{
   GomResourceClass *resource_class;

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "notes");
}

static void
catch_note_init (CatchNote *note)
{
}
