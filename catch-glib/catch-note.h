/* catch-note.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_NOTE_H
#define CATCH_NOTE_H

#include "catch-object.h"

G_BEGIN_DECLS

#define CATCH_TYPE_NOTE            (catch_note_get_type())
#define CATCH_NOTE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_NOTE, CatchNote))
#define CATCH_NOTE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_NOTE, CatchNote const))
#define CATCH_NOTE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_NOTE, CatchNoteClass))
#define CATCH_IS_NOTE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_NOTE))
#define CATCH_IS_NOTE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_NOTE))
#define CATCH_NOTE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_NOTE, CatchNoteClass))

typedef struct _CatchNote        CatchNote;
typedef struct _CatchNoteClass   CatchNoteClass;
typedef struct _CatchNotePrivate CatchNotePrivate;

struct _CatchNote
{
   CatchObject parent;

   /*< private >*/
   CatchNotePrivate *priv;
};

struct _CatchNoteClass
{
   CatchObjectClass parent_class;
};

GType catch_note_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CATCH_NOTE_H */
