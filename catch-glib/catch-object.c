/* catch-object.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-object.h"

G_DEFINE_TYPE(CatchObject, catch_object, CATCH_TYPE_RESOURCE)

/**
 * SECTION:catch-object
 * @title: CatchObject
 * @short_description: Represents any note, comment or attachment.
 *
 * #CatchObject is a base class used by #CatchNote, #CatchComment,
 * and #CatchAttachment and its subclasses.
 */

struct _CatchObjectPrivate
{
   gchar *text;
   gchar **unknown_spaces;
};

enum
{
   PROP_0,
   PROP_TEXT,
   PROP_UNKNOWN_SPACES,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_object_get_text:
 * @object: (in): A #CatchObject.
 *
 * Fetches the text associated with the object.
 *
 * Returns: The text as a UTF-8 string.
 */
const gchar *
catch_object_get_text (CatchObject *object)
{
   g_return_val_if_fail(CATCH_IS_OBJECT(object), NULL);
   return object->priv->text;
}

/**
 * catch_object_set_text:
 * @object: (in): A #CatchObject.
 * @text: (in): The new text for @object.
 *
 * Sets the text associated with the object. This is limited to 100,000
 * UTF-8 characters.
 */
void
catch_object_set_text (CatchObject *object,
                       const gchar *text)
{
   g_return_if_fail(CATCH_IS_OBJECT(object));

   g_free(object->priv->text);
   if (text && (g_utf8_strlen(text, -1) > 100000)) {
      g_warning("%s() text is too long, trimming to 100000 characters.",
                G_STRFUNC);
      object->priv->text = g_strndup(text, 100000);
   } else {
      object->priv->text = g_strdup(text);
   }
   g_object_notify_by_pspec(G_OBJECT(object), gParamSpecs[PROP_TEXT]);
}

/**
 * catch_object_get_unknown_spaces:
 * @object: (in): A #CatchObject.
 *
 * Fetches a #GStrv of spaces that @object belongs to, but are not yet
 * known about by the client. This is sometimes used when objects are
 * fetched and a space is not yet known about.
 *
 * Returns: (transfer none): A #GStrv of remote space ids.
 */
gchar **
catch_object_get_unknown_spaces (CatchObject *object)
{
   g_return_val_if_fail(CATCH_IS_OBJECT(object), NULL);
   return object->priv->unknown_spaces;
}

/**
 * catch_object_set_unknown_spaces:
 * @object: (in): A #CatchObject.
 * @unknown_spaces: (in) (transfer none): A #GStrv of spaces.
 *
 * Sets the list of space identifiers that the object exists within,
 * but are not yet known about by the client.
 */
void
catch_object_set_unknown_spaces (CatchObject  *object,
                                  gchar       **unknown_spaces)
{
   g_return_if_fail(CATCH_IS_OBJECT(object));

   g_strfreev(object->priv->unknown_spaces);
   object->priv->unknown_spaces = g_strdupv(unknown_spaces);
   g_object_notify_by_pspec(G_OBJECT(object),
                            gParamSpecs[PROP_UNKNOWN_SPACES]);
}

static gboolean
catch_object_load_from_json (CatchResource  *resource,
                             JsonNode       *node,
                             GError        **error)
{
   CatchObject *object = (CatchObject *)resource;
   JsonObject *obj;

   g_return_val_if_fail(CATCH_IS_OBJECT(object), FALSE);
   g_return_val_if_fail(node != NULL, FALSE);

   if (CATCH_RESOURCE_CLASS(catch_object_parent_class)->load_from_json) {
      if (!CATCH_RESOURCE_CLASS(catch_object_parent_class)->
          load_from_json(resource, node, error)) {
         return FALSE;
      }
   }

   if (JSON_NODE_HOLDS_OBJECT(node) &&
       (obj = json_node_get_object(node))) {
      if (json_object_has_member(obj, "text") &&
          JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "text"))) {
         g_object_set(resource,
                      "text", json_object_get_string_member(obj, "text"),
                      NULL);
      }
   }

   return TRUE;
}

/**
 * catch_object_finalize:
 * @object: (in): A #CatchObject.
 *
 * Finalizer for a #CatchObject instance.
 */
static void
catch_object_finalize (GObject *object)
{
   CatchObjectPrivate *priv = CATCH_OBJECT(object)->priv;

   g_free(priv->text);
   g_strfreev(priv->unknown_spaces);

   G_OBJECT_CLASS(catch_object_parent_class)->finalize(object);
}

/**
 * catch_object_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
catch_object_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
   CatchObject *catch = CATCH_OBJECT(object);

   switch (prop_id) {
   case PROP_TEXT:
      g_value_set_string(value, catch_object_get_text(catch));
      break;
   case PROP_UNKNOWN_SPACES:
      g_value_set_boxed(value, catch_object_get_unknown_spaces(catch));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_object_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
catch_object_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
   CatchObject *catch = CATCH_OBJECT(object);

   switch (prop_id) {
   case PROP_TEXT:
      catch_object_set_text(catch, g_value_get_string(value));
      break;
   case PROP_UNKNOWN_SPACES:
      catch_object_set_unknown_spaces(catch, g_value_get_boxed(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_object_class_init:
 * @klass: (in): A #CatchObjectClass.
 *
 * Initializes the #CatchObjectClass and prepares the vtable.
 */
static void
catch_object_class_init (CatchObjectClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;
   CatchResourceClass *catch_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_object_finalize;
   object_class->get_property = catch_object_get_property;
   object_class->set_property = catch_object_set_property;
   g_type_class_add_private(object_class, sizeof(CatchObjectPrivate));

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "objects");

   catch_class = CATCH_RESOURCE_CLASS(klass);
   catch_class->load_from_json = catch_object_load_from_json;

   /**
    * CatchObject:text:
    *
    * The "text" property is the text associated with the object. It
    * is limited to 100,000 characters.
    */
   gParamSpecs[PROP_TEXT] =
      g_param_spec_string("text",
                          _("Text"),
                          _("The text for the object."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_TEXT,
                                   gParamSpecs[PROP_TEXT]);

   /**
    * CatchObject:unknown-spaces:
    *
    * The "unknown-spaces" property is a list of spaces that
    * the object exists within, but the client does not know about.
    */
   gParamSpecs[PROP_UNKNOWN_SPACES] =
      g_param_spec_boxed("unknown-spaces",
                          _("Unknown Spaces"),
                          _("The spaces this object is in that we do not have information on."),
                          G_TYPE_STRV,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_UNKNOWN_SPACES,
                                   gParamSpecs[PROP_UNKNOWN_SPACES]);
}

/**
 * catch_object_init:
 * @object: (in): A #CatchObject.
 *
 * Initializes the newly created #CatchObject instance.
 */
static void
catch_object_init (CatchObject *object)
{
   object->priv = G_TYPE_INSTANCE_GET_PRIVATE(object,
                                              CATCH_TYPE_OBJECT,
                                              CatchObjectPrivate);
}
