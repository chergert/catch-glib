/* catch-attachment.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_ATTACHMENT_H
#define CATCH_ATTACHMENT_H

#include "catch-object.h"

G_BEGIN_DECLS

#define CATCH_TYPE_ATTACHMENT            (catch_attachment_get_type())
#define CATCH_ATTACHMENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_ATTACHMENT, CatchAttachment))
#define CATCH_ATTACHMENT_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_ATTACHMENT, CatchAttachment const))
#define CATCH_ATTACHMENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_ATTACHMENT, CatchAttachmentClass))
#define CATCH_IS_ATTACHMENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_ATTACHMENT))
#define CATCH_IS_ATTACHMENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_ATTACHMENT))
#define CATCH_ATTACHMENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_ATTACHMENT, CatchAttachmentClass))

typedef struct _CatchAttachment        CatchAttachment;
typedef struct _CatchAttachmentClass   CatchAttachmentClass;
typedef struct _CatchAttachmentPrivate CatchAttachmentPrivate;

struct _CatchAttachment
{
   CatchObject parent;

   /*< private >*/
   CatchAttachmentPrivate *priv;
};

struct _CatchAttachmentClass
{
   CatchObjectClass parent_class;
};

GType        catch_attachment_get_type         (void) G_GNUC_CONST;
const gchar *catch_attachment_get_content_type (CatchAttachment *attachment);
const gchar *catch_attachment_get_local_uri    (CatchAttachment *attachment);
void         catch_attachment_set_local_uri    (CatchAttachment *attachment,
                                                const gchar     *local_uri);
gsize        catch_attachment_get_size         (CatchAttachment *attachment);

G_END_DECLS

#endif /* CATCH_ATTACHMENT_H */
