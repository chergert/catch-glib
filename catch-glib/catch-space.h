/* catch-space.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_SPACE_H
#define CATCH_SPACE_H

#include "catch-resource.h"

G_BEGIN_DECLS

#define CATCH_TYPE_SPACE            (catch_space_get_type())
#define CATCH_SPACE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_SPACE, CatchSpace))
#define CATCH_SPACE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_SPACE, CatchSpace const))
#define CATCH_SPACE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_SPACE, CatchSpaceClass))
#define CATCH_IS_SPACE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_SPACE))
#define CATCH_IS_SPACE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_SPACE))
#define CATCH_SPACE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_SPACE, CatchSpaceClass))

typedef struct _CatchSpace        CatchSpace;
typedef struct _CatchSpaceClass   CatchSpaceClass;
typedef struct _CatchSpacePrivate CatchSpacePrivate;

struct _CatchSpace
{
   CatchResource parent;

   /*< private >*/
   CatchSpacePrivate *priv;
};

struct _CatchSpaceClass
{
   CatchResourceClass parent_class;
};

GType             catch_space_get_type           (void) G_GNUC_CONST;
const gchar      *catch_space_get_name           (CatchSpace          *space);
void              catch_space_set_name           (CatchSpace          *space,
                                                   const gchar          *name);
void              catch_space_get_objects_async  (CatchSpace          *space,
                                                   GomFilter            *filter,
                                                   GAsyncReadyCallback   callback,
                                                   gpointer              user_data);
GomResourceGroup *catch_space_get_objects_finish (CatchSpace          *space,
                                                   GAsyncResult         *result,
                                                   GError              **error);

G_END_DECLS

#endif /* CATCH_SPACE_H */
