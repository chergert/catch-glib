/* catch-check-item.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_CHECK_ITEM_H
#define CATCH_CHECK_ITEM_H

#include "catch-object.h"

G_BEGIN_DECLS

#define CATCH_TYPE_CHECK_ITEM            (catch_check_item_get_type())
#define CATCH_CHECK_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_CHECK_ITEM, CatchCheckItem))
#define CATCH_CHECK_ITEM_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_CHECK_ITEM, CatchCheckItem const))
#define CATCH_CHECK_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_CHECK_ITEM, CatchCheckItemClass))
#define CATCH_IS_CHECK_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_CHECK_ITEM))
#define CATCH_IS_CHECK_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_CHECK_ITEM))
#define CATCH_CHECK_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_CHECK_ITEM, CatchCheckItemClass))

typedef struct _CatchCheckItem        CatchCheckItem;
typedef struct _CatchCheckItemClass   CatchCheckItemClass;
typedef struct _CatchCheckItemPrivate CatchCheckItemPrivate;

struct _CatchCheckItem
{
   CatchObject parent;

   /*< private >*/
   CatchCheckItemPrivate *priv;
};

struct _CatchCheckItemClass
{
   CatchObjectClass parent_class;
};

GType catch_check_item_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CATCH_CHECK_ITEM_H */
