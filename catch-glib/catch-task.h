/* catch-task.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_TASK_H
#define CATCH_TASK_H

#include <gio/gio.h>

G_BEGIN_DECLS

#define CATCH_TYPE_TASK            (catch_task_get_type())
#define CATCH_TASK(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_TASK, CatchTask))
#define CATCH_TASK_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_TASK, CatchTask const))
#define CATCH_TASK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_TASK, CatchTaskClass))
#define CATCH_IS_TASK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_TASK))
#define CATCH_IS_TASK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_TASK))
#define CATCH_TASK_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_TASK, CatchTaskClass))

typedef struct _CatchTask        CatchTask;
typedef struct _CatchTaskClass   CatchTaskClass;
typedef struct _CatchTaskPrivate CatchTaskPrivate;

typedef void (*CatchTaskFunc) (CatchTask *task,
                               gpointer   user_data);

struct _CatchTask
{
   GInitiallyUnowned parent;

   /*< private >*/
   CatchTaskPrivate *priv;
};

struct _CatchTaskClass
{
   GInitiallyUnownedClass parent_class;

   void (*completed) (CatchTask *task);
   void (*execute)   (CatchTask *task);
};

void          catch_task_add_dependency    (CatchTask            *task,
                                            CatchTask            *dependency);
void          catch_task_complete          (CatchTask            *task);
void          catch_task_complete_in_idle  (CatchTask            *task);
const GError *catch_task_get_error         (CatchTask            *task);
gboolean      catch_task_get_is_error      (CatchTask            *task);
GType         catch_task_get_type          (void) G_GNUC_CONST;
gboolean      catch_task_get_use_thread    (CatchTask            *task);
CatchTask    *catch_task_new               (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data);
CatchTask    *catch_task_new_all_of        (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            CatchTask            *first_task,
                                            ...) G_GNUC_NULL_TERMINATED;
CatchTask    *catch_task_new_all_of_valist (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            CatchTask            *first_task,
                                            va_list               args);
CatchTask    *catch_task_new_any_of        (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            CatchTask            *first_task,
                                            ...) G_GNUC_NULL_TERMINATED;
CatchTask    *catch_task_new_any_of_valist (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            CatchTask            *first_task,
                                            va_list               args);
CatchTask    *catch_task_new_n_of_valist   (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            guint                 n_required,
                                            CatchTask            *first_task,
                                            va_list               args);
CatchTask    *catch_task_new_n_of          (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            guint                 n_required,
                                            CatchTask            *first_task,
                                            ...) G_GNUC_NULL_TERMINATED;
CatchTask    *catch_task_new_with_func     (gpointer              source_object,
                                            GAsyncReadyCallback   callback,
                                            gpointer              user_data,
                                            CatchTaskFunc         task_func,
                                            gpointer              task_data,
                                            GDestroyNotify        task_notify);
void          catch_task_print_tree        (CatchTask            *task);
void          catch_task_propagate_error   (CatchTask            *task,
                                            GError              **error);
void          catch_task_remove_dependency (CatchTask            *task,
                                            CatchTask            *dependency);
void          catch_task_run               (CatchTask            *task);
void          catch_task_run_all           (CatchTask            *first_task,
                                            ...) G_GNUC_NULL_TERMINATED;
void          catch_task_run_all_valist    (CatchTask            *first_task,
                                            va_list               tasks);
void          catch_task_set_error         (CatchTask            *task,
                                            const GError         *error);
void          catch_task_set_use_thread    (CatchTask            *task,
                                            gboolean              use_thread);
void          catch_task_set_value         (CatchTask            *task,
                                            const GValue         *value);
void          catch_task_set_value_boolean (CatchTask            *task,
                                            gboolean              v_boolean);
void          catch_task_set_value_int     (CatchTask            *task,
                                            gint                  v_int);
void          catch_task_set_value_string  (CatchTask            *task,
                                            const gchar          *v_str);
void          catch_task_take_error        (CatchTask            *task,
                                            GError               *error);

G_END_DECLS

#endif /* CATCH_TASK_H */
