/* catch-check-item.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "catch-check-item.h"

G_DEFINE_TYPE(CatchCheckItem, catch_check_item, CATCH_TYPE_OBJECT)

/**
 * SECTION:catch-check_item
 * @title: CatchCheckItem
 * @short_description: Represents a Check Item on a note.
 *
 * #CatchCheckItem represents a Check Item on a #CatchNote.
 */

static void
catch_check_item_class_init (CatchCheckItemClass *klass)
{
   GomResourceClass *resource_class;

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "checkitems");
}

static void
catch_check_item_init (CatchCheckItem *check_item)
{
}
