/* catch-task-sync.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_TASK_SYNC_H
#define CATCH_TASK_SYNC_H

#include "catch-task.h"

G_BEGIN_DECLS

#define CATCH_TYPE_TASK_SYNC            (catch_task_sync_get_type())
#define CATCH_TASK_SYNC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_TASK_SYNC, CatchTaskSync))
#define CATCH_TASK_SYNC_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_TASK_SYNC, CatchTaskSync const))
#define CATCH_TASK_SYNC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_TASK_SYNC, CatchTaskSyncClass))
#define CATCH_IS_TASK_SYNC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_TASK_SYNC))
#define CATCH_IS_TASK_SYNC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_TASK_SYNC))
#define CATCH_TASK_SYNC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_TASK_SYNC, CatchTaskSyncClass))

typedef struct _CatchTaskSync        CatchTaskSync;
typedef struct _CatchTaskSyncClass   CatchTaskSyncClass;
typedef struct _CatchTaskSyncPrivate CatchTaskSyncPrivate;

struct _CatchTaskSync
{
   CatchTask parent;

   /*< private >*/
   CatchTaskSyncPrivate *priv;
};

struct _CatchTaskSyncClass
{
   CatchTaskClass parent_class;
};

GType catch_task_sync_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CATCH_TASK_SYNC_H */
