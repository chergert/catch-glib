/* catch-task.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-task.h"

/**
 * SECTION:catch-task
 * @title: CatchTask
 * @short_description: Asynchronous task decomposition.
 *
 * #CatchTask is a #GAsyncResult that provides many additional features
 * for building complex task dependency trees.
 */

static void g_async_result_init (GAsyncResultIface *iface);

G_DEFINE_TYPE_EXTENDED(CatchTask, catch_task, G_TYPE_INITIALLY_UNOWNED, 0,
                       G_IMPLEMENT_INTERFACE(G_TYPE_ASYNC_RESULT,
                                             g_async_result_init))

struct _CatchTaskPrivate
{
   GObject *source_object;
   GAsyncReadyCallback callback;
   gpointer callback_data;

   GMutex mutex;
   GList *deps;
   gint state;
   gint n_deps_required;

   CatchTaskFunc task_func;
   gpointer task_data;
   GDestroyNotify task_notify;

   gboolean use_thread;

   GError *error;
   GValue  value;
};

enum
{
   PROP_0,
   PROP_CALLBACK,
   PROP_CALLBACK_DATA,
   PROP_IS_ERROR,
   PROP_SOURCE_OBJECT,
   PROP_USE_THREAD,
   LAST_PROP
};

enum
{
   STATE_0,
   STATE_WAITING,
   STATE_READY,
   STATE_RUNNING,
   STATE_COMPLETED,
};

enum
{
   COMPLETED,
   LAST_SIGNAL
};

static GParamSpec *gParamSpecs[LAST_PROP];
static guint       gSignals[LAST_SIGNAL];

CatchTask *
catch_task_new (gpointer            source_object,
                GAsyncReadyCallback callback,
                gpointer            user_data)
{
   CatchTask *task;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);

   task = g_object_new(CATCH_TYPE_TASK, NULL);
   task->priv->source_object = g_object_ref(source_object);
   task->priv->callback = callback;
   task->priv->callback_data = user_data;

   return task;
}

CatchTask *
catch_task_new_all_of_valist (gpointer             source_object,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data,
                              CatchTask           *first_task,
                              va_list              args)
{
   CatchTask *task;
   gint n_deps = 0;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);
   g_return_val_if_fail(CATCH_IS_TASK(first_task), NULL);

   task = catch_task_new(source_object, callback, user_data);

   do {
      catch_task_add_dependency(task, first_task);
      n_deps++;
   } while ((first_task = va_arg(args, CatchTask *)));

   task->priv->n_deps_required = n_deps;

   return task;
}

CatchTask *
catch_task_new_all_of (gpointer             source_object,
                       GAsyncReadyCallback  callback,
                       gpointer             user_data,
                       CatchTask           *first_task,
                       ...)
{
   CatchTask *task;
   va_list args;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);
   g_return_val_if_fail(CATCH_IS_TASK(first_task), NULL);

   va_start(args, first_task);
   task = catch_task_new_all_of_valist(source_object, callback, user_data,
                                       first_task, args);
   va_end(args);

   return task;
}

CatchTask *
catch_task_new_any_of_valist (gpointer             source_object,
                              GAsyncReadyCallback  callback,
                              gpointer             user_data,
                              CatchTask           *first_task,
                              va_list              args)
{
   CatchTask *task;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);
   g_return_val_if_fail(CATCH_IS_TASK(first_task), NULL);

   task = catch_task_new(source_object, callback, user_data);
   task->priv->n_deps_required = 1;

   do {
      catch_task_add_dependency(task, first_task);
   } while ((first_task = va_arg(args, CatchTask *)));

   return task;
}

CatchTask *
catch_task_new_any_of (gpointer             source_object,
                       GAsyncReadyCallback  callback,
                       gpointer             user_data,
                       CatchTask           *first_task,
                       ...)
{
   CatchTask *task;
   va_list args;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);
   g_return_val_if_fail(CATCH_IS_TASK(first_task), NULL);

   va_start(args, first_task);
   task = catch_task_new_any_of_valist(source_object, callback, user_data,
                                       first_task, args);
   va_end(args);

   return task;
}

CatchTask *
catch_task_new_n_of_valist (gpointer             source_object,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data,
                            guint                n_required,
                            CatchTask           *first_task,
                            va_list              args)
{
   CatchTask *task;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);
   g_return_val_if_fail(CATCH_IS_TASK(first_task), NULL);

   task = catch_task_new(source_object, callback, user_data);
   task->priv->n_deps_required = n_required;

   do {
      catch_task_add_dependency(task, first_task);
   } while ((first_task = va_arg(args, CatchTask *)));

   return task;
}

CatchTask *
catch_task_new_n_of (gpointer             source_object,
                     GAsyncReadyCallback  callback,
                     gpointer             user_data,
                     guint                n_required,
                     CatchTask           *first_task,
                     ...)
{
   CatchTask *task;
   va_list args;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);
   g_return_val_if_fail(CATCH_IS_TASK(first_task), NULL);

   va_start(args, first_task);
   task = catch_task_new_n_of_valist(source_object, callback, user_data,
                                     n_required, first_task, args);
   va_end(args);

   return task;
}

CatchTask *
catch_task_new_with_func (gpointer            source_object,
                          GAsyncReadyCallback callback,
                          gpointer            user_data,
                          CatchTaskFunc       task_func,
                          gpointer            task_data,
                          GDestroyNotify      task_notify)
{
   CatchTask *task;

   g_return_val_if_fail(G_IS_OBJECT(source_object), NULL);

   task = catch_task_new(source_object, callback, user_data);
   task->priv->task_func = task_func;
   task->priv->task_data = task_data;
   task->priv->task_notify = task_notify;

   return task;
}

gboolean
catch_task_get_is_error (CatchTask *task)
{
   g_return_val_if_fail(CATCH_IS_TASK(task), FALSE);
   return !!task->priv->error;
}

const GError *
catch_task_get_error (CatchTask *task)
{
   g_return_val_if_fail(CATCH_IS_TASK(task), NULL);
   return task->priv->error;
}

void
catch_task_set_error (CatchTask    *task,
                      const GError *error)
{
   g_return_if_fail(CATCH_IS_TASK(task));

   g_clear_error(&task->priv->error);
   if (error) {
      task->priv->error = g_error_copy(error);
   }
   g_value_unset(&task->priv->value);
}

void
catch_task_take_error (CatchTask *task,
                       GError    *error)
{
   g_return_if_fail(CATCH_IS_TASK(task));

   g_clear_error(&task->priv->error);
   task->priv->error = error;
   g_value_unset(&task->priv->value);
}

void
catch_task_set_value (CatchTask    *task,
                      const GValue *value)
{
   g_return_if_fail(CATCH_IS_TASK(task));
   g_return_if_fail(G_IS_VALUE(value));

   if (task->priv->error) {
      g_clear_error(&task->priv->error);
   }

   g_value_unset(&task->priv->value);
   g_value_copy(value, &task->priv->value);
}

void
catch_task_set_value_boolean (CatchTask *task,
                              gboolean   v_boolean)
{
   GValue value = { 0 };

   g_return_if_fail(CATCH_IS_TASK(task));

   g_value_init(&value, G_TYPE_BOOLEAN);
   g_value_set_boolean(&value, v_boolean);
   catch_task_set_value(task, &value);
   g_value_unset(&value);
}

void
catch_task_set_value_int (CatchTask *task,
                          gint       v_int)
{
   GValue value = { 0 };

   g_return_if_fail(CATCH_IS_TASK(task));

   g_value_init(&value, G_TYPE_INT);
   g_value_set_int(&value, v_int);
   catch_task_set_value(task, &value);
   g_value_unset(&value);
}

void
catch_task_set_value_string (CatchTask   *task,
                             const gchar *v_str)
{
   GValue value = { 0 };

   g_return_if_fail(CATCH_IS_TASK(task));

   g_value_init(&value, G_TYPE_STRING);
   g_value_set_string(&value, v_str);
   catch_task_set_value(task, &value);
   g_value_unset(&value);
}

void
catch_task_propagate_error (CatchTask  *task,
                            GError    **error)
{
   g_return_if_fail(CATCH_IS_TASK(task));

   if (error) {
      *error = g_error_copy(task->priv->error);
   }
}

gboolean
catch_task_get_use_thread (CatchTask *task)
{
   g_return_val_if_fail(CATCH_IS_TASK(task), FALSE);
   return task->priv->use_thread;
}

void
catch_task_set_use_thread (CatchTask *task,
                           gboolean   use_thread)
{
   CatchTaskPrivate *priv;

   g_return_if_fail(CATCH_IS_TASK(task));
   g_return_if_fail(task->priv->state == STATE_0);

   priv = task->priv;

   g_mutex_lock(&priv->mutex);
   if (priv->state == STATE_0) {
      priv->use_thread = use_thread;
   }
   g_mutex_unlock(&priv->mutex);

   g_object_notify_by_pspec(G_OBJECT(task), gParamSpecs[PROP_USE_THREAD]);
}

static void
catch_task_real_completed (CatchTask *task)
{
   g_return_if_fail(CATCH_IS_TASK(task));

   if (task->priv->callback) {
      task->priv->callback(task->priv->source_object,
                           G_ASYNC_RESULT(task),
                           task->priv->callback_data);
   }
}

void
catch_task_complete (CatchTask *task)
{
   CatchTaskPrivate *priv;

   g_return_if_fail(CATCH_IS_TASK(task));

   priv = task->priv;

   g_mutex_lock(&priv->mutex);
   if (priv->state != STATE_RUNNING) {
      g_warning("%s(): Attempt to complete task that is not running.",
                G_STRFUNC);
   }
   priv->state = STATE_COMPLETED;
   g_mutex_unlock(&priv->mutex);

   g_signal_emit(task, gSignals[COMPLETED], 0);

   /*
    * Release our floating ref from catch_task_run().
    */
   g_object_unref(task);
}

static gboolean
catch_task_complete_in_idle_cb (gpointer data)
{
   g_assert(CATCH_IS_TASK(data));
   catch_task_complete(data);
   g_object_unref(data);
   return FALSE;
}

void
catch_task_complete_in_idle (CatchTask *task)
{
   g_return_if_fail(CATCH_IS_TASK(task));
   g_timeout_add(0, catch_task_complete_in_idle_cb, g_object_ref(task));
}

static gboolean
catch_task_worker (CatchTask *task)
{
   g_assert(CATCH_IS_TASK(task));

   if (task->priv->task_func) {
      task->priv->task_func(task, task->priv->task_data);
   } else if (CATCH_TASK_GET_CLASS(task)->execute) {
      CATCH_TASK_GET_CLASS(task)->execute(task);
   } else {
      catch_task_complete_in_idle(task);
   }

   /*
    * Release reference from catch_task_execute_locked().
    */
   g_object_unref(task);

   return FALSE;
}

static void
catch_task_execute_locked (CatchTask *task)
{
   CatchTaskPrivate *priv;

   g_assert(CATCH_IS_TASK(task));
   g_assert(task->priv->state == STATE_READY);

   priv = task->priv;

   priv->state = STATE_RUNNING;

   if (priv->use_thread) {
      g_thread_new("CatchTaskWorker",
                   (GThreadFunc)catch_task_worker,
                   g_object_ref(task));
   } else {
      g_timeout_add(0,
                    (GSourceFunc)catch_task_worker,
                    g_object_ref(task));
   }
}

static void
catch_task_update_state_locked (CatchTask *task)
{
   CatchTaskPrivate *priv;
   GList *iter;
   GList *list;

   g_assert(CATCH_IS_TASK(task));

   priv = task->priv;

   switch (priv->state) {
   case STATE_0:
   case STATE_WAITING:
      priv->state = (!priv->deps) ? STATE_READY : STATE_WAITING;
      if (priv->state == STATE_WAITING) {
         if (priv->n_deps_required == 0) {
            list = priv->deps, priv->deps = NULL;
            priv->state = STATE_READY;
            for (iter = list; iter; iter = iter->next) {
               g_signal_handlers_disconnect_by_func(iter->data,
                                                    catch_task_remove_dependency,
                                                    task);
               g_object_unref(iter->data);
            }
            g_list_free(list);
         }
      }
      break;
   case STATE_READY:
   case STATE_RUNNING:
   case STATE_COMPLETED:
      break;
   default:
      break;
   }
}

static void
catch_task_try_execute_locked (CatchTask *task)
{
   g_assert(CATCH_IS_TASK(task));
   catch_task_update_state_locked(task);
   if (task->priv->state == STATE_READY) {
      catch_task_execute_locked(task);
   }
}

void
catch_task_add_dependency (CatchTask *task,
                           CatchTask *dependency)
{
   CatchTaskPrivate *priv;

   g_return_if_fail(CATCH_IS_TASK(task));
   g_return_if_fail(CATCH_IS_TASK(dependency));
   g_return_if_fail(task != dependency);

   priv = task->priv;

   g_mutex_lock(&priv->mutex);
   priv->deps = g_list_prepend(priv->deps, g_object_ref(dependency));
   g_signal_connect_swapped(dependency, "completed",
                            G_CALLBACK(catch_task_remove_dependency),
                            task);
   g_mutex_unlock(&priv->mutex);
}

void
catch_task_remove_dependency (CatchTask *task,
                              CatchTask *dependency)
{
   CatchTaskPrivate *priv;
   GList *iter;

   g_return_if_fail(CATCH_IS_TASK(task));
   g_return_if_fail(CATCH_IS_TASK(dependency));
   g_return_if_fail(task != dependency);

   priv = task->priv;

   g_mutex_lock(&priv->mutex);

   for (iter = priv->deps; iter; iter = iter->next) {
      if (iter->data == (void *)dependency) {
         priv->deps = g_list_remove_link(priv->deps, iter);
         g_object_unref(dependency);
         priv->n_deps_required--;
         break;
      }
   }

   catch_task_update_state_locked(task);
   catch_task_try_execute_locked(task);

   g_mutex_unlock(&priv->mutex);
}

void
catch_task_run (CatchTask *task)
{
   CatchTaskPrivate *priv;

   g_return_if_fail(CATCH_IS_TASK(task));

   priv = task->priv;

   g_mutex_lock(&priv->mutex);
   if (priv->state != STATE_0) {
      g_warning("%s(): Attempt to start task more than once!",
                G_STRFUNC);
      g_mutex_unlock(&priv->mutex);
      return;
   }
   g_object_ref_sink(task);
   catch_task_try_execute_locked(task);
   g_mutex_unlock(&priv->mutex);
}

void
catch_task_run_all_valist (CatchTask *first_task,
                           va_list    args)
{
   CatchTask *task = first_task;

   g_return_if_fail(CATCH_IS_TASK(first_task));

   do {
      catch_task_run(task);
   } while ((task = va_arg(args, CatchTask *)));
}

void
catch_task_run_all (CatchTask *first_task,
                    ...)
{
   va_list args;

   g_return_if_fail(CATCH_IS_TASK(first_task));

   va_start(args, first_task);
   catch_task_run_all_valist(first_task, args);
   va_end(args);
}

static const gchar *
get_state_name (gint state)
{
   switch (state) {
   case STATE_0:
      return "INITIAL";
   case STATE_WAITING:
      return "WAITING";
   case STATE_READY:
      return "READY";
   case STATE_RUNNING:
      return "RUNNING";
   case STATE_COMPLETED:
      return "COMPLETED";
   default:
      g_assert_not_reached();
      return "";
   }
}

static void
catch_task_print_task (CatchTask *task,
                       guint      level)
{
   GList *iter;
   guint i;

   for (i = 0; i < level; i++) {
      g_print("  ");
   }

   g_print("Task[%p] [%s] (%d)\n",
           task,
           get_state_name(task->priv->state),
           g_list_length(task->priv->deps));

   for (iter = task->priv->deps; iter; iter = iter->next) {
      catch_task_print_task(iter->data, level + 1);
   }
}

void
catch_task_print_tree (CatchTask *task)
{
   catch_task_print_task(task, 0);
}

static void
catch_task_dispose (GObject *object)
{
   CatchTaskPrivate *priv;
   GDestroyNotify notify;
   gpointer data;

   priv = CATCH_TASK(object)->priv;

   if ((data = priv->source_object)) {
      priv->source_object = NULL;
      g_object_unref(data);
   }

   if ((notify = priv->task_notify)) {
      data = priv->task_data;
      priv->task_notify = NULL;
      priv->task_data = NULL;
      notify(data);
   }
}

static void
catch_task_finalize (GObject *object)
{
   CatchTaskPrivate *priv;

   priv = CATCH_TASK(object)->priv;
   g_mutex_clear(&priv->mutex);
   G_OBJECT_CLASS(catch_task_parent_class)->finalize(object);
}

static void
catch_task_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
   CatchTask *task = CATCH_TASK(object);

   switch (prop_id) {
   case PROP_IS_ERROR:
      g_value_set_boolean(value, catch_task_get_is_error(task));
      break;
   case PROP_USE_THREAD:
      g_value_set_boolean(value, catch_task_get_use_thread(task));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_task_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
   CatchTask *task = CATCH_TASK(object);

   switch (prop_id) {
   case PROP_CALLBACK:
      task->priv->callback = g_value_get_pointer(value);
      break;
   case PROP_CALLBACK_DATA:
      task->priv->callback_data = g_value_get_pointer(value);
      break;
   case PROP_SOURCE_OBJECT:
      task->priv->source_object = g_value_dup_object(value);
      break;
   case PROP_USE_THREAD:
      catch_task_set_use_thread(task, g_value_get_boolean(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_task_class_init (CatchTaskClass *klass)
{
   GObjectClass *object_class;
   CatchTaskClass *task_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->dispose = catch_task_dispose;
   object_class->finalize = catch_task_finalize;
   object_class->get_property = catch_task_get_property;
   object_class->set_property = catch_task_set_property;
   g_type_class_add_private(object_class, sizeof(CatchTaskPrivate));

   task_class = CATCH_TASK_CLASS(klass);
   task_class->completed = catch_task_real_completed;

   gParamSpecs[PROP_CALLBACK] =
      g_param_spec_pointer("callback",
                           _("Callback"),
                           _("The callback upon completion."),
                           G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_CALLBACK,
                                   gParamSpecs[PROP_CALLBACK]);

   gParamSpecs[PROP_CALLBACK_DATA] =
      g_param_spec_pointer("callback-data",
                           _("Callback Data"),
                           _("User data for callback."),
                           G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_CALLBACK_DATA,
                                   gParamSpecs[PROP_CALLBACK_DATA]);

   /**
    * CatchTask:is-error:
    *
    * The "is-error" property denotes if the task finished unsuccessfuly.
    * If this is %TRUE, then you may fetch the error using
    * catch_task_get_error().
    */
   gParamSpecs[PROP_IS_ERROR] =
      g_param_spec_boolean("is-error",
                          _("Is Error"),
                          _("Did the task generate an error."),
                          FALSE,
                          G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_IS_ERROR,
                                   gParamSpecs[PROP_IS_ERROR]);

   gParamSpecs[PROP_SOURCE_OBJECT] =
      g_param_spec_object("source-object",
                          _("Source Object"),
                          _("The source object for the task."),
                          G_TYPE_OBJECT,
                          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_SOURCE_OBJECT,
                                   gParamSpecs[PROP_SOURCE_OBJECT]);

   /**
    * CatchTask:use-thread:
    *
    * The "use-thread" property denotes that the task should be executed
    * within a thread rather than on the main loop. This might be ideal if
    * your task needs to run a long blocking call and you do not want to
    * manage using a thread pool directly.
    */
   gParamSpecs[PROP_USE_THREAD] =
      g_param_spec_boolean("use-thread",
                           _("Use Thread"),
                           _("If a thread should be used to run the task."),
                           FALSE,
                           G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_USE_THREAD,
                                   gParamSpecs[PROP_USE_THREAD]);

   /**
    * CatchTask::completed:
    *
    * The "completed" signal is emitted when the task has finished its
    * execution.
    */
   gSignals[COMPLETED] = g_signal_new("completed",
                                      CATCH_TYPE_TASK,
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET(CatchTaskClass, completed),
                                      NULL,
                                      NULL,
                                      g_cclosure_marshal_VOID__VOID,
                                      G_TYPE_NONE,
                                      0);
}

static void
catch_task_init (CatchTask *task)
{
   task->priv = G_TYPE_INSTANCE_GET_PRIVATE(task, CATCH_TYPE_TASK,
                                            CatchTaskPrivate);
   task->priv->n_deps_required = -1;
   g_mutex_init(&task->priv->mutex);
}

static gpointer
catch_task_get_user_data (GAsyncResult *result)
{
   return CATCH_TASK(result)->priv->callback_data;
}

static GObject *
catch_task_get_source_object (GAsyncResult *result)
{
   return CATCH_TASK(result)->priv->source_object;
}

static void
g_async_result_init (GAsyncResultIface *iface)
{
   iface->get_user_data = catch_task_get_user_data;
   iface->get_source_object = catch_task_get_source_object;
}
