/* catch-attachment.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-attachment.h"

G_DEFINE_TYPE(CatchAttachment, catch_attachment, CATCH_TYPE_OBJECT)

/**
 * SECTION:catch-attachment
 * @title: CatchAttachment
 * @short_description: File attachments for notes.
 *
 * #CatchAttachment represents an attachment to a note. It contains a file
 * as well as some information about the file. If the file has not yet
 * been downloaded, then the "local-uri" property will be %NULL.
 */

struct _CatchAttachmentPrivate
{
   gchar   *content_type;
   gchar   *local_uri;
   guint64  size;
};

enum
{
   PROP_0,
   PROP_CONTENT_TYPE,
   PROP_LOCAL_URI,
   PROP_SIZE,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_attachment_get_content_type:
 * @attachment: (in): A #CatchAttachment.
 *
 * Fetches the "content-type" property for @attachment.
 *
 * Returns: The "content-type" of the attachment.
 */
const gchar *
catch_attachment_get_content_type (CatchAttachment *attachment)
{
   g_return_val_if_fail(CATCH_IS_ATTACHMENT(attachment), NULL);
   return attachment->priv->content_type;
}

static void
catch_attachment_set_content_type (CatchAttachment *attachment,
                                   const gchar     *content_type)
{
   g_return_if_fail(CATCH_IS_ATTACHMENT(attachment));

   g_free(attachment->priv->content_type);
   attachment->priv->content_type = g_strdup(content_type);
   g_object_notify_by_pspec(G_OBJECT(attachment),
                            gParamSpecs[PROP_CONTENT_TYPE]);
}

/**
 * catch_attachment_get_local_uri:
 * @attachment: (in): A #CatchAttachment.
 *
 * Gets the URI to the file represented by @attachment. If the file has not
 * yet been downloaded, then this will be %NULL.
 *
 * Returns: A URI to the file or %NULL.
 */
const gchar *
catch_attachment_get_local_uri (CatchAttachment *attachment)
{
   g_return_val_if_fail(CATCH_IS_ATTACHMENT(attachment), NULL);
   return attachment->priv->local_uri;
}

/**
 * catch_attachment_set_local_uri:
 * @attachment: (in): A #CatchAttachment.
 * @local_uri: (in): The path to the attachment as a URI.
 *
 * Sets the URI to the file as found on the local system.
 */
void
catch_attachment_set_local_uri (CatchAttachment *attachment,
                                const gchar     *local_uri)
{
   g_return_if_fail(CATCH_IS_ATTACHMENT(attachment));
   g_free(attachment->priv->local_uri);
   attachment->priv->local_uri = g_strdup(local_uri);
   g_object_notify_by_pspec(G_OBJECT(attachment),
                            gParamSpecs[PROP_LOCAL_URI]);
}

/**
 * catch_attachment_get_size:
 * @attachment: (in): A #CatchAttachment.
 *
 * Fetches the size of the attachment.
 *
 * Returns: A #guint64 representing the file size.
 */
gsize
catch_attachment_get_size (CatchAttachment *attachment)
{
   g_return_val_if_fail(CATCH_IS_ATTACHMENT(attachment), 0);
   return attachment->priv->size;
}

static void
catch_attachment_set_size (CatchAttachment *attachment,
                           guint64          size)
{
   g_return_if_fail(CATCH_IS_ATTACHMENT(attachment));
   attachment->priv->size = size;
   g_object_notify_by_pspec(G_OBJECT(attachment), gParamSpecs[PROP_SIZE]);
}

static gboolean
catch_attachment_load_from_json (CatchResource  *resource,
                                 JsonNode       *node,
                                 GError        **error)
{
   CatchAttachment *attachment = (CatchAttachment *)resource;
   JsonObject *obj;

   g_return_val_if_fail(CATCH_IS_ATTACHMENT(attachment), FALSE);
   g_return_val_if_fail(node != NULL, FALSE);

   if (CATCH_RESOURCE_CLASS(catch_attachment_parent_class)->load_from_json) {
      if (!CATCH_RESOURCE_CLASS(catch_attachment_parent_class)->load_from_json(resource, node, error)) {
         return FALSE;
      }
   }

   if (JSON_NODE_HOLDS_OBJECT(node) &&
       (obj = json_node_get_object(node))) {
      if (json_object_has_member(obj, "content-type") &&
          JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "content-type"))) {
         g_object_set(resource,
                      "content-type", json_object_get_string_member(obj, "content-type"),
                      NULL);
      }
      if (json_object_has_member(obj, "size") &&
          JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "size"))) {
         g_object_set(resource,
                      "size", json_object_get_int_member(obj, "size"),
                      NULL);
      }
   }

   return TRUE;
}

/**
 * catch_attachment_finalize:
 * @object: (in): A #CatchAttachment.
 *
 * Finalizer for a #CatchAttachment instance.  Frees any resources held by
 * the instance.
 */
static void
catch_attachment_finalize (GObject *object)
{
   CatchAttachmentPrivate *priv = CATCH_ATTACHMENT(object)->priv;

   g_free(priv->content_type);
   g_free(priv->local_uri);

   G_OBJECT_CLASS(catch_attachment_parent_class)->finalize(object);
}

/**
 * catch_attachment_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
catch_attachment_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
   CatchAttachment *attachment = CATCH_ATTACHMENT(object);

   switch (prop_id) {
   case PROP_CONTENT_TYPE:
      g_value_set_string(value, catch_attachment_get_content_type(attachment));
      break;
   case PROP_LOCAL_URI:
      g_value_set_string(value, catch_attachment_get_local_uri(attachment));
      break;
   case PROP_SIZE:
      g_value_set_uint64(value, catch_attachment_get_size(attachment));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_attachment_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
catch_attachment_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
   CatchAttachment *attachment = CATCH_ATTACHMENT(object);

   switch (prop_id) {
   case PROP_CONTENT_TYPE:
      catch_attachment_set_content_type(attachment, g_value_get_string(value));
      break;
   case PROP_LOCAL_URI:
      catch_attachment_set_local_uri(attachment, g_value_get_string(value));
      break;
   case PROP_SIZE:
      catch_attachment_set_size(attachment, g_value_get_uint64(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_attachment_class_init:
 * @klass: (in): A #CatchAttachmentClass.
 *
 * Initializes the #CatchAttachmentClass and prepares the vtable.
 */
static void
catch_attachment_class_init (CatchAttachmentClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;
   CatchResourceClass *catch_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_attachment_finalize;
   object_class->get_property = catch_attachment_get_property;
   object_class->set_property = catch_attachment_set_property;
   g_type_class_add_private(object_class, sizeof(CatchAttachmentPrivate));

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "attachments");

   catch_class = CATCH_RESOURCE_CLASS(klass);
   catch_class->load_from_json = catch_attachment_load_from_json;

   /**
    * CatchAttachment:content-type:
    *
    * The "content-type" of the attachment.
    */
   gParamSpecs[PROP_CONTENT_TYPE] =
      g_param_spec_string("content-type",
                          _("Content Type"),
                          _("The content type of the attachment."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_CONTENT_TYPE,
                                   gParamSpecs[PROP_CONTENT_TYPE]);

   /**
    * CatchAttachment:local-uri:
    *
    * The URI of where the attachment can be found on the local system.
    * If the file has not yet been downloaded, this may be a remote
    * URI or %NULL.
    */
   gParamSpecs[PROP_LOCAL_URI] =
      g_param_spec_string("local-uri",
                          _("Local URI"),
                          _("A URI to the attachment locally accessable."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_LOCAL_URI,
                                   gParamSpecs[PROP_LOCAL_URI]);

   /**
    * CatchAttachment:size:
    *
    * The size of the attachment in bytes. If this is not yet known,
    * this may be zero.
    */
   gParamSpecs[PROP_SIZE] =
      g_param_spec_uint64("size",
                          _("Size"),
                          _("The content length of the attachment."),
                          0,
                          G_MAXUINT64,
                          0,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SIZE,
                                   gParamSpecs[PROP_SIZE]);
}

/**
 * catch_attachment_init:
 * @attachment: (in): A #CatchAttachment.
 *
 * Initializes the newly created #CatchAttachment instance.
 */
static void
catch_attachment_init (CatchAttachment *attachment)
{
   attachment->priv = G_TYPE_INSTANCE_GET_PRIVATE(attachment,
                                                  CATCH_TYPE_ATTACHMENT,
                                                  CatchAttachmentPrivate);
}
