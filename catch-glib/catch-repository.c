/* catch-repository.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "catch-debug.h"
#include "catch-repository.h"

G_DEFINE_TYPE(CatchRepository, catch_repository, GOM_TYPE_REPOSITORY)

static gboolean
catch_repository_migrate (GomRepository  *repository,
                          GomAdapter     *adapter,
                          guint           version,
                          gpointer        user_data,
                          GError        **error)
{
#define EXEC_OR_FAIL(sql) \
   G_STMT_START { \
      GomCommand *c = g_object_new(GOM_TYPE_COMMAND,   \
                                   "adapter", adapter, \
                                   "sql", sql,         \
                                   NULL);              \
      if (!gom_command_execute(c, NULL, error)) {      \
         return FALSE;                                 \
      }                                                \
   } G_STMT_END

   if (version == 1) {
      EXEC_OR_FAIL("CREATE TABLE resources ("
                   " 'local-id' INTEGER PRIMARY KEY,"
                   " 'remote-id' TEXT UNIQUE,"
                   " 'created-at' TEXT,"
                   " 'created-by' TEXT,"
                   " 'created-by-name' TEXT,"
                   " 'deleted' INTEGER,"
                   " 'modified-at' TEXT,"
                   " 'pending' INTEGER,"
                   " 'server-deleted-at' TEXT,"
                   " 'server-modified-at' TEXT"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE spaces ("
                   " 'local-id' INTEGER UNIQUE,"
                   " 'name' TEXT"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE objects ("
                   " 'local-id' INTEGER UNIQUE,"
                   " 'type' TEXT,"
                   " 'text' TEXT,"
                   " 'unknown-spaces' BLOB"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE spaces_objects ("
                   " 'spaces:local-id' INTEGER,"
                   " 'objects:local-id' INTEGER"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE notes ("
                   " 'local-id' INTEGER UNIQUE"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE checkitems ("
                   " 'local-id' INTEGER UNIQUE"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE comments ("
                   " 'local-id' INTEGER UNIQUE"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE attachments ("
                   " 'local-id' INTEGER UNIQUE,"
                   " 'local-uri' TEXT,"
                   " 'content-type' TEXT,"
                   " 'size' INTEGER"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE images ("
                   " 'local-id' INTEGER UNIQUE,"
                   " 'width' INTEGER,"
                   " 'height' INTEGER"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE audio ("
                   " 'local-id' INTEGER UNIQUE,"
                   " 'duration' FLOAT"
                   ");");
      EXEC_OR_FAIL("CREATE TABLE activities ("
                   " 'local-id' INTEGER UNIQUE,"
                   " 'action' INTEGER,"
                   " 'action-type' TEXT,"
                   " 'stream-id' TEXT,"
                   " 'object-id' TEXT,"
                   " 'read' INTEGER"
                   ");");
      EXEC_OR_FAIL("CREATE INDEX resources_server_modified_at "
                   "ON resources ('server-modified-at');");
      EXEC_OR_FAIL("INSERT INTO resources ("
                   " 'local-id',"
                   " 'remote-id'"
                   ") VALUES ("
                   " 1,"
                   " 'default'"
                   ");");
      EXEC_OR_FAIL("INSERT INTO spaces ("
                   " 'local-id',"
                   " 'name'"
                   ") VALUES ("
                   " 1,"
                   " 'My Ideas'"
                   ");");
      EXEC_OR_FAIL("INSERT INTO resources ("
                   " 'local-id'"
                   ") VALUES ("
                   " 2"
                   ");");
      EXEC_OR_FAIL("INSERT INTO objects ("
                   " 'local-id',"
                   " 'type',"
                   " 'text'"
                   ") VALUES ("
                   " 2,"
                   " 'note',"
                   " 'Welcome to Catch Notes!'"
                   ");");
      EXEC_OR_FAIL("INSERT INTO notes ("
                   " 'local-id'"
                   ") VALUES ("
                   " 2"
                   ");");
   }

   return TRUE;

#undef EXEC_OR_FAIL
}

static void
catch_repository_migrate_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
   GomRepository *repository = (GomRepository *)object;
   GError *error = NULL;

   ENTRY;

   g_assert(CATCH_IS_REPOSITORY(repository));

   if (!gom_repository_migrate_finish(repository, result, &error)) {
      g_error("%s", error->message);
      EXIT;
   }

   EXIT;
}

static void
catch_repository_notify (GObject    *object,
                         GParamSpec *pspec)
{
   GomRepository *repository = (GomRepository *)object;

   ENTRY;

   g_assert(CATCH_IS_REPOSITORY(repository));

   if (!g_strcmp0(pspec->name, "adapter")) {
      gom_repository_migrate_async(repository, 1,
                                   catch_repository_migrate,
                                   catch_repository_migrate_cb, NULL);
   }

   EXIT;
}

static void
catch_repository_class_init (CatchRepositoryClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->notify = catch_repository_notify;
}

static void
catch_repository_init (CatchRepository *repository)
{
}
