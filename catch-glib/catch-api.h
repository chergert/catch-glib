/* catch-api.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_API_H
#define CATCH_API_H

#include <glib-object.h>
#include <gom/gom.h>

#include "catch-resource-group.h"
#include "catch-session.h"

G_BEGIN_DECLS

#define CATCH_TYPE_API            (catch_api_get_type())
#define CATCH_API(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_API, CatchApi))
#define CATCH_API_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_API, CatchApi const))
#define CATCH_API_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_API, CatchApiClass))
#define CATCH_IS_API(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_API))
#define CATCH_IS_API_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_API))
#define CATCH_API_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_API, CatchApiClass))
#define CATCH_API_ERROR           (catch_api_error_quark())

typedef struct _CatchApi        CatchApi;
typedef struct _CatchApiClass   CatchApiClass;
typedef struct _CatchApiPrivate CatchApiPrivate;
typedef enum   _CatchApiError   CatchApiError;

enum _CatchApiError
{
   CATCH_API_ERROR_BAD_REQUEST = 1,
   CATCH_API_ERROR_BAD_RESPONSE,
};

struct _CatchApi
{
   GObject parent;

   /*< private >*/
   CatchApiPrivate *priv;
};

struct _CatchApiClass
{
   GObjectClass parent_class;
};

gchar              *catch_api_build_url            (CatchApi             *api,
                                                    gchar                *first_part,
                                                    ...);
GQuark              catch_api_error_quark          (void) G_GNUC_CONST;
GType               catch_api_get_type             (void) G_GNUC_CONST;
CatchApi           *catch_api_new                  (CatchSession         *session);
const gchar        *catch_api_get_bearer_token     (CatchApi             *api);
guint               catch_api_get_port             (CatchApi             *api);
gboolean            catch_api_get_secure           (CatchApi             *api);
const gchar        *catch_api_get_server           (CatchApi             *api);
CatchSession       *catch_api_get_session          (CatchApi             *api);
void                catch_api_set_bearer_token     (CatchApi             *api,
                                                    const gchar          *bearer_token);
void                catch_api_set_port             (CatchApi             *api,
                                                    guint                 port);
void                catch_api_set_secure           (CatchApi             *api,
                                                    gboolean              secure);
void                catch_api_set_server           (CatchApi             *api,
                                                    const gchar          *server);
void                catch_api_get_activities_async (CatchApi             *api,
                                                    GomFilter            *filter,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
CatchResourceGroup *catch_api_get_activities_finish(CatchApi             *api,
                                                    GAsyncResult         *result,
                                                    GError              **error);
void                catch_api_get_spaces_async     (CatchApi             *api,
                                                    GomFilter            *filter,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
CatchResourceGroup *catch_api_get_spaces_finish    (CatchApi             *api,
                                                    GAsyncResult         *result,
                                                    GError              **error);
void                catch_api_get_objects_async    (CatchApi             *api,
                                                    GomFilter            *filter,
                                                    GAsyncReadyCallback   callback,
                                                    gpointer              user_data);
CatchResourceGroup *catch_api_get_objects_finish   (CatchApi             *api,
                                                    GAsyncResult         *result,
                                                    GError              **error);

G_END_DECLS

#endif /* CATCH_API_H */
