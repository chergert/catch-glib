/* catch-glib.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_GLIB_H
#define CATCH_GLIB_H

#include <glib.h>

G_BEGIN_DECLS

#include "catch-activity.h"
#include "catch-attachment.h"
#include "catch-audio.h"
#include "catch-comment.h"
#include "catch-check-item.h"
#include "catch-image.h"
#include "catch-note.h"
#include "catch-object.h"
#include "catch-resource.h"
#include "catch-resource-group.h"
#include "catch-session.h"
#include "catch-space.h"

G_END_DECLS

#endif /* CATCH_GLIB_H */
