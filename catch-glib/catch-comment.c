/* catch-comment.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "catch-comment.h"

G_DEFINE_TYPE(CatchComment, catch_comment, CATCH_TYPE_OBJECT)

/**
 * SECTION:catch-comment
 * @title: CatchComment
 * @short_description: Represents a comment on a note.
 *
 * #CatchComment represents a comment on a #CatchNote.
 */

static void
catch_comment_class_init (CatchCommentClass *klass)
{
   GomResourceClass *resource_class;

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "comments");
}

static void
catch_comment_init (CatchComment *comment)
{
}
