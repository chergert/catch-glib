/* catch-activity.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_ACTIVITY_H
#define CATCH_ACTIVITY_H

#include "catch-resource.h"

G_BEGIN_DECLS

#define CATCH_TYPE_ACTIVITY            (catch_activity_get_type())
#define CATCH_TYPE_ACTIVITY_ACTION     (catch_activity_action_get_type())
#define CATCH_ACTIVITY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_ACTIVITY, CatchActivity))
#define CATCH_ACTIVITY_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_ACTIVITY, CatchActivity const))
#define CATCH_ACTIVITY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_ACTIVITY, CatchActivityClass))
#define CATCH_IS_ACTIVITY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_ACTIVITY))
#define CATCH_IS_ACTIVITY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_ACTIVITY))
#define CATCH_ACTIVITY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_ACTIVITY, CatchActivityClass))

typedef struct _CatchActivity        CatchActivity;
typedef struct _CatchActivityClass   CatchActivityClass;
typedef struct _CatchActivityPrivate CatchActivityPrivate;

typedef enum
{
   CATCH_ACTIVITY_ADDED   = 1,
   CATCH_ACTIVITY_EDITED  = 2,
   CATCH_ACTIVITY_REMOVED = 3,
   CATCH_ACTIVITY_CLAIMED = 4,
} CatchActivityAction;

struct _CatchActivity
{
   CatchResource parent;

   /*< private >*/
   CatchActivityPrivate *priv;
};

struct _CatchActivityClass
{
   CatchResourceClass parent_class;
};

GType    catch_activity_get_type           (void) G_GNUC_CONST;
GType    catch_activity_action_get_type    (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CATCH_ACTIVITY_H */
