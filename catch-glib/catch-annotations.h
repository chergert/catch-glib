/* catch-annotations.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_ANNOTATIONS_H
#define CATCH_ANNOTATIONS_H

#include <glib-object.h>

G_BEGIN_DECLS

#define CATCH_TYPE_ANNOTATIONS            (catch_annotations_get_type())
#define CATCH_ANNOTATIONS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_ANNOTATIONS, CatchAnnotations))
#define CATCH_ANNOTATIONS_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_ANNOTATIONS, CatchAnnotations const))
#define CATCH_ANNOTATIONS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_ANNOTATIONS, CatchAnnotationsClass))
#define CATCH_IS_ANNOTATIONS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_ANNOTATIONS))
#define CATCH_IS_ANNOTATIONS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_ANNOTATIONS))
#define CATCH_ANNOTATIONS_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_ANNOTATIONS, CatchAnnotationsClass))

typedef struct _CatchAnnotations        CatchAnnotations;
typedef struct _CatchAnnotationsClass   CatchAnnotationsClass;
typedef struct _CatchAnnotationsPrivate CatchAnnotationsPrivate;

struct _CatchAnnotations
{
   GObject parent;

   /*< private >*/
   CatchAnnotationsPrivate *priv;
};

struct _CatchAnnotationsClass
{
   GObjectClass parent_class;
};

GType    catch_annotations_get_type  (void) G_GNUC_CONST;
gboolean catch_annotations_contains  (CatchAnnotations *annotations,
                                      const gchar      *key);
gboolean catch_annotations_get_value (CatchAnnotations *annotations,
                                      const gchar      *key,
                                      GValue           *value);
void     catch_annotations_set_value (CatchAnnotations *annotations,
                                      const gchar      *key,
                                      const GValue     *value);

G_END_DECLS

#endif /* CATCH_ANNOTATIONS_H */
