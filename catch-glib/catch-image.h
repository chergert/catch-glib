/* catch-image.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_IMAGE_H
#define CATCH_IMAGE_H

#include "catch-attachment.h"

G_BEGIN_DECLS

#define CATCH_TYPE_IMAGE            (catch_image_get_type())
#define CATCH_IMAGE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_IMAGE, CatchImage))
#define CATCH_IMAGE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_IMAGE, CatchImage const))
#define CATCH_IMAGE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_IMAGE, CatchImageClass))
#define CATCH_IS_IMAGE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_IMAGE))
#define CATCH_IS_IMAGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_IMAGE))
#define CATCH_IMAGE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_IMAGE, CatchImageClass))

typedef struct _CatchImage        CatchImage;
typedef struct _CatchImageClass   CatchImageClass;
typedef struct _CatchImagePrivate CatchImagePrivate;

struct _CatchImage
{
   CatchAttachment parent;

   /*< private >*/
   CatchImagePrivate *priv;
};

struct _CatchImageClass
{
   CatchAttachmentClass parent_class;
};

guint catch_image_get_height (CatchImage *image);
GType catch_image_get_type   (void) G_GNUC_CONST;
guint catch_image_get_width  (CatchImage *image);

G_END_DECLS

#endif /* CATCH_IMAGE_H */
