/* catch-image.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-image.h"

G_DEFINE_TYPE(CatchImage, catch_image, CATCH_TYPE_ATTACHMENT)

/**
 * SECTION:catch-image
 * @title: CatchImage
 * @short_description: An attachment containing an image.
 *
 * #CatchImage represents an image that has been attached to a note.
 * The #CatchAttachment parent class contains access to the image
 * file through the "local-uri" property. If the image has been
 * downloaded and the width and height were extracted, then the
 * "width" and "height" properties will be non-zero.
 *
 * Alternate representations of the image such as thumbnails and
 * scaled representations will be added in a future release.
 */

struct _CatchImagePrivate
{
   guint height;
   guint width;
};

enum
{
   PROP_0,
   PROP_HEIGHT,
   PROP_WIDTH,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_image_get_height:
 * @image: (in): A #CatchImage.
 *
 * Fetches the height of the image if it is known. If not, zero
 * is returned.
 *
 * Returns: The height of the image or zero.
 */
guint
catch_image_get_height (CatchImage *image)
{
   g_return_val_if_fail(CATCH_IS_IMAGE(image), 0);
   return image->priv->height;
}

/**
 * catch_image_set_height:
 * @image: (in): A #CatchImage.
 *
 * Sets the height of the image.
 */
void
catch_image_set_height (CatchImage *image,
                        guint       height)
{
   g_return_if_fail(CATCH_IS_IMAGE(image));
   image->priv->height = height;
   g_object_notify_by_pspec(G_OBJECT(image), gParamSpecs[PROP_HEIGHT]);
}

/**
 * catch_image_get_width:
 * @image: (in): A #CatchImage.
 *
 * Fetches the width of the image. If the width has not yet been
 * calculated, then zero is returned.
 *
 * Returns: The width, or zero.
 */
guint
catch_image_get_width (CatchImage *image)
{
   g_return_val_if_fail(CATCH_IS_IMAGE(image), 0);
   return image->priv->width;
}

/**
 * catch_image_set_width:
 * @image: (in): A #CatchImage.
 *
 * Sets the width of the image.
 */
void
catch_image_set_width (CatchImage *image,
                       guint       width)
{
   g_return_if_fail(CATCH_IS_IMAGE(image));
   image->priv->width = width;
   g_object_notify_by_pspec(G_OBJECT(image), gParamSpecs[PROP_WIDTH]);
}

/**
 * catch_image_finalize:
 * @object: (in): A #CatchImage.
 *
 * Finalizer for a #CatchImage instance.  Frees any resources held by
 * the instance.
 */
static void
catch_image_finalize (GObject *object)
{
   G_OBJECT_CLASS(catch_image_parent_class)->finalize(object);
}

/**
 * catch_image_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
catch_image_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
   CatchImage *image = CATCH_IMAGE(object);

   switch (prop_id) {
   case PROP_HEIGHT:
      g_value_set_uint(value, catch_image_get_height(image));
      break;
   case PROP_WIDTH:
      g_value_set_uint(value, catch_image_get_width(image));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_image_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
catch_image_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
   CatchImage *image = CATCH_IMAGE(object);

   switch (prop_id) {
   case PROP_HEIGHT:
      catch_image_set_height(image, g_value_get_uint(value));
      break;
   case PROP_WIDTH:
      catch_image_set_width(image, g_value_get_uint(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_image_class_init:
 * @klass: (in): A #CatchImageClass.
 *
 * Initializes the #CatchImageClass and prepares the vtable.
 */
static void
catch_image_class_init (CatchImageClass *klass)
{
   GObjectClass *object_class;
   GomResourceClass *resource_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_image_finalize;
   object_class->get_property = catch_image_get_property;
   object_class->set_property = catch_image_set_property;
   g_type_class_add_private(object_class, sizeof(CatchImagePrivate));

   resource_class = GOM_RESOURCE_CLASS(klass);
   gom_resource_class_set_table(resource_class, "images");

   /**
    * CatchImage:height:
    *
    * The height of the image in pixels. If the height is unknown,
    * this property is zero.
    */
   gParamSpecs[PROP_HEIGHT] =
      g_param_spec_uint("height",
                        _("Height"),
                        _("The images natural height."),
                        0,
                        G_MAXUINT,
                        0,
                        G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_HEIGHT,
                                   gParamSpecs[PROP_HEIGHT]);

   /**
    * CatchImage:width:
    *
    * The width of the image in pixels. If the width is unknown,
    * this property is zero.
    */
   gParamSpecs[PROP_WIDTH] =
      g_param_spec_uint("width",
                        _("Width"),
                        _("The images natural width."),
                        0,
                        G_MAXUINT,
                        0,
                        G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_WIDTH,
                                   gParamSpecs[PROP_WIDTH]);
}

/**
 * catch_image_init:
 * @image: (in): A #CatchImage.
 *
 * Initializes the newly created #CatchImage instance.
 */
static void
catch_image_init (CatchImage *image)
{
   image->priv = G_TYPE_INSTANCE_GET_PRIVATE(image,
                                             CATCH_TYPE_IMAGE,
                                             CatchImagePrivate);
}
