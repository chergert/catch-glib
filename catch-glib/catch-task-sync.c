/* catch-task-sync.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-activity.h"
#include "catch-api.h"
#include "catch-debug.h"
#include "catch-object.h"
#include "catch-repository.h"
#include "catch-space.h"
#include "catch-task-sync.h"

#define PAGINATION_SIZE 100

G_DEFINE_TYPE(CatchTaskSync, catch_task_sync, CATCH_TYPE_TASK)

struct _CatchTaskSyncPrivate
{
   CatchRepository *repository;
   CatchApi *api;
   gboolean spaces_done;
   gboolean objects_done;
   gboolean activities_done;
};

enum
{
   PROP_0,
   PROP_API,
   PROP_REPOSITORY,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

static void
catch_task_sync_get_activities_cb (GObject      *object,
                                   GAsyncResult *result,
                                   gpointer      user_data)
{
   CatchResourceGroup *group;
   CatchTaskSync *sync = user_data;
   CatchResource *activity;
   CatchApi *api = (CatchApi *)object;
   GError *error = NULL;
   guint count;
   guint i;

   ENTRY;

   g_assert(CATCH_IS_API(api));
   g_assert(CATCH_IS_TASK_SYNC(sync));

   if (!(group = catch_api_get_activities_finish(api, result, &error))) {
      catch_task_take_error(CATCH_TASK(sync), error);
      catch_task_complete_in_idle(CATCH_TASK(sync));
      GOTO(cleanup);
   }

   /*
    * Activities come back as a single request to Catch, so no need
    * to fill the resource group. (At least for now ...).
    */
   count = catch_resource_group_get_count(group);
   for (i = 0; i < count; i++) {
      activity = catch_resource_group_get_resource(group, i);
      g_assert(activity);
      g_assert(CATCH_IS_ACTIVITY(activity));
      g_object_set(activity, "repository", sync->priv->repository, NULL);
      if (!gom_resource_save_sync(GOM_RESOURCE(activity), &error)) {
         g_printerr("%s\n", error->message);
         g_clear_error(&error);
      }
   }

   sync->priv->activities_done = TRUE;

cleanup:
   g_object_unref(sync);
   g_clear_object(&group);
   catch_task_complete_in_idle(CATCH_TASK(sync));

   EXIT;
}
static void
catch_task_sync_fetch_objects_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
   CatchResourceGroup *group = (CatchResourceGroup *)object;
   CatchTaskSync *sync = user_data;
   CatchResource *resource;
   GError *error = NULL;
   guint count;
   guint next_page = 0;
   guint fetched_begin;
   guint fetched_end;
   guint i;

   ENTRY;

   g_assert(CATCH_IS_RESOURCE_GROUP(group));
   g_assert(CATCH_IS_TASK_SYNC(sync));

   /*
    * Make sure we loaded the page of objects successfully.
    */
   if (!catch_resource_group_fetch_finish(group, result, &error)) {
      catch_task_take_error(CATCH_TASK(sync), error);
      catch_task_complete_in_idle(CATCH_TASK(sync));
      g_object_unref(sync);
      EXIT;
   }

   /*
    * If there are more objects to fetch, go ahead and start that request
    * now. We will save objects to disk while the request is processing.
    */
   next_page = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(group),
                                                 "page-begin"));
   fetched_begin = next_page;
   fetched_end = next_page + PAGINATION_SIZE;
   count = catch_resource_group_get_count(group);
   if ((next_page + PAGINATION_SIZE) < count) {
      next_page += PAGINATION_SIZE;
      g_object_set_data(G_OBJECT(group), "page-begin",
                        GINT_TO_POINTER(next_page));
      catch_resource_group_fetch_async(group,
                                       next_page,
                                       PAGINATION_SIZE,
                                       catch_task_sync_fetch_objects_cb,
                                       g_object_ref(sync));
   } else {
      sync->priv->objects_done = TRUE;
   }

   /*
    * Try to save each object to our local database.
    */
   count = MIN(fetched_end, count);
   for (i = fetched_begin; i < count; i++) {
      resource = catch_resource_group_get_resource(group, i);
      g_assert(resource);
      g_assert(CATCH_IS_OBJECT(resource));
      g_object_set(resource, "repository", sync->priv->repository, NULL);
      if (!gom_resource_save_sync(GOM_RESOURCE(resource), &error)) {
         g_printerr("%s\n", error->message);
         g_clear_error(&error);
      }
   }

   /*
    * If objects are done, we can go ahead and start fetching media items.
    */
   if (sync->priv->objects_done) {
      /* TODO: Start fetching media items. We probably want to do some
       *       heuristics to determine if we want to actually download
       *       a particular item. Fetching small thumbnails is probably
       *       a good idea though.
       */
      catch_api_get_activities_async(sync->priv->api,
                                     NULL,
                                     catch_task_sync_get_activities_cb,
                                     g_object_ref(sync));
   }

   g_object_unref(sync);

   EXIT;
}

static void
catch_task_sync_get_objects_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
   CatchResourceGroup *group;
   CatchTaskSync *sync = user_data;
   CatchApi *api = (CatchApi *)object;
   GError *error = NULL;

   ENTRY;

   g_assert(CATCH_IS_API(api));
   g_assert(CATCH_IS_TASK_SYNC(sync));

   if (!(group = catch_api_get_objects_finish(api, result, &error))) {
      catch_task_take_error(CATCH_TASK(sync), error);
      catch_task_complete_in_idle(CATCH_TASK(sync));
      GOTO(cleanup);
   }

   g_object_set_data(G_OBJECT(group), "page-begin", NULL);
   catch_resource_group_fetch_async(group,
                                    0,
                                    PAGINATION_SIZE,
                                    catch_task_sync_fetch_objects_cb,
                                    g_object_ref(sync));

cleanup:
   g_object_unref(sync);
   g_clear_object(&group);

   EXIT;
}

static void
catch_task_sync_fetch_spaces_cb (GObject      *object,
                                  GAsyncResult *result,
                                  gpointer      user_data)
{
   CatchResourceGroup *group = (CatchResourceGroup *)object;
   CatchTaskSync *sync = user_data;
   CatchResource *space;
   GError *error = NULL;
   guint count;
   guint next_page = 0;
   guint fetched_begin;
   guint fetched_end;
   guint i;

   ENTRY;

   g_assert(CATCH_IS_RESOURCE_GROUP(group));
   g_assert(CATCH_IS_TASK_SYNC(sync));

   catch_task_print_tree(CATCH_TASK(sync));

   /*
    * Make sure we loaded the page of spaces successfully.
    */
   if (!catch_resource_group_fetch_finish(group, result, &error)) {
      catch_task_take_error(CATCH_TASK(sync), error);
      catch_task_complete_in_idle(CATCH_TASK(sync));
      g_object_unref(sync);
      EXIT;
   }

   /*
    * If there are more spaces to fetch, go ahead and start that request
    * now. We will save spaces to disk while the request is processing.
    */
   next_page = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(group),
                                                 "page-begin"));
   fetched_begin = next_page;
   fetched_end = next_page + PAGINATION_SIZE;
   count = catch_resource_group_get_count(group);
   if ((next_page + PAGINATION_SIZE) < count) {
      next_page += PAGINATION_SIZE;
      g_object_set_data(G_OBJECT(group), "page-begin",
                        GINT_TO_POINTER(next_page));
      catch_resource_group_fetch_async(group,
                                       next_page,
                                       PAGINATION_SIZE,
                                       catch_task_sync_fetch_spaces_cb,
                                       g_object_ref(sync));
   } else {
      sync->priv->spaces_done = TRUE;
   }

   /*
    * Try to save each space to our local database.
    */
   count = MIN(fetched_end, count);
   for (i = fetched_begin; i < count; i++) {
      space = catch_resource_group_get_resource(group, i);
      g_assert(space);
      g_assert(CATCH_IS_SPACE(space));
      g_object_set(space, "repository", sync->priv->repository, NULL);
      if (!gom_resource_save_sync(GOM_RESOURCE(space), &error)) {
         g_printerr("%s\n", error->message);
         g_clear_error(&error);
      }
   }

   /*
    * If spaces are done, we can go ahead and start fetching objects.
    */
   if (sync->priv->spaces_done) {
      catch_api_get_objects_async(sync->priv->api,
                                  NULL,
                                  catch_task_sync_get_objects_cb,
                                  g_object_ref(sync));
   }

   g_object_unref(sync);
   EXIT;
}

static void
catch_task_sync_get_spaces_cb (GObject      *object,
                                GAsyncResult *result,
                                gpointer      user_data)
{
   CatchResourceGroup *group;
   CatchTaskSync *sync = user_data;
   CatchApi *api = (CatchApi *)object;
   GError *error = NULL;

   ENTRY;

   g_assert(CATCH_IS_TASK_SYNC(sync));
   g_assert(CATCH_IS_API(api));

   if (!(group = catch_api_get_spaces_finish(api, result, &error))) {
      catch_task_take_error(CATCH_TASK(sync), error);
      catch_task_complete_in_idle(CATCH_TASK(sync));
      GOTO(cleanup);
   }

   g_object_set_data(G_OBJECT(group), "page-begin", NULL);
   catch_resource_group_fetch_async(group,
                                    0,
                                    PAGINATION_SIZE,
                                    catch_task_sync_fetch_spaces_cb,
                                    g_object_ref(sync));

cleanup:
   g_object_unref(sync);
   g_clear_object(&group);

   EXIT;
}

static void
catch_task_sync_execute (CatchTask *task)
{
   CatchTaskSync *sync = (CatchTaskSync *)task;

   ENTRY;

   g_assert(CATCH_IS_TASK_SYNC(sync));

   catch_api_get_spaces_async(sync->priv->api,
                               NULL,
                               catch_task_sync_get_spaces_cb,
                               g_object_ref(task));

   /*
    * TODO: Saving of spaces and objects is being done in the main loop.
    *       Which is bad on its own, but even worse because we aren't
    *       currently using a transaction.
    */

   g_print("EXECUTING SYNC TASK\n");

   EXIT;
}

CatchApi *
catch_task_sync_get_api (CatchTaskSync *sync)
{
   g_return_val_if_fail(CATCH_IS_TASK_SYNC(sync), NULL);
   return sync->priv->api;
}

static void
catch_task_sync_set_api (CatchTaskSync *sync,
                         CatchApi      *api)
{
   CatchTaskSyncPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_TASK_SYNC(sync));
   g_return_if_fail(!api || CATCH_IS_API(api));

   priv = sync->priv;

   g_clear_object(&priv->api);
   priv->api = api ? g_object_ref(api) : NULL;
   g_object_notify_by_pspec(G_OBJECT(sync), gParamSpecs[PROP_API]);

   EXIT;
}

CatchRepository *
catch_task_sync_get_repository (CatchTaskSync *sync)
{
   g_return_val_if_fail(CATCH_IS_TASK_SYNC(sync), NULL);
   return sync->priv->repository;
}

static void
catch_task_sync_set_repository (CatchTaskSync   *sync,
                                CatchRepository *repository)
{
   CatchTaskSyncPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_TASK_SYNC(sync));
   g_return_if_fail(!repository || CATCH_IS_REPOSITORY(repository));

   priv = sync->priv;

   g_clear_object(&priv->repository);
   priv->repository = repository ? g_object_ref(repository) : NULL;
   g_object_notify_by_pspec(G_OBJECT(sync), gParamSpecs[PROP_REPOSITORY]);

   EXIT;
}

static void
catch_task_sync_dispose (GObject *object)
{
   CatchTaskSyncPrivate *priv = CATCH_TASK_SYNC(object)->priv;

   ENTRY;

   g_clear_object(&priv->api);
   g_clear_object(&priv->repository);

   G_OBJECT_CLASS(catch_task_sync_parent_class)->dispose(object);

   EXIT;
}

static void
catch_task_sync_finalize (GObject *object)
{
   ENTRY;
   G_OBJECT_CLASS(catch_task_sync_parent_class)->finalize(object);
   EXIT;
}

static void
catch_task_sync_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
   CatchTaskSync *sync = CATCH_TASK_SYNC(object);

   switch (prop_id) {
   case PROP_API:
      g_value_set_object(value, catch_task_sync_get_api(sync));
      break;
   case PROP_REPOSITORY:
      g_value_set_object(value, catch_task_sync_get_repository(sync));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_task_sync_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
   CatchTaskSync *sync = CATCH_TASK_SYNC(object);

   switch (prop_id) {
   case PROP_API:
      catch_task_sync_set_api(sync, g_value_get_object(value));
      break;
   case PROP_REPOSITORY:
      catch_task_sync_set_repository(sync, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_task_sync_class_init (CatchTaskSyncClass *klass)
{
   GObjectClass *object_class;
   CatchTaskClass *task_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->dispose = catch_task_sync_dispose;
   object_class->finalize = catch_task_sync_finalize;
   object_class->get_property = catch_task_sync_get_property;
   object_class->set_property = catch_task_sync_set_property;
   g_type_class_add_private(object_class, sizeof(CatchTaskSyncPrivate));

   task_class = CATCH_TASK_CLASS(klass);
   task_class->execute = catch_task_sync_execute;

   gParamSpecs[PROP_API] =
      g_param_spec_object("api",
                          _("Api"),
                          _("The api to query."),
                          CATCH_TYPE_API,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_API,
                                   gParamSpecs[PROP_API]);

   gParamSpecs[PROP_REPOSITORY] =
      g_param_spec_object("repository",
                          _("Repository"),
                          _("The repository to sync to."),
                          CATCH_TYPE_REPOSITORY,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_REPOSITORY,
                                   gParamSpecs[PROP_REPOSITORY]);
}

static void
catch_task_sync_init (CatchTaskSync *sync)
{
   sync->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(sync,
                                  CATCH_TYPE_TASK_SYNC,
                                  CatchTaskSyncPrivate);
}
