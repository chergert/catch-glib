/* catch-session.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_SESSION_H
#define CATCH_SESSION_H

#include <gio/gio.h>
#include <gom/gom.h>
#include <libsoup/soup.h>

G_BEGIN_DECLS

#define CATCH_TYPE_SESSION            (catch_session_get_type())
#define CATCH_SESSION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_SESSION, CatchSession))
#define CATCH_SESSION_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_SESSION, CatchSession const))
#define CATCH_SESSION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_SESSION, CatchSessionClass))
#define CATCH_IS_SESSION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_SESSION))
#define CATCH_IS_SESSION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_SESSION))
#define CATCH_SESSION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_SESSION, CatchSessionClass))
#define CATCH_SESSION_ERROR           (catch_session_error_quark())

typedef struct _CatchSession        CatchSession;
typedef struct _CatchSessionClass   CatchSessionClass;
typedef struct _CatchSessionPrivate CatchSessionPrivate;
typedef enum   _CatchSessionEnum    CatchSessionEnum;

enum _CatchSessionEnum
{
   CATCH_SESSION_ERROR_NO_DATABASE = 1,
};

struct _CatchSession
{
   SoupSessionAsync parent;

   /*< private >*/
   CatchSessionPrivate *priv;
};

struct _CatchSessionClass
{
   SoupSessionAsyncClass parent_class;
};

CatchSession       *catch_session_new                 (void);
GQuark              catch_session_error_quark         (void) G_GNUC_CONST;
GType               catch_session_get_type            (void) G_GNUC_CONST;
const gchar        *catch_session_get_bearer_token    (CatchSession         *session);
guint               catch_session_get_port            (CatchSession         *session);
gboolean            catch_session_get_secure          (CatchSession         *session);
const gchar        *catch_session_get_server          (CatchSession         *session);
void                catch_session_set_bearer_token    (CatchSession         *session,
                                                       const gchar          *bearer_token);
void                catch_session_set_port            (CatchSession         *session,
                                                       guint                 port);
void                catch_session_set_secure          (CatchSession         *session,
                                                       gboolean              secure);
void                catch_session_set_server          (CatchSession         *session,
                                                       const gchar          *server);
void                catch_session_set_data_dir_async  (CatchSession         *session,
                                                       const gchar          *data_dir,
                                                       GAsyncReadyCallback   callback,
                                                       gpointer              user_data);
gboolean            catch_session_set_data_dir_finish (CatchSession         *session,
                                                       GAsyncResult         *result,
                                                       GError              **error);
void                catch_session_get_activities_async(CatchSession         *session,
                                                       GomFilter            *filter,
                                                       GAsyncReadyCallback   callback,
                                                       gpointer              user_data);
CatchResourceGroup *catch_session_get_activities_finish(CatchSession         *session,
                                                       GAsyncResult         *result,
                                                       GError              **error);
void                catch_session_get_spaces_async    (CatchSession         *session,
                                                       GomFilter            *filter,
                                                       GAsyncReadyCallback   callback,
                                                       gpointer              user_data);
CatchResourceGroup *catch_session_get_spaces_finish   (CatchSession         *session,
                                                       GAsyncResult         *result,
                                                       GError              **error);
void                catch_session_get_objects_async   (CatchSession         *session,
                                                       GomFilter            *filter,
                                                       GAsyncReadyCallback   callback,
                                                       gpointer              user_data);
CatchResourceGroup *catch_session_get_objects_finish  (CatchSession         *session,
                                                       GAsyncResult         *result,
                                                       GError              **error);
void                catch_session_sync_async          (CatchSession         *session,
                                                       GAsyncReadyCallback   callback,
                                                       gpointer              user_data);
gboolean            catch_session_sync_finish         (CatchSession         *session,
                                                       GAsyncResult         *result,
                                                       GError              **error);

G_END_DECLS

#endif /* CATCH_SESSION_H */
