/* catch-audio.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_AUDIO_H
#define CATCH_AUDIO_H

#include "catch-attachment.h"

G_BEGIN_DECLS

#define CATCH_TYPE_AUDIO            (catch_audio_get_type())
#define CATCH_AUDIO(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_AUDIO, CatchAudio))
#define CATCH_AUDIO_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_AUDIO, CatchAudio const))
#define CATCH_AUDIO_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_AUDIO, CatchAudioClass))
#define CATCH_IS_AUDIO(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_AUDIO))
#define CATCH_IS_AUDIO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_AUDIO))
#define CATCH_AUDIO_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_AUDIO, CatchAudioClass))

typedef struct _CatchAudio        CatchAudio;
typedef struct _CatchAudioClass   CatchAudioClass;
typedef struct _CatchAudioPrivate CatchAudioPrivate;

struct _CatchAudio
{
   CatchAttachment parent;

   /*< private >*/
   CatchAudioPrivate *priv;
};

struct _CatchAudioClass
{
   CatchAttachmentClass parent_class;
};

GType   catch_audio_get_type     (void) G_GNUC_CONST;
gdouble catch_audio_get_duration (CatchAudio *audio);
void    catch_audio_set_duration (CatchAudio *audio,
                                  gdouble     duration);

G_END_DECLS

#endif /* CATCH_AUDIO_H */
