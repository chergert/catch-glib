/* catch-debug.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_DEBUG_H
#define CATCH_DEBUG_H

#include <glib.h>

G_BEGIN_DECLS

#ifndef G_LOG_LEVEL_TRACE
#define G_LOG_LEVEL_TRACE (1 << G_LOG_LEVEL_USER_SHIFT)
#endif

#ifdef CATCH_TRACE
#define TRACE(_f, ...) g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE, "  MSG: " _f, ##__VA_ARGS__)
#define ENTRY                                                       \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE,                          \
          "ENTRY: %s():%d", G_STRFUNC, __LINE__)
#define EXIT                                                        \
    G_STMT_START {                                                  \
        g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE,                      \
              " EXIT: %s():%d", G_STRFUNC, __LINE__);               \
        return;                                                     \
    } G_STMT_END
#define RETURN(_r)                                                  \
    G_STMT_START {                                                  \
    	g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE,                        \
              " EXIT: %s():%d", G_STRFUNC, __LINE__);               \
        return _r;                                                  \
    } G_STMT_END
#define GOTO(_l)                                                    \
    G_STMT_START {                                                  \
        g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE,                      \
              " GOTO: %s():%d %s", G_STRFUNC, __LINE__, #_l);       \
        goto _l;                                                    \
    } G_STMT_END
#define CASE(_l)                                                    \
    case _l:                                                        \
        g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE,                      \
              " CASE: %s():%d %s", G_STRFUNC, __LINE__, #_l)
#define BREAK                                                       \
    g_log(G_LOG_DOMAIN, G_LOG_LEVEL_TRACE,                          \
          "BREAK: %s():%d", G_STRFUNC, __LINE__);                   \
    break
#else
#define TRACE(_f, ...)
#define ENTRY
#define EXIT       return
#define RETURN(_r) return _r
#define GOTO(_l)   goto _l
#define CASE(_l)   case _l:
#define BREAK      break
#endif

#define CASE_RETURN_STR(_l) case _l: return #_l

G_END_DECLS

#endif /* CATCH_DEBUG_H */
