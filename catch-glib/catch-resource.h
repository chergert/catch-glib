/* catch-resource.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_RESOURCE_H
#define CATCH_RESOURCE_H

#include <gom/gom.h>
#include <json-glib/json-glib.h>

G_BEGIN_DECLS

#define CATCH_TYPE_RESOURCE            (catch_resource_get_type())
#define CATCH_TYPE_PENDING             (catch_pending_get_type())
#define CATCH_RESOURCE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_RESOURCE, CatchResource))
#define CATCH_RESOURCE_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_RESOURCE, CatchResource const))
#define CATCH_RESOURCE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_RESOURCE, CatchResourceClass))
#define CATCH_IS_RESOURCE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_RESOURCE))
#define CATCH_IS_RESOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_RESOURCE))
#define CATCH_RESOURCE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_RESOURCE, CatchResourceClass))
#define CATCH_RESOURCE_ERROR           (catch_resource_error_quark())

typedef struct _CatchResource        CatchResource;
typedef struct _CatchResourceClass   CatchResourceClass;
typedef struct _CatchResourcePrivate CatchResourcePrivate;
typedef enum   _CatchResourceError   CatchResourceError;
typedef enum   _CatchPending         CatchPending;

enum _CatchPending
{
   CATCH_PENDING_NONE    = 0,
   CATCH_PENDING_CREATE  = 1,
   CATCH_PENDING_DELETE  = 2,
   CATCH_PENDING_UPDATE  = 3,
   CATCH_PENDING_IN_SYNC = 1 << 2,
};

enum _CatchResourceError
{
   CATCH_RESOURCE_ERROR_INVALID_JSON = 1,
};

struct _CatchResource
{
   GomResource parent;

   /*< private >*/
   CatchResourcePrivate *priv;
};

struct _CatchResourceClass
{
   GomResourceClass parent_class;

   gboolean  (*load_from_json) (CatchResource  *resource,
                                JsonNode       *node,
                                GError        **error);
   gboolean  (*save_to_json)   (CatchResource  *resource,
                                JsonNode       *node,
                                GError        **error);
};

GType        catch_pending_get_type                (void) G_GNUC_CONST;
GQuark       catch_resource_error_quark            (void) G_GNUC_CONST;
GType        catch_resource_get_type               (void) G_GNUC_CONST;
GDateTime   *catch_resource_get_created_at         (CatchResource  *resource);
void         catch_resource_set_created_at         (CatchResource  *resource,
                                                    GDateTime      *created_at);
const gchar *catch_resource_get_created_by         (CatchResource  *resource);
void         catch_resource_set_created_by         (CatchResource  *resource,
                                                    const gchar    *created_by);
const gchar *catch_resource_get_created_by_name    (CatchResource  *resource);
void         catch_resource_set_created_by_name    (CatchResource  *resource,
                                                    const gchar    *created_by_name);
gboolean     catch_resource_get_deleted            (CatchResource  *resource);
void         catch_resource_set_deleted            (CatchResource  *resource,
                                                    gboolean        deleted);
gint64       catch_resource_get_local_id           (CatchResource  *resource);
void         catch_resource_set_local_id           (CatchResource  *resource,
                                                    gint64          local_id);
GDateTime   *catch_resource_get_modified_at        (CatchResource  *resource);
void         catch_resource_set_modified_at        (CatchResource  *resource,
                                                    GDateTime      *modified_at);
const gchar *catch_resource_get_remote_id          (CatchResource  *resource);
void         catch_resource_set_remote_id          (CatchResource  *resource,
                                                    const gchar    *remote_id);
const gchar *catch_resource_get_server_modified_at (CatchResource  *resource);
void         catch_resource_set_server_modified_at (CatchResource  *resource,
                                                    const gchar    *server_modified_at);
gboolean     catch_resource_load_from_json         (CatchResource  *resource,
                                                    JsonNode       *node,
                                                    GError        **error);

G_END_DECLS

#endif /* CATCH_RESOURCE_H */
