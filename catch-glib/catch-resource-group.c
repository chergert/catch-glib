/* catch-resource-group.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <gom/gom.h>

#include "catch-activity.h"
#include "catch-api.h"
#include "catch-audio.h"
#include "catch-check-item.h"
#include "catch-comment.h"
#include "catch-debug.h"
#include "catch-image.h"
#include "catch-message-builder.h"
#include "catch-object.h"
#include "catch-note.h"
#include "catch-resource-group.h"
#include "catch-session.h"
#include "catch-space.h"

G_DEFINE_TYPE(CatchResourceGroup, catch_resource_group, G_TYPE_OBJECT)

/**
 * SECTION:catch-resource-group
 * @title: CatchResourceGroup
 * @short_description: Collections of #CatchResource<!-- -->s
 *
 * #CatchResourceGroup represents a collection of #CatchResource instances.
 * This is used when searching for spaces, notes or similar.
 * #CatchResourceGroup is used whether you are searching against the Catch
 * hosted service or a local SQLite database.
 *
 * Since both disk usage and remote API calls are non-deterministic in
 * running time, the operation of loading them into memory is done
 * asynchronously. Use catch_resource_group_fetch_async() to ensure the
 * range of resources you intend to work with are loaded into memory.
 */

struct _CatchResourceGroupPrivate
{
   GomResourceGroup *group;
   CatchSession *session;
   GHashTable *resources;
   GomFilter *filter;
   GType resource_type;
   guint count;
   guint first_page_size;
};

enum
{
   PROP_0,
   PROP_COUNT,
   PROP_FILTER,
   PROP_GROUP,
   PROP_RESOURCE_TYPE,
   PROP_SESSION,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

static void
catch_resource_group_set_group (CatchResourceGroup *group,
                                GomResourceGroup   *gom_group)
{
   CatchResourceGroupPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(!gom_group || GOM_IS_RESOURCE_GROUP(gom_group));

   priv = group->priv;

   g_clear_object(&priv->group);
   priv->count = 0;

   if (gom_group) {
      priv->group = g_object_ref(gom_group);
      priv->count = gom_resource_group_get_count(gom_group);
   }

   g_object_notify_by_pspec(G_OBJECT(group), gParamSpecs[PROP_COUNT]);
   g_object_notify_by_pspec(G_OBJECT(group), gParamSpecs[PROP_GROUP]);

   EXIT;
}

/**
 * catch_resource_group_get_count:
 * @group: (in): A #CatchResourceGroup.
 *
 * Fetches the number of #CatchResource instances that are in @group. Make
 * sure the instances are loaded into memory using
 * catch_resource_group_fetch_async() before accessing them with
 * catch_resource_group_get_resource().
 *
 * Returns: The number of resources in @group.
 */
guint
catch_resource_group_get_count (CatchResourceGroup *group)
{
   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), 0);
   return group->priv->count;
}

/**
 * catch_resource_group_get_resource:
 * @group: (in): A #CatchResourceGroup.
 * @index_: (in): The index of the resource to retrieve.
 *
 * Fetches the resource found at @index_. catch_resource_group_fetch_async()
 * must have been called to load a range including this resource.
 *
 * Returns: (transfer none): A #GomResource.
 */
CatchResource *
catch_resource_group_get_resource (CatchResourceGroup *group,
                                   guint               index_)
{
   CatchResourceGroupPrivate *priv;
   CatchResource *ret = NULL;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), NULL);

   priv = group->priv;

   if (priv->group) {
      ret = CATCH_RESOURCE(gom_resource_group_get_index(priv->group, index_));
   } else {
      ret = g_hash_table_lookup(priv->resources, &index_);
   }

   RETURN(ret);
}

static void
catch_resource_group_set_resource (CatchResourceGroup *group,
                                   guint               index_,
                                   CatchResource      *resource)
{
   CatchResourceGroupPrivate *priv;
   guint *key;

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(CATCH_IS_RESOURCE(resource));

   priv = group->priv;

   if (!priv->resources) {
      priv->resources = g_hash_table_new_full(g_int_hash, g_int_equal,
                                              g_free, g_object_unref);
   }

   key = g_new0(guint, 1);
   *key = index_;
   g_hash_table_replace(priv->resources, key, g_object_ref(resource));

   EXIT;
}

static void
catch_resource_group_fetch_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
   CatchResourceGroupPrivate *priv;
   CatchResourceGroup *group;
   GSimpleAsyncResult *simple = user_data;
   gboolean ret = FALSE;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));
   group = CATCH_RESOURCE_GROUP(g_async_result_get_source_object(user_data));
   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));

   priv = group->priv;

   if (priv->group) {
      ret = gom_resource_group_fetch_finish(GOM_RESOURCE_GROUP(object), result, &error);
   } else {
      g_assert_not_reached();
   }

   if (!ret) {
      g_simple_async_result_take_error(simple, error);
   }

   g_simple_async_result_set_op_res_gboolean(simple, ret);
   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

static GType
object_peek_type (JsonNode *node)
{
   JsonObject *obj;
   const gchar *type_str;

   if (!node ||
       !JSON_NODE_HOLDS_OBJECT(node) ||
       !(obj = json_node_get_object(node)) ||
       !json_object_has_member(obj, "type") ||
       !JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "type")) ||
       !(type_str = json_object_get_string_member(obj, "type"))) {
      return FALSE;
   }

   if (!g_strcmp0(type_str, "note")) {
      return CATCH_TYPE_NOTE;
   } else if (!g_strcmp0(type_str, "image")) {
      return CATCH_TYPE_IMAGE;
   } else if (!g_strcmp0(type_str, "audio")) {
      return CATCH_TYPE_AUDIO;
   } else if (!g_strcmp0(type_str, "comment")) {
      return CATCH_TYPE_COMMENT;
   } else if (!g_strcmp0(type_str, "attachment")) {
      return CATCH_TYPE_ATTACHMENT;
   } else if (!g_strcmp0(type_str, "checkitem")) {
      return CATCH_TYPE_CHECK_ITEM;
   }

   return G_TYPE_NONE;
}

static JsonNode *
catch_resource_group_api_parse_response (CatchResourceGroup   *group,
                                         SoupMessage          *message,
                                         GError              **error)
{
   const gchar *content_type;
   const gchar *error_message = NULL;
   const gchar *status = NULL;
   JsonParser *parser;
   JsonObject *obj = NULL;
   JsonNode *result = NULL;
   JsonNode *root;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), NULL);
   g_return_val_if_fail(SOUP_IS_MESSAGE(message), NULL);

   /*
    * If the result isn't application/json, something really wrong happened.
    */
   content_type = soup_message_headers_get_one(message->response_headers,
                                               "Content-Type");
   if (!!g_strcmp0("application/json", content_type)) {
      g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_REQUEST,
                  _("The API request failed for unknown reasons; HTTP %d."),
                  message->status_code);
      RETURN(NULL);
   }

   /*
    * Create a JSON parser to extract the result object from the response. Load
    * the content from the response body.
    */
   parser = json_parser_new();
   if (!json_parser_load_from_data(parser, message->response_body->data,
                                   message->response_body->length, error)) {
      g_object_unref(parser);
      RETURN(NULL);
   }

   /*
    * Check to see if the response is improperly formatted or an error.
    */
   root = json_parser_get_root(parser);
   if (!(root = json_parser_get_root(parser)) ||
       !JSON_NODE_HOLDS_OBJECT(root) ||
       !(obj = json_node_get_object(root)) ||
       !json_object_has_member(obj, "status") ||
       !JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "status")) ||
       !(status = json_object_get_string_member(obj, "status")) ||
       !!g_strcmp0("ok", status)) {
      if (obj &&
          json_object_has_member(obj, "error") &&
          JSON_NODE_HOLDS_OBJECT(json_object_get_member(obj, "error")) &&
          (obj = json_object_get_object_member(obj, "error")) &&
          json_object_has_member(obj, "message") &&
          JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "message")) &&
          (error_message = json_object_get_string_member(obj, "message"))) {
         g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_REQUEST,
                     _("%s"), error_message);
      } else {
         g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_REQUEST,
                     _("The API request failed for unknown reasons; HTTP %d."),
                     message->status_code);
      }
      g_object_unref(parser);
      RETURN(NULL);
   }

   /*
    * Try to extract the result node from the response.
    */
   if (obj && json_object_has_member(obj, "result")) {
      if ((result = json_object_get_member(obj, "result"))) {
         result = json_node_copy(result);
      }
   }

   g_object_unref(parser);

   RETURN(result);
}

static gboolean
catch_resource_group_api_parse_activities (CatchResourceGroup  *group,
                                           JsonNode            *result,
                                           guint               *result_offset,
                                           guint               *n_resources,
                                           GError             **error)
{
   CatchResourceGroupPrivate *priv;
   CatchResource *resource;
   JsonObject *obj;
   JsonArray *array;
   JsonNode *element;
   gboolean set_first_page = FALSE;
   gboolean ret = FALSE;
   guint i;
   guint length;
   guint offset = 0;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), FALSE);
   g_return_val_if_fail(result != NULL, FALSE);

   priv = group->priv;

   g_object_freeze_notify(G_OBJECT(group));

   if (!JSON_NODE_HOLDS_OBJECT(result) ||
       !(obj = json_node_get_object(result)) ||
       !json_object_has_member(obj, "activities") ||
       !JSON_NODE_HOLDS_ARRAY(json_object_get_member(obj, "activities")) ||
       !(array = json_object_get_array_member(obj, "activities"))) {
      g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_RESPONSE,
                  _("The resonse was invalid."));
      GOTO(failure);
   }

   if (json_object_has_member(obj, "count") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "count"))) {
      priv->count = json_object_get_int_member(obj, "count");
      set_first_page = TRUE;
      g_object_notify_by_pspec(G_OBJECT(group), gParamSpecs[PROP_COUNT]);
   }

   if (json_object_has_member(obj, "offset") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "offset"))) {
      offset = json_object_get_int_member(obj, "offset");
   }

   length = json_array_get_length(array);
   for (i = 0; i < length; i++) {
      element = json_array_get_element(array, i);
      resource = g_object_new(CATCH_TYPE_ACTIVITY, NULL);
      if (!catch_resource_load_from_json(resource, element, error)) {
         g_object_unref(resource);
         GOTO(failure);
      }
      catch_resource_group_set_resource(CATCH_RESOURCE_GROUP(group),
                                        offset + i, resource);
      g_object_unref(resource);
   }

   if (set_first_page) {
      priv->first_page_size = length;
   }

   if (result_offset) {
      *result_offset = offset;
   }

   if (n_resources) {
      *n_resources = length;
   }

   ret = TRUE;

failure:
   g_object_thaw_notify(G_OBJECT(group));

   RETURN(ret);
}

static gboolean
catch_resource_group_api_parse_spaces (CatchResourceGroup  *group,
                                        JsonNode            *result,
                                        guint               *result_offset,
                                        guint               *n_resources,
                                        GError             **error)
{
   CatchResourceGroupPrivate *priv;
   CatchResource *resource;
   JsonObject *obj;
   JsonArray *array;
   JsonNode *element;
   gboolean set_first_page = FALSE;
   gboolean ret = FALSE;
   guint i;
   guint length;
   guint offset = 0;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), FALSE);
   g_return_val_if_fail(result != NULL, FALSE);

   priv = group->priv;

   g_object_freeze_notify(G_OBJECT(group));

   if (!JSON_NODE_HOLDS_OBJECT(result) ||
       !(obj = json_node_get_object(result)) ||
       !json_object_has_member(obj, "streams") ||
       !JSON_NODE_HOLDS_ARRAY(json_object_get_member(obj, "streams")) ||
       !(array = json_object_get_array_member(obj, "streams"))) {
      g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_RESPONSE,
                  _("The resonse was invalid."));
      GOTO(failure);
   }

   if (json_object_has_member(obj, "count") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "count"))) {
      priv->count = json_object_get_int_member(obj, "count");
      set_first_page = TRUE;
      g_object_notify_by_pspec(G_OBJECT(group), gParamSpecs[PROP_COUNT]);
   }

   if (json_object_has_member(obj, "offset") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "offset"))) {
      offset = json_object_get_int_member(obj, "offset");
   }

   length = json_array_get_length(array);
   for (i = 0; i < length; i++) {
      element = json_array_get_element(array, i);
      resource = g_object_new(CATCH_TYPE_SPACE, NULL);
      if (!catch_resource_load_from_json(resource, element, error)) {
         g_object_unref(resource);
         GOTO(failure);
      }
      catch_resource_group_set_resource(CATCH_RESOURCE_GROUP(group),
                                        offset + i, resource);
      g_object_unref(resource);
   }

   if (set_first_page) {
      priv->first_page_size = length;
   }

   if (result_offset) {
      *result_offset = offset;
   }

   if (n_resources) {
      *n_resources = length;
   }

   ret = TRUE;

failure:
   g_object_thaw_notify(G_OBJECT(group));

   RETURN(ret);
}

static gboolean
catch_resource_group_api_parse_objects (CatchResourceGroup  *group,
                                        JsonNode            *result,
                                        guint               *result_offset,
                                        guint               *n_resources,
                                        GError             **error)
{
   CatchResourceGroupPrivate *priv;
   CatchResource *resource;
   JsonObject *obj;
   JsonArray *array;
   JsonNode *element;
   gboolean set_first_page = FALSE;
   gboolean ret = FALSE;
   GType resource_type;
   guint i;
   guint length;
   guint offset = 0;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), FALSE);
   g_return_val_if_fail(result != NULL, FALSE);

   priv = group->priv;

   g_object_freeze_notify(G_OBJECT(group));

   if (!JSON_NODE_HOLDS_OBJECT(result) ||
       !(obj = json_node_get_object(result)) ||
       !json_object_has_member(obj, "objects") ||
       !JSON_NODE_HOLDS_ARRAY(json_object_get_member(obj, "objects")) ||
       !(array = json_object_get_array_member(obj, "objects"))) {
      g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_RESPONSE,
                  _("The resonse did not contain the objects array."));
      GOTO(failure);
   }

   if (json_object_has_member(obj, "count") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "count"))) {
      priv->count = json_object_get_int_member(obj, "count");
      set_first_page = TRUE;
      g_object_notify(G_OBJECT(group), "count");
   }

   if (json_object_has_member(obj, "offset") &&
       JSON_NODE_HOLDS_VALUE(json_object_get_member(obj, "offset"))) {
      offset = json_object_get_int_member(obj, "offset");
   }

   length = json_array_get_length(array);
   for (i = 0; i < length; i++) {
      element = json_array_get_element(array, i);
      if (!(resource_type = object_peek_type(element))) {
         g_set_error(error, CATCH_API_ERROR, CATCH_API_ERROR_BAD_RESPONSE,
                     _("The JSON object did not contain a \"type\" field."));
         GOTO(failure);
      }
      resource = g_object_new(resource_type, NULL);
      g_assert(CATCH_IS_RESOURCE(resource));
      if (!catch_resource_load_from_json(resource, element, error)) {
         g_object_unref(resource);
         GOTO(failure);
      }
      catch_resource_group_set_resource(CATCH_RESOURCE_GROUP(group),
                                        offset + i, resource);
      g_object_unref(resource);
   }

   if (set_first_page) {
      priv->first_page_size = length;
   }

   if (result_offset) {
      *result_offset = offset;
   }

   if (n_resources) {
      *n_resources = length;
   }

   ret = TRUE;

failure:
   g_object_thaw_notify(G_OBJECT(group));

   RETURN(ret);
}

static CatchMessageBuilder *
catch_resource_group_get_builder (CatchResourceGroup *group)
{
   CatchResourceGroupPrivate *priv;
   CatchMessageBuilder *builder;
   const gchar *bearer_token = NULL;
   const gchar *server;
   gboolean secure;
   guint port;

   ENTRY;

   g_assert(CATCH_IS_RESOURCE_GROUP(group));

   priv = group->priv;

   server = catch_session_get_server(priv->session);
   port = catch_session_get_port(priv->session);
   secure = catch_session_get_secure(priv->session);
   bearer_token = catch_session_get_bearer_token(priv->session);

   builder = catch_message_builder_new("GET");
   catch_message_builder_set_server(builder, server,
                                    secure ? "https" : "http",
                                    port ? port : (secure ? 443 : 80));
   catch_message_builder_add_string_param(builder, "bearer_token", bearer_token);
   catch_message_builder_add_bool_param(builder, "full", TRUE);

   RETURN(builder);
}

static void
catch_resource_group_api_fetch_cb (SoupSession *session,
                                   SoupMessage *message,
                                   gpointer     user_data)
{
   CatchResourceGroupPrivate *priv;
   CatchMessageBuilder *builder;
   GSimpleAsyncResult *simple = user_data;
   CatchResourceGroup *group;
   const gchar *uri;
   JsonNode *node = NULL;
   GError *error = NULL;
   guint offset;
   guint n_resources;
   guint count;
   guint index_;

   ENTRY;

   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));
   group = CATCH_RESOURCE_GROUP(g_async_result_get_source_object(user_data));
   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));

   priv = group->priv;

   if (!(node = catch_resource_group_api_parse_response(group, message, &error))) {
      g_simple_async_result_take_error(simple, error);
      GOTO(finish);
   }

   if (g_type_is_a(priv->resource_type, CATCH_TYPE_SPACE)) {
      if (!catch_resource_group_api_parse_spaces(group, node, &offset, &n_resources, &error)) {
         g_simple_async_result_take_error(simple, error);
         GOTO(finish);
      }
      g_simple_async_result_set_op_res_gboolean(simple, TRUE);
   } else if (g_type_is_a(priv->resource_type, CATCH_TYPE_OBJECT)) {
      if (!catch_resource_group_api_parse_objects(group, node, &offset, &n_resources, &error)) {
         g_simple_async_result_take_error(simple, error);
         GOTO(finish);
      }
      g_simple_async_result_set_op_res_gboolean(simple, TRUE);
   } else if (g_type_is_a(priv->resource_type, CATCH_TYPE_ACTIVITY)) {
      if (!catch_resource_group_api_parse_activities(group, node, &offset, &n_resources, &error)) {
         g_simple_async_result_take_error(simple, error);
         GOTO(finish);
      }
      g_simple_async_result_set_op_res_gboolean(simple, TRUE);
   } else {
      g_assert_not_reached();
   }

   /*
    * If we did not satisfy the range of items to load on this query, we need
    * to start another one.
    */
   count = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(simple), "count"));
   index_ = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(simple), "index"));

   if ((offset + n_resources) < (index_ + count)) {
      offset += n_resources;
      builder = catch_resource_group_get_builder(group);
      catch_message_builder_add_int_param(builder, "offset", offset);
      uri = soup_uri_get_path(soup_message_get_uri(message));
      catch_message_builder_set_path(builder, uri, NULL);
      message = catch_message_builder_build(builder);
      /*
       * TODO: Limit range to what we need, rather than server max.
       */
      soup_session_queue_message(SOUP_SESSION(priv->session), message,
                                 catch_resource_group_api_fetch_cb,
                                 simple);
      catch_message_builder_unref(builder);
      if (node) {
         json_node_free(node);
      }
      EXIT;
   }

   g_simple_async_result_set_op_res_gboolean(simple, TRUE);

finish:
   if (node) {
      json_node_free(node);
   }
   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

static void
catch_resource_group_api_fetch_async (CatchResourceGroup  *group,
                                      guint                index_,
                                      guint                count,
                                      GAsyncReadyCallback  callback,
                                      gpointer             user_data)
{
   CatchResourceGroupPrivate *priv;
   CatchMessageBuilder *builder;
   GSimpleAsyncResult *simple;
   SoupMessage *message;
   const gchar *space = NULL; /* TODO */

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(callback != NULL);

   priv = group->priv;

   g_assert(priv->resource_type);
   g_assert(priv->session);

   if (count && ((index_ + count) > priv->count)) {
      count = priv->count - index_;
   }

   /*
    * If we can satisfy the request immediately, do so.
    */
   if (priv->first_page_size && priv->first_page_size >= (index_ + count)) {
      simple = g_simple_async_result_new(G_OBJECT(group), callback, user_data,
                                         catch_resource_group_fetch_async);
      g_simple_async_result_set_op_res_gboolean(simple, TRUE);
      g_simple_async_result_complete_in_idle(simple);
      g_object_unref(simple);
      EXIT;
   }

   /*
    * Ignore the first page of results if we have already fetched them.
    */
   if (!index_) {
      index_ = priv->first_page_size;
      count -= priv->first_page_size;
   }

   simple = g_simple_async_result_new(G_OBJECT(group), callback, user_data,
                                      catch_resource_group_fetch_async);
   g_object_set_data(G_OBJECT(simple), "index", GINT_TO_POINTER(index_));
   g_object_set_data(G_OBJECT(simple), "count", GINT_TO_POINTER(count));

   builder = catch_resource_group_get_builder(group);

   if (index_) {
      catch_message_builder_add_int_param(builder, "offset", index_);
   }

   if (count) {
      catch_message_builder_add_int_param(builder, "limit", count);
   }

   /*
    * TODO: Filtering.
    */
#if 0
   filter = catch_resource_group_get_filter(set);

   if (filter && (search_query = catch_resource_filter_get_search_query(filter))) {
      catch_message_builder_add_string_param(builder, "q", search_query);
   }

   if (filter && (sma = catch_resource_filter_get_server_modified_at(filter))) {
      catch_message_builder_add_string_param(builder, "server_modified_at$gte", sma);
   }
#endif

   /*
    * TODO: If the whole range doesn't come back on our request, we should pump
    *       the next request.
    */

   if (g_type_is_a(priv->resource_type, CATCH_TYPE_SPACE)) {
      catch_message_builder_set_path(builder, "v3", "streams", NULL);
   } else if (g_type_is_a(priv->resource_type, CATCH_TYPE_OBJECT)) {
#if 0
      if (filter) {
         if ((spaces = catch_resource_filter_get_spaces(filter))) {
            space = spaces[0];
         }
      }
#endif
      catch_message_builder_set_path(builder, "v3", "streams",
                                     space ? space : "sync",
                                     NULL);
   } else if (g_type_is_a(priv->resource_type, CATCH_TYPE_ACTIVITY)) {
      catch_message_builder_set_path(builder, "v3", "activities", NULL);
   } else {
      g_assert_not_reached();
   }

   message = catch_message_builder_build(builder);
   soup_session_queue_message(SOUP_SESSION(priv->session), message,
                              catch_resource_group_api_fetch_cb,
                              simple);
   catch_message_builder_unref(builder);

   EXIT;
}

/**
 * catch_resource_group_fetch_async:
 * @group: (in): A #CatchResourceGroup.
 * @index_: (in): The start index to fetch.
 * @count: (in): The number of resources to fetch starting from @index_.
 * @callback: (in): A callback to execute upon completion.
 * @user_data: (in): User data for @callback.
 *
 * Asynchronously requests that @count resources starting from @index_ be
 * loaded from either the remote API or the local SQLite database. @callback
 * is responsible for calling catch_resource_group_fetch_finish().
 *
 * Once this operation has completed, you may access the loaded resource
 * using catch_resource_group_get_resource().
 */
void
catch_resource_group_fetch_async (CatchResourceGroup  *group,
                                  guint                index_,
                                  guint                count,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
   CatchResourceGroupPrivate *priv;
   GSimpleAsyncResult *simple;

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(callback != NULL);

   priv = group->priv;

   simple = g_simple_async_result_new(G_OBJECT(group), callback, user_data,
                                      catch_resource_group_fetch_async);

   if (priv->group) {
      gom_resource_group_fetch_async(priv->group, index_, count,
                                     catch_resource_group_fetch_cb,
                                     simple);
   } else {
      catch_resource_group_api_fetch_async(group, index_, count,
                                           callback, user_data);
   }

   EXIT;
}

/**
 * catch_resource_group_fetch_finish:
 * @group: (in): A #CatchResourceGroup.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A locationf or a #GError, or %NULL.
 *
 * Completes an asynchronous request to catch_resource_group_fetch_async().
 * %TRUE is returned if the resources were loaded, otherwise %FALSE and
 * @error is set.
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
catch_resource_group_fetch_finish (CatchResourceGroup  *group,
                                   GAsyncResult        *result,
                                   GError             **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   gboolean ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_RESOURCE_GROUP(group), FALSE);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple), FALSE);

   if (!(ret = g_simple_async_result_get_op_res_gboolean(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   }

   RETURN(ret);
}

static void
catch_resource_group_finalize (GObject *object)
{
   CatchResourceGroupPrivate *priv;

   ENTRY;
   priv = CATCH_RESOURCE_GROUP(object)->priv;
   g_clear_object(&priv->group);
   g_clear_object(&priv->session);
   g_hash_table_unref(priv->resources);
   g_clear_object(&priv->filter);
   G_OBJECT_CLASS(catch_resource_group_parent_class)->finalize(object);
   EXIT;
}

static void
catch_resource_group_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
   CatchResourceGroup *group = CATCH_RESOURCE_GROUP(object);

   switch (prop_id) {
   case PROP_COUNT:
      g_value_set_uint(value, group->priv->count);
      break;
   case PROP_FILTER:
      g_value_set_object(value, group->priv->filter);
      break;
   case PROP_GROUP:
      g_value_set_object(value, group->priv->group);
      break;
   case PROP_RESOURCE_TYPE:
      g_value_set_gtype(value, group->priv->resource_type);
      break;
   case PROP_SESSION:
      g_value_set_object(value, group->priv->session);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_resource_group_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
   CatchResourceGroup *group = CATCH_RESOURCE_GROUP(object);

   switch (prop_id) {
   case PROP_FILTER:
      group->priv->filter = g_value_dup_object(value);
      g_object_notify_by_pspec(object, pspec);
      break;
   case PROP_GROUP:
      catch_resource_group_set_group(group, g_value_get_object(value));
      break;
   case PROP_RESOURCE_TYPE:
      group->priv->resource_type = g_value_get_gtype(value);
      g_object_notify_by_pspec(object, pspec);
      break;
   case PROP_SESSION:
      group->priv->session = g_value_dup_object(value);
      g_object_notify_by_pspec(object, pspec);
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_resource_group_class_init (CatchResourceGroupClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_resource_group_finalize;
   object_class->get_property = catch_resource_group_get_property;
   object_class->set_property = catch_resource_group_set_property;
   g_type_class_add_private(object_class, sizeof(CatchResourceGroupPrivate));

   /**
    * CatchResourceGroup:count:
    *
    * The number of resources in the #CatchResourceGroup.
    */
   gParamSpecs[PROP_COUNT] =
      g_param_spec_uint("count",
                        _("Count"),
                        _("The number of items in the resource group."),
                        0,
                        G_MAXUINT,
                        0,
                        G_PARAM_READABLE);
   g_object_class_install_property(object_class, PROP_COUNT,
                                   gParamSpecs[PROP_COUNT]);

   /**
    * CatchResourceGroup:filter:
    *
    * A filter used to search for resources.
    */
   gParamSpecs[PROP_FILTER] =
      g_param_spec_object("filter",
                          _("Filter"),
                          _("The GomFilter for the query."),
                          GOM_TYPE_FILTER,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_FILTER,
                                   gParamSpecs[PROP_FILTER]);

   /**
    * CatchResourceGroup:group:
    *
    * The handle to the GomResourceGroup used for accessing items
    * from the SQLite database.
    */
   gParamSpecs[PROP_GROUP] =
      g_param_spec_object("group",
                          _("Group"),
                          _("The underlying GomResourceGroup."),
                          GOM_TYPE_RESOURCE_GROUP,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_GROUP,
                                   gParamSpecs[PROP_GROUP]);

   /**
    * CatchResourceGroup:resource-type:
    *
    * The #GType of #CatchResource instances to fetch.
    */
   gParamSpecs[PROP_RESOURCE_TYPE] =
      g_param_spec_gtype("resource-type",
                          _("Resource Type"),
                          _("The object resource type."),
                          CATCH_TYPE_RESOURCE,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_RESOURCE_TYPE,
                                   gParamSpecs[PROP_RESOURCE_TYPE]);

   /**
    * CatchResourceGroup:session:
    *
    * The #CatchSession to use for querying and credentials.
    */
   gParamSpecs[PROP_SESSION] =
      g_param_spec_object("session",
                          _("Session"),
                          _("The CatchSession."),
                          CATCH_TYPE_SESSION,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_SESSION,
                                   gParamSpecs[PROP_SESSION]);
}

static void
catch_resource_group_init (CatchResourceGroup *group)
{
   group->priv = G_TYPE_INSTANCE_GET_PRIVATE(group,
                                             CATCH_TYPE_RESOURCE_GROUP,
                                             CatchResourceGroupPrivate);
}
