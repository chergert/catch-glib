/* catch-repository.h
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CATCH_REPOSITORY_H
#define CATCH_REPOSITORY_H

#include <gom/gom.h>

G_BEGIN_DECLS

#define CATCH_TYPE_REPOSITORY            (catch_repository_get_type())
#define CATCH_REPOSITORY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_REPOSITORY, CatchRepository))
#define CATCH_REPOSITORY_CONST(obj)      (G_TYPE_CHECK_INSTANCE_CAST ((obj), CATCH_TYPE_REPOSITORY, CatchRepository const))
#define CATCH_REPOSITORY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass),  CATCH_TYPE_REPOSITORY, CatchRepositoryClass))
#define CATCH_IS_REPOSITORY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CATCH_TYPE_REPOSITORY))
#define CATCH_IS_REPOSITORY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  CATCH_TYPE_REPOSITORY))
#define CATCH_REPOSITORY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj),  CATCH_TYPE_REPOSITORY, CatchRepositoryClass))

typedef struct _CatchRepository        CatchRepository;
typedef struct _CatchRepositoryClass   CatchRepositoryClass;
typedef struct _CatchRepositoryPrivate CatchRepositoryPrivate;

struct _CatchRepository
{
   GomRepository parent;

   /*< private >*/
   CatchRepositoryPrivate *priv;
};

struct _CatchRepositoryClass
{
   GomRepositoryClass parent_class;
};

GType catch_repository_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* CATCH_REPOSITORY_H */
