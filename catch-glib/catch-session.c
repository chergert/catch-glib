/* catch-session.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-activity.h"
#include "catch-api.h"
#include "catch-debug.h"
#include "catch-object.h"
#include "catch-repository.h"
#include "catch-session.h"
#include "catch-space.h"
#include "catch-task-sync.h"

G_DEFINE_TYPE(CatchSession, catch_session, SOUP_TYPE_SESSION_ASYNC)

/**
 * SECTION:catch-session
 * @title: CatchSession
 * @short_description: Session manager for interacting with Catch.
 *
 * #CatchSession is the toplevel #GObject you will work with when interacting
 * with the Catch-GLib library. It abstracts interacting with the Catch REST
 * API as well as the synchronization protocol between the client and server.
 *
 * You can query the API service directly using #CatchSession, or query the
 * local database and synchronize in the background.
 *
 * To query locally, but synchronize with the Catch hosted service, you
 * will need to set the "bearer-token" property and set the directory for
 * the database using catch_session_set_data_dir_async().
 *
 * To query the API directly, simply set the "bearer-token" property and do
 * not load a database directory.
 */

struct _CatchSessionPrivate
{
   CatchApi        *api;
   CatchRepository *repository;
   gboolean         offline;
   gchar           *data_dir;
};

enum
{
   PROP_0,
   PROP_BEARER_TOKEN,
   PROP_PORT,
   PROP_SECURE,
   PROP_SERVER,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

/**
 * catch_session_new:
 *
 * Creates a new #CatchSession.
 *
 * Returns: A #CatchSession which should be freed with g_object_unref().
 */
CatchSession *
catch_session_new (void)
{
   ENTRY;
   RETURN(g_object_new(CATCH_TYPE_SESSION, NULL));
}

/**
 * catch_session_get_data_dir:
 * @session: (in): A #CatchSession.
 *
 * Fetches the path to the directory used to store the local database
 * for user content.
 *
 * Returns: A string containing the data directory.
 */
const gchar *
catch_session_get_data_dir (CatchSession *session)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_SESSION(session), NULL);
   RETURN(session->priv->data_dir);
}

/**
 * catch_session_get_server:
 * @session: (in): A #CatchSession.
 *
 * Fetches the name of the server in which to connect.
 *
 * Returns: A string containing the server.
 */
const gchar *
catch_session_get_server (CatchSession *session)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_SESSION(session), NULL);
   RETURN(catch_api_get_server(session->priv->api));
}

/**
 * catch_session_set_server:
 * @session: (in): A #CatchSession.
 * @server: (in): A string containing the server.
 *
 * Sets the server which should be connected to. This is typically
 * "catch.com". However, if you have reverse engineered the API you would
 * set this to hostname of your server.
 */
void
catch_session_set_server (CatchSession *session,
                          const gchar  *server)
{
   ENTRY;
   g_return_if_fail(CATCH_IS_SESSION(session));
   catch_api_set_server(session->priv->api, server);
   EXIT;
}

/**
 * catch_session_get_port:
 * @session: (in): A #CatchSession.
 *
 * Fetches the port to use when connecting to the Catch hosted service.
 *
 * Returns: A #guint containing the port.
 */
guint
catch_session_get_port (CatchSession *session)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_SESSION(session), 0);
   RETURN(catch_api_get_port(session->priv->api));
}

/**
 * catch_session_set_port:
 * @session: (in): A #CatchSession.
 * @port: (in): The port number.
 *
 * Sets the port to use when connecting to the Catch hosted service.
 */
void
catch_session_set_port (CatchSession *session,
                        guint         port)
{
   ENTRY;
   g_return_if_fail(CATCH_IS_SESSION(session));
   catch_api_set_port(session->priv->api, port);
   EXIT;
}

/**
 * catch_session_get_secure:
 * @session: (in): A #CatchSession.
 *
 * Fetches the "secure" property, indiciating if SSL should be used. SSL
 * must always be used with the Catch hosted service. So only set this to
 * %FALSE if you are reverse engineering the hosted service.
 *
 * Returns: %TRUE if SSL is to be used.
 */
gboolean
catch_session_get_secure (CatchSession *session)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_SESSION(session), FALSE);
   RETURN(catch_api_get_secure(session->priv->api));
}

/**
 * catch_session_set_secure:
 * @session: (in): A #CatchSession.
 * @secure: (in): If SSL should be used.
 *
 * Sets if SSL should be used when connecting to the Catch hosted service.
 */
void
catch_session_set_secure (CatchSession *session,
                          gboolean      secure)
{
   ENTRY;
   g_return_if_fail(CATCH_IS_SESSION(session));
   catch_api_set_secure(session->priv->api, secure);
   EXIT;
}

/**
 * catch_session_get_bearer_token:
 * @session: (in): A #CatchSession.
 *
 * Retrieves the OAuth2 bearer_token used for the Catch.com API.
 *
 * Returns: A string containing the bearer_token, or %NULL.
 */
const gchar *
catch_session_get_bearer_token (CatchSession *session)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_SESSION(session), NULL);
   RETURN(catch_api_get_bearer_token(session->priv->api));
}

/**
 * catch_session_set_bearer_token:
 * @session: (in): A #CatchSession.
 * @bearer_token: (in) (allow-none): The OAuth2 bearer_token, or %NULL.
 *
 * Set the OAuth2 bearer_token to use in the Catch.com API.
 */
void
catch_session_set_bearer_token (CatchSession *session,
                                const gchar  *bearer_token)
{
   ENTRY;
   g_return_if_fail(CATCH_IS_SESSION(session));
   catch_api_set_bearer_token(session->priv->api, bearer_token);
   EXIT;
}

static void
catch_session_get_spaces_cb (GObject      *object,
                              GAsyncResult *result,
                              gpointer      user_data)
{
   GSimpleAsyncResult *simple = user_data;
   CatchResourceGroup *group;
   GomResourceGroup *gom_group;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(object) || CATCH_IS_REPOSITORY(object));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (CATCH_IS_API(object)) {
      if (!(group = catch_api_get_spaces_finish(CATCH_API(object), result, &error))) {
         g_simple_async_result_take_error(simple, error);
         g_simple_async_result_complete_in_idle(simple);
         g_object_unref(simple);
         EXIT;
      }
   } else {
      if (!(gom_group = gom_repository_find_finish(GOM_REPOSITORY(object), result, &error))) {
         g_simple_async_result_take_error(simple, error);
         g_simple_async_result_complete_in_idle(simple);
         g_object_unref(simple);
         EXIT;
      }
      group = g_object_new(CATCH_TYPE_RESOURCE_GROUP,
                           "group", gom_group,
                           NULL);
      g_object_unref(gom_group);
   }

   g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);
}

/**
 * catch_session_get_spaces_async:
 * @session: (in): A #CatchSession.
 * @filter: (in) (allow-none): An optional #GomFilter.
 * @callback: (in): A callback to execute upon completion.
 * @user_data: (in): User data for @callback.
 *
 * Asynchronously requests the available spaces for the session. If the
 * session is using SQLite for storage, the query will be executed there.
 * If no local storage is being used, an HTTP request will be made to the
 * Catch API server.
 */
void
catch_session_get_spaces_async (CatchSession        *session,
                                 GomFilter           *filter,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
   CatchSessionPrivate *priv;
   GSimpleAsyncResult *simple;

   ENTRY;

   g_return_if_fail(CATCH_IS_SESSION(session));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   priv = session->priv;

   simple = g_simple_async_result_new(G_OBJECT(session), callback, user_data,
                                      catch_session_get_spaces_async);

   if (priv->offline) {
      gom_repository_find_async(GOM_REPOSITORY(priv->repository),
                                CATCH_TYPE_SPACE, filter,
                                catch_session_get_spaces_cb, simple);
   } else {
      catch_api_get_spaces_async(priv->api, filter,
                                  catch_session_get_spaces_cb, simple);
   }

   EXIT;
}

/**
 * catch_session_get_spaces_finish:
 * @session: (in): A #CatchSession.
 * @result: (in): A #GAsyncResult.
 * @error: (out) (allow-none): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to fetch a list of spaces.
 *
 * Returns: (transfer full): A #CatchResourceGroup.
 */
CatchResourceGroup *
catch_session_get_spaces_finish (CatchSession  *session,
                                  GAsyncResult  *result,
                                  GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   CatchResourceGroup *group;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SESSION(session), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple), NULL);

   if (!(group = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   }

   RETURN(group ? g_object_ref(group) : NULL);
}

static void
catch_session_get_objects_cb (GObject      *object,
                              GAsyncResult *result,
                              gpointer      user_data)
{
   GSimpleAsyncResult *simple = user_data;
   CatchResourceGroup *group;
   GomResourceGroup *gom_group;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(object) || CATCH_IS_REPOSITORY(object));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (CATCH_IS_API(object)) {
      if (!(group = catch_api_get_objects_finish(CATCH_API(object), result, &error))) {
         g_simple_async_result_take_error(simple, error);
         g_simple_async_result_complete_in_idle(simple);
         g_object_unref(simple);
         EXIT;
      }
   } else {
      if (!(gom_group = gom_repository_find_finish(GOM_REPOSITORY(object), result, &error))) {
         g_simple_async_result_take_error(simple, error);
         g_simple_async_result_complete_in_idle(simple);
         g_object_unref(simple);
         EXIT;
      }
      group = g_object_new(CATCH_TYPE_RESOURCE_GROUP,
                           "group", gom_group,
                           NULL);
      g_object_unref(gom_group);
   }

   g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   ENTRY;
}

/**
 * catch_session_get_objects_async:
 * @session: (in): A #CatchSession.
 * @filter: (in) (allow-none): An optional #GomFilter.
 * @callback: (in): A callback to be executed upon completion.
 * @user_data: (in): User data for @callback.
 *
 * Asynchronously requests the available objects for the session. If the
 * session is using SQLite for storage, the query will be executed there.
 * If no local storage is being used, an HTTP request will be made to the
 * Catch API server.
 */
void
catch_session_get_objects_async (CatchSession        *session,
                                 GomFilter           *filter,
                                 GAsyncReadyCallback  callback,
                                 gpointer             user_data)
{
   CatchSessionPrivate *priv;
   GSimpleAsyncResult *simple;

   ENTRY;

   g_return_if_fail(CATCH_IS_SESSION(session));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   priv = session->priv;

   simple = g_simple_async_result_new(G_OBJECT(session), callback, user_data,
                                      catch_session_get_objects_async);

   if (priv->offline) {
      gom_repository_find_async(GOM_REPOSITORY(priv->repository),
                                CATCH_TYPE_OBJECT, filter,
                                catch_session_get_objects_cb,
                                simple);
   } else {
      catch_api_get_objects_async(priv->api, filter,
                                  catch_session_get_objects_cb,
                                  simple);
   }

   EXIT;
}

/**
 * catch_session_get_objects_finish:
 * @session: (in): A #CatchSession.
 * @result: (in): A #GAsyncResult.
 * @error: (out) (allow-none): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to fetch a list of objects.
 *
 * Returns: (transfer full): A #CatchResourceGroup.
 */
CatchResourceGroup *
catch_session_get_objects_finish (CatchSession  *session,
                                  GAsyncResult  *result,
                                  GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   CatchResourceGroup *ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SESSION(session), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple), NULL);

   if (!(ret = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   }

   RETURN(ret ? g_object_ref(ret) : NULL);
}

static void
catch_session_get_activities_cb (GObject      *object,
                                 GAsyncResult *result,
                                 gpointer      user_data)
{
   GSimpleAsyncResult *simple = user_data;
   CatchResourceGroup *group;
   GomResourceGroup *gom_group;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(object) || CATCH_IS_REPOSITORY(object));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (CATCH_IS_API(object)) {
      if (!(group = catch_api_get_activities_finish(CATCH_API(object), result, &error))) {
         g_simple_async_result_take_error(simple, error);
         g_simple_async_result_complete_in_idle(simple);
         g_object_unref(simple);
         EXIT;
      }
   } else {
      if (!(gom_group = gom_repository_find_finish(GOM_REPOSITORY(object), result, &error))) {
         g_simple_async_result_take_error(simple, error);
         g_simple_async_result_complete_in_idle(simple);
         g_object_unref(simple);
         EXIT;
      }
      group = g_object_new(CATCH_TYPE_RESOURCE_GROUP,
                           "group", gom_group,
                           NULL);
      g_object_unref(gom_group);
   }

   g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

/**
 * catch_session_get_activities_async:
 * @session: (in): A #CatchSession.
 * @filter: (in) (allow-none): An optional #GomFilter.
 * @callback: (in): A callback to execute upon completion.
 * @user_data: (in): User data for @callback.
 *
 * Asynchronously requests the available activities for the session. If the
 * session is using SQLite for storage, the query will be executed there.
 * If no local storage is being used, an HTTP request will be made to the
 * Catch API server.
 */
void
catch_session_get_activities_async (CatchSession        *session,
                                    GomFilter           *filter,
                                    GAsyncReadyCallback  callback,
                                    gpointer             user_data)
{
   CatchSessionPrivate *priv;
   GSimpleAsyncResult *simple;

   ENTRY;

   g_return_if_fail(CATCH_IS_SESSION(session));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   priv = session->priv;

   simple = g_simple_async_result_new(G_OBJECT(session), callback, user_data,
                                      catch_session_get_activities_async);

   if (priv->offline) {
      gom_repository_find_async(GOM_REPOSITORY(priv->repository),
                                CATCH_TYPE_ACTIVITY, filter,
                                catch_session_get_activities_cb, simple);
   } else {
      catch_api_get_activities_async(priv->api, filter,
                                     catch_session_get_activities_cb, simple);
   }

   EXIT;
}

/**
 * catch_session_get_activities_finish:
 * @session: (in): A #CatchSession.
 * @result: (in): A #GAsyncResult.
 * @error: (out) (allow-none): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to fetch a list of activities.
 *
 * Returns: (transfer full): A #CatchResourceGroup.
 */
CatchResourceGroup *
catch_session_get_activities_finish (CatchSession  *session,
                                     GAsyncResult  *result,
                                     GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   CatchResourceGroup *group;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SESSION(session), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple), NULL);

   if (!(group = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   }

   RETURN(group ? g_object_ref(group) : NULL);
}

/**
 * catch_session_sync_async:
 * @session: (in): A #CatchSession.
 * @callback: (in): A callback to be executed upon completion.
 * @user_data: (in): User data for @callback.
 *
 * This function will asynchronously synchronize the local database with
 * the catch.com services. @callback must call catch_session_sync_finish()
 * to complete the operation.
 */
void
catch_session_sync_async (CatchSession        *session,
                          GAsyncReadyCallback  callback,
                          gpointer             user_data)
{
   CatchSessionPrivate *priv;
   CatchTask *task;

   ENTRY;

   g_return_if_fail(CATCH_IS_SESSION(session));
   g_return_if_fail(callback);

   priv = session->priv;

   if (!priv->offline) {
      g_warning("Cannot sync without local database.");
      return;
   }

   task = g_object_new(CATCH_TYPE_TASK_SYNC,
                       "api", priv->api,
                       "callback", callback,
                       "callback-data", user_data,
                       "repository", priv->repository,
                       "source-object", session,
                       NULL);
   catch_task_run(task);

   EXIT;
}

/**
 * catch_session_sync_finish:
 * @session: (in): A #CatchSession.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to catch_session_sync_async().
 * This must be called from the callback provided to
 * catch_session_sync_async().
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
catch_session_sync_finish (CatchSession  *session,
                           GAsyncResult  *result,
                           GError       **error)
{
   CatchTask *task = (CatchTask *)result;
   gboolean ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SESSION(session), FALSE);
   g_return_val_if_fail(CATCH_IS_TASK(task), FALSE);

   if ((ret = catch_task_get_is_error(task))) {
      catch_task_propagate_error(task, error);
   }

   RETURN(!ret);
}

static void
catch_session_adapter_open_cb (GObject      *object,
                               GAsyncResult *result,
                               gpointer      user_data)
{
   GomAdapter *adapter = (GomAdapter *)object;
   GSimpleAsyncResult *simple = user_data;
   CatchSession *session;
   gboolean ret;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(GOM_IS_ADAPTER(adapter));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (!(ret = gom_adapter_open_finish(adapter, result, &error))) {
      g_simple_async_result_take_error(simple, error);
      g_simple_async_result_complete_in_idle(simple);
      g_object_unref(simple);
      g_object_unref(adapter);
      EXIT;
   }

   session = CATCH_SESSION(g_async_result_get_source_object(user_data));
   session->priv->repository = g_object_new(CATCH_TYPE_REPOSITORY,
                                            "adapter", adapter,
                                            NULL);
   g_object_unref(adapter);

   g_simple_async_result_set_op_res_gboolean(simple, TRUE);
   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

/**
 * catch_session_set_data_dir_async:
 * @session: (in): A #CatchSession.
 * @data_dir: (in): The path to the data directory.
 * @callback: (in): A callback to execute upon completion.
 * @user_data: (in): User data for @callback.
 *
 * Sets the directory to use for user storage by @session. This will be
 * loaded asynchronously and the database opened. Upon completion, @callback
 * will be executed and it MUST call catch_session_set_data_dir_finish().
 */
void
catch_session_set_data_dir_async (CatchSession        *session,
                                  const gchar         *data_dir,
                                  GAsyncReadyCallback  callback,
                                  gpointer             user_data)
{
   CatchSessionPrivate *priv;
   GSimpleAsyncResult *simple;
   GomAdapter *adapter;
   gchar *db_path;

   ENTRY;

   g_return_if_fail(CATCH_IS_SESSION(session));
   g_return_if_fail(data_dir != NULL);
   g_return_if_fail(callback != NULL);

   priv = session->priv;

   simple = g_simple_async_result_new(G_OBJECT(session), callback, user_data,
                                      catch_session_set_data_dir_async);

   g_free(priv->data_dir);
   priv->data_dir = g_strdup(data_dir);

   db_path = g_build_filename(data_dir, "catch.db", NULL);
   adapter = g_object_new(GOM_TYPE_ADAPTER, NULL);
   gom_adapter_open_async(adapter, db_path, catch_session_adapter_open_cb,
                          simple);

   g_free(db_path);

   EXIT;
}

/**
 * catch_session_set_data_dir_finish:
 * @session: (in): A #CatchSession.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to set the data directory with
 * catch_session_set_data_dir_async().
 *
 * Returns: %TRUE if successful; otherwise %FALSE and @error is set.
 */
gboolean
catch_session_set_data_dir_finish (CatchSession  *session,
                                   GAsyncResult  *result,
                                   GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   gboolean ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_SESSION(session), FALSE);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple), FALSE);

   if (!(ret = g_simple_async_result_get_op_res_gboolean(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   } else {
      /*
       * Opening the database succeeded, so we can go ahead and run in offline
       * mode, and only fetch from the API when synchronizing.
       */
      session->priv->offline = TRUE;
   }

   RETURN(ret);
}

static void
catch_session_finalize (GObject *object)
{
   CatchSessionPrivate *priv = CATCH_SESSION(object)->priv;

   ENTRY;
   g_clear_object(&priv->api);
   g_clear_object(&priv->repository);
   g_free(priv->data_dir);
   g_free(priv->data_dir);
   G_OBJECT_CLASS(catch_session_parent_class)->finalize(object);
   EXIT;
}

static void
catch_session_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
   CatchSession *session = CATCH_SESSION(object);

   switch (prop_id) {
   case PROP_BEARER_TOKEN:
      g_value_set_string(value, catch_session_get_bearer_token(session));
      break;
   case PROP_PORT:
      g_value_set_uint(value, catch_session_get_port(session));
      break;
   case PROP_SECURE:
      g_value_set_boolean(value, catch_session_get_secure(session));
      break;
   case PROP_SERVER:
      g_value_set_string(value, catch_session_get_server(session));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_session_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
   CatchSession *session = CATCH_SESSION(object);

   switch (prop_id) {
   case PROP_BEARER_TOKEN:
      catch_session_set_bearer_token(session, g_value_get_string(value));
      break;
   case PROP_PORT:
      catch_session_set_port(session, g_value_get_uint(value));
      break;
   case PROP_SECURE:
      catch_session_set_secure(session, g_value_get_boolean(value));
      break;
   case PROP_SERVER:
      catch_session_set_server(session, g_value_get_string(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

static void
catch_session_class_init (CatchSessionClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_session_finalize;
   object_class->get_property = catch_session_get_property;
   object_class->set_property = catch_session_set_property;
   g_type_class_add_private(object_class, sizeof(CatchSessionPrivate));

   /**
    * CatchSession:bearer-token:
    *
    * The "bearer-token" property containing the OAuth2 token.
    */
   gParamSpecs[PROP_BEARER_TOKEN] =
      g_param_spec_string("bearer-token",
                          _("Bearer Token"),
                          _("The OAuth2 bearer token for the API."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_BEARER_TOKEN,
                                   gParamSpecs[PROP_BEARER_TOKEN]);

   /**
    * CatchSession:port:
    *
    * The "port" property containing the port to connect to.
    */
   gParamSpecs[PROP_PORT] =
      g_param_spec_uint("port",
                        _("Port"),
                        _("The port to use when connecting to the API."),
                        0,
                        G_MAXUSHORT,
                        443,
                        G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_PORT,
                                   gParamSpecs[PROP_PORT]);

   /**
    * CatchSession:secure:
    *
    * If SSL should be used. This is always required with the Catch hosted
    * service.
    */
   gParamSpecs[PROP_SECURE] =
      g_param_spec_boolean("secure",
                          _("Secure"),
                          _("Use SSL when connecting to the API."),
                          TRUE,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SECURE,
                                   gParamSpecs[PROP_SECURE]);

   /**
    * CatchSession:server:
    *
    * The "server" property. This is the server that should be connected
    * to. This is always "catch.com" for the Catch hosted service.
    */
   gParamSpecs[PROP_SERVER] =
      g_param_spec_string("server",
                          _("Server"),
                          _("The server to connect to."),
                          "api.catch.com",
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SERVER,
                                   gParamSpecs[PROP_SERVER]);
}

static void
catch_session_init (CatchSession *session)
{
   ENTRY;
   session->priv = G_TYPE_INSTANCE_GET_PRIVATE(session,
                                               CATCH_TYPE_SESSION,
                                               CatchSessionPrivate);
   session->priv->api = catch_api_new(session);
   EXIT;
}

GQuark
catch_session_error_quark (void)
{
   return g_quark_from_static_string("catch-session-error-quark");
}
