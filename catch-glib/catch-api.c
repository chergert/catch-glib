/* catch-api.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <json-glib/json-glib.h>

#include "catch-activity.h"
#include "catch-api.h"
#include "catch-debug.h"
#include "catch-message-builder.h"
#include "catch-object.h"
#include "catch-space.h"

#define catch_api_return_and_complete_with_error(simple, error) \
G_STMT_START {                                                  \
   g_simple_async_result_take_error(simple, error);             \
   g_simple_async_result_complete_in_idle(simple);              \
   g_object_unref(simple);                                      \
   return;                                                      \
} G_STMT_END

#define catch_api_return_and_complete(simple)      \
G_STMT_START {                                     \
   g_simple_async_result_complete_in_idle(simple); \
   g_object_unref(simple);                         \
   return;                                         \
} G_STMT_END

G_DEFINE_TYPE(CatchApi, catch_api, G_TYPE_OBJECT)

struct _CatchApiPrivate
{
   gboolean secure;
   gchar *server;
   guint port;
   gchar *bearer_token;
   CatchSession *session;
};

enum
{
   PROP_0,
   PROP_BEARER_TOKEN,
   PROP_PORT,
   PROP_SECURE,
   PROP_SERVER,
   PROP_SESSION,
   LAST_PROP
};

static GParamSpec *gParamSpecs[LAST_PROP];

CatchApi *
catch_api_new (CatchSession *session)
{
   ENTRY;
   RETURN(g_object_new(CATCH_TYPE_API, "session", session, NULL));
}

static void
catch_api_get_activities_cb (GObject      *object,
                             GAsyncResult *result,
                             gpointer      user_data)
{
   CatchResourceGroup *group = (CatchResourceGroup *)object;
   GSimpleAsyncResult *simple = user_data;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (!catch_resource_group_fetch_finish(group, result, &error)) {
      g_simple_async_result_take_error(simple, error);
      g_simple_async_result_set_op_res_gpointer(simple, NULL, NULL);
   }

   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

void
catch_api_get_activities_async (CatchApi            *api,
                                GomFilter           *filter,
                                GAsyncReadyCallback  callback,
                                gpointer             user_data)
{
   GSimpleAsyncResult *simple;
   CatchResourceGroup *group;
   CatchApiPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(api));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   priv = api->priv;

   group = g_object_new(CATCH_TYPE_RESOURCE_GROUP,
                        "filter", filter,
                        "resource-type", CATCH_TYPE_ACTIVITY,
                        "session", priv->session,
                        NULL);
   simple = g_simple_async_result_new(G_OBJECT(api), callback, user_data,
                                      catch_api_get_activities_async);
   g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   catch_resource_group_fetch_async(group, 0, 0, catch_api_get_activities_cb,
                                    simple);

   EXIT;
}

/**
 * catch_api_get_activities_finish:
 * @api: (in): A #CatchApi.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to catch_api_get_activities_async().
 *
 * Returns: (transfer full): A #CatchResourceGroup if successful; or %NULL.
 */
CatchResourceGroup *
catch_api_get_activities_finish (CatchApi      *api,
                                 GAsyncResult  *result,
                                 GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   CatchResourceGroup *ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(result), NULL);

   if (!(ret = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
      RETURN(NULL);
   }

   ret = g_object_ref(ret);

   RETURN(ret);
}

const gchar *
catch_api_get_server (CatchApi *api)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   RETURN(api->priv->server);
}

void
catch_api_set_server (CatchApi    *api,
                      const gchar *server)
{
   CatchApiPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(api));

   priv = api->priv;

   g_free(priv->server);
   priv->server = g_strdup(server);
   g_object_notify_by_pspec(G_OBJECT(api), gParamSpecs[PROP_SERVER]);

   EXIT;
}

static void
catch_api_get_spaces_cb (GObject      *object,
                         GAsyncResult *result,
                         gpointer      user_data)
{
   CatchResourceGroup *group = (CatchResourceGroup *)object;
   GSimpleAsyncResult *simple = user_data;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (!catch_resource_group_fetch_finish(group, result, &error)) {
      g_simple_async_result_take_error(simple, error);
      g_simple_async_result_set_op_res_gpointer(simple, NULL, NULL);
   }

   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

void
catch_api_get_spaces_async (CatchApi            *api,
                            GomFilter           *filter,
                            GAsyncReadyCallback  callback,
                            gpointer             user_data)
{
   GSimpleAsyncResult *simple;
   CatchResourceGroup *group;
   CatchApiPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(api));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   priv = api->priv;

   group = g_object_new(CATCH_TYPE_RESOURCE_GROUP,
                        "filter", filter,
                        "resource-type", CATCH_TYPE_SPACE,
                        "session", priv->session,
                        NULL);
   simple = g_simple_async_result_new(G_OBJECT(api), callback, user_data,
                                      catch_api_get_spaces_async);
   g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   catch_resource_group_fetch_async(group, 0, 0, catch_api_get_spaces_cb,
                                    simple);

   EXIT;
}

/**
 * catch_api_get_spaces_finish:
 * @api: (in): A #CatchApi.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to catch_api_get_spaces_async().
 *
 * Returns: (transfer full): A #CatchResourceGroup if successful; or %NULL.
 */
CatchResourceGroup *
catch_api_get_spaces_finish (CatchApi      *api,
                             GAsyncResult  *result,
                             GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   CatchResourceGroup *ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(result), NULL);

   if (!(ret = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
      RETURN(NULL);
   }

   RETURN(g_object_ref(ret));
}

static void
catch_api_get_objects_cb (GObject      *object,
                          GAsyncResult *result,
                          gpointer      user_data)
{
   CatchResourceGroup *group = (CatchResourceGroup *)object;
   GSimpleAsyncResult *simple = user_data;
   GError *error = NULL;

   ENTRY;

   g_return_if_fail(CATCH_IS_RESOURCE_GROUP(group));
   g_return_if_fail(G_IS_SIMPLE_ASYNC_RESULT(simple));

   if (!catch_resource_group_fetch_finish(group, result, &error)) {
      g_simple_async_result_take_error(simple, error);
      g_simple_async_result_set_op_res_gpointer(simple, NULL, NULL);
   }

   g_simple_async_result_complete_in_idle(simple);
   g_object_unref(simple);

   EXIT;
}

void
catch_api_get_objects_async (CatchApi            *api,
                             GomFilter           *filter,
                             GAsyncReadyCallback  callback,
                             gpointer             user_data)
{
   GSimpleAsyncResult *simple;
   CatchResourceGroup *group;
   CatchApiPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(api));
   g_return_if_fail(!filter || GOM_IS_FILTER(filter));
   g_return_if_fail(callback != NULL);

   priv = api->priv;

   group = g_object_new(CATCH_TYPE_RESOURCE_GROUP,
                        "filter", filter,
                        "resource-type", CATCH_TYPE_OBJECT,
                        "session", priv->session,
                        NULL);
   simple = g_simple_async_result_new(G_OBJECT(api), callback, user_data,
                                      catch_api_get_objects_async);
   g_simple_async_result_set_op_res_gpointer(simple, group, g_object_unref);
   catch_resource_group_fetch_async(group, 0, 0, catch_api_get_objects_cb,
                                    simple);

   EXIT;
}

/**
 * catch_api_get_objects_finish:
 * @api: (in): A #CatchApi.
 * @result: (in): A #GAsyncResult.
 * @error: (out): A location for a #GError, or %NULL.
 *
 * Completes an asynchronous request to catch_api_get_objects_async().
 *
 * Returns: (transfer full): A #CatchResourceGroup.
 */
CatchResourceGroup *
catch_api_get_objects_finish (CatchApi      *api,
                              GAsyncResult  *result,
                              GError       **error)
{
   GSimpleAsyncResult *simple = (GSimpleAsyncResult *)result;
   CatchResourceGroup *ret;

   ENTRY;

   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   g_return_val_if_fail(G_IS_SIMPLE_ASYNC_RESULT(result), NULL);

   if (!(ret = g_simple_async_result_get_op_res_gpointer(simple))) {
      g_simple_async_result_propagate_error(simple, error);
   }

   RETURN(ret ? g_object_ref(ret) : NULL);
}

const gchar *
catch_api_get_bearer_token (CatchApi *api)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   RETURN(api->priv->bearer_token);
}

void
catch_api_set_bearer_token (CatchApi    *api,
                            const gchar *bearer_token)
{
   ENTRY;

   g_return_if_fail(CATCH_IS_API(api));

   g_free(api->priv->bearer_token);
   api->priv->bearer_token = g_strdup(bearer_token);
   g_object_notify_by_pspec(G_OBJECT(api), gParamSpecs[PROP_BEARER_TOKEN]);

   EXIT;
}

/**
 * catch_api_get_session:
 * @api: (in): A #CatchApi.
 *
 * Retrieves the session used by the API.
 *
 * Returns: (transfer none): A #CatchSession.
 */
CatchSession *
catch_api_get_session (CatchApi *api)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_API(api), NULL);
   RETURN(api->priv->session);
}

static void
catch_api_set_session (CatchApi     *api,
                       CatchSession *session)
{
   CatchApiPrivate *priv;

   ENTRY;

   g_return_if_fail(CATCH_IS_API(api));
   g_return_if_fail(!session || CATCH_IS_SESSION(session));

   priv = api->priv;

   if (priv->session) {
      g_object_remove_weak_pointer(G_OBJECT(api), (gpointer *)&priv->session);
      priv->session = NULL;
   }
   if (session) {
      priv->session = session;
      g_object_add_weak_pointer(G_OBJECT(api), (gpointer *)&priv->session);
   }
   g_object_notify_by_pspec(G_OBJECT(api), gParamSpecs[PROP_SESSION]);
   EXIT;
}

guint
catch_api_get_port (CatchApi *api)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_API(api), 0);
   RETURN(api->priv->port);
}

void
catch_api_set_port (CatchApi *api,
                    guint     port)
{
   ENTRY;
   g_return_if_fail(CATCH_IS_API(api));
   api->priv->port = port;
   g_object_notify_by_pspec(G_OBJECT(api), gParamSpecs[PROP_PORT]);
   EXIT;
}

gboolean
catch_api_get_secure (CatchApi *api)
{
   ENTRY;
   g_return_val_if_fail(CATCH_IS_API(api), FALSE);
   RETURN(api->priv->secure);
}

void
catch_api_set_secure (CatchApi *api,
                      gboolean  secure)
{
   ENTRY;
   g_return_if_fail(CATCH_IS_API(api));
   api->priv->secure = secure;
   g_object_notify_by_pspec(G_OBJECT(api), gParamSpecs[PROP_SECURE]);
   EXIT;
}

/**
 * catch_api_finalize:
 * @object: (in): A #CatchApi.
 *
 * Finalizer for a #CatchApi instance.  Frees any resources held by
 * the instance.
 */
static void
catch_api_finalize (GObject *object)
{
   CatchApiPrivate *priv = CATCH_API(object)->priv;

   ENTRY;
   catch_api_set_session(CATCH_API(object), NULL);
   g_free(priv->bearer_token);
   g_free(priv->server);
   G_OBJECT_CLASS(catch_api_parent_class)->finalize(object);
   EXIT;
}

/**
 * catch_api_get_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (out): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Get a given #GObject property.
 */
static void
catch_api_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
   CatchApi *api = CATCH_API(object);

   switch (prop_id) {
   case PROP_BEARER_TOKEN:
      g_value_set_string(value, catch_api_get_bearer_token(api));
      break;
   case PROP_PORT:
      g_value_set_uint(value, catch_api_get_port(api));
      break;
   case PROP_SECURE:
      g_value_set_boolean(value, catch_api_get_secure(api));
      break;
   case PROP_SERVER:
      g_value_set_string(value, catch_api_get_server(api));
      break;
   case PROP_SESSION:
      g_value_set_object(value, catch_api_get_session(api));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_api_set_property:
 * @object: (in): A #GObject.
 * @prop_id: (in): The property identifier.
 * @value: (in): The given property.
 * @pspec: (in): A #ParamSpec.
 *
 * Set a given #GObject property.
 */
static void
catch_api_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
   CatchApi *api = CATCH_API(object);

   switch (prop_id) {
   case PROP_BEARER_TOKEN:
      catch_api_set_bearer_token(api, g_value_get_string(value));
      break;
   case PROP_PORT:
      catch_api_set_port(api, g_value_get_uint(value));
      break;
   case PROP_SECURE:
      catch_api_set_secure(api, g_value_get_boolean(value));
      break;
   case PROP_SERVER:
      catch_api_set_server(api, g_value_get_string(value));
      break;
   case PROP_SESSION:
      catch_api_set_session(api, g_value_get_object(value));
      break;
   default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
   }
}

/**
 * catch_api_class_init:
 * @klass: (in): A #CatchApiClass.
 *
 * Initializes the #CatchApiClass and prepares the vtable.
 */
static void
catch_api_class_init (CatchApiClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_api_finalize;
   object_class->get_property = catch_api_get_property;
   object_class->set_property = catch_api_set_property;
   g_type_class_add_private(object_class, sizeof(CatchApiPrivate));

   gParamSpecs[PROP_BEARER_TOKEN] =
      g_param_spec_string("bearer-token",
                          _("Bearer Token"),
                          _("The OAuth2 bearer_token to use."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_BEARER_TOKEN,
                                   gParamSpecs[PROP_BEARER_TOKEN]);

   gParamSpecs[PROP_PORT] =
      g_param_spec_uint("port",
                        _("Port"),
                        _("The api.catch.com API port number."),
                        0,
                        G_MAXUSHORT,
                        0,
                        G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_PORT,
                                   gParamSpecs[PROP_PORT]);

   gParamSpecs[PROP_SECURE] =
      g_param_spec_boolean("secure",
                           _("Secure"),
                           _("If SSL should be used for communication."),
                           TRUE,
                           G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SECURE,
                                   gParamSpecs[PROP_SECURE]);

   gParamSpecs[PROP_SERVER] =
      g_param_spec_string("server",
                          _("Server"),
                          _("The api.catch.com API server."),
                          NULL,
                          G_PARAM_READWRITE);
   g_object_class_install_property(object_class, PROP_SERVER,
                                   gParamSpecs[PROP_SERVER]);

   gParamSpecs[PROP_SESSION] =
      g_param_spec_object("session",
                          _("Session"),
                          _("The CatchSession the API works for."),
                          CATCH_TYPE_SESSION,
                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
   g_object_class_install_property(object_class, PROP_SESSION,
                                   gParamSpecs[PROP_SESSION]);
}

/**
 * catch_api_init:
 * @api: (in): A #CatchApi.
 *
 * Initializes the newly created #CatchApi instance.
 */
static void
catch_api_init (CatchApi *api)
{
   ENTRY;
   api->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(api,
                                  CATCH_TYPE_API,
                                  CatchApiPrivate);
   api->priv->server = g_strdup("api.catch.com");
   api->priv->secure = TRUE;
   EXIT;
}

GQuark
catch_api_error_quark (void)
{
   return g_quark_from_static_string("catch_api_error_quark");
}
