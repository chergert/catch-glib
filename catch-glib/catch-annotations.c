/* catch-annotations.c
 *
 * Copyright (C) 2012 Catch.com
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>

#include "catch-annotations.h"

G_DEFINE_TYPE(CatchAnnotations, catch_annotations, G_TYPE_OBJECT)

struct _CatchAnnotationsPrivate
{
   GHashTable *values;
};

static inline void
_g_value_free (gpointer data)
{
   GValue *value = data;

   if (value) {
      g_value_unset(value);
      g_slice_free(GValue, value);
   }
}

static inline GValue *
_g_value_new (void)
{
   return g_slice_new0(GValue);
}

/**
 * catch_annotations_contains:
 * @annotations: (in): A #CatchAnnotations.
 * @key: (in): The annotation key.
 *
 * Checks to see if the annotation named @key exists.
 *
 * Returns: %TRUE if @key was found; otherwise %FALSE.
 */
gboolean
catch_annotations_contains (CatchAnnotations *annotations,
                            const gchar      *key)
{
   g_return_val_if_fail(CATCH_IS_ANNOTATIONS(annotations), FALSE);
   return g_hash_table_contains(annotations->priv->values, key);
}

/**
 * catch_annotations_get_value:
 * @annotations: (in): A #CatchAnnotations.
 * @key: (in): The annotation key.
 * @value: (out): A location for the value.
 *
 * Gets the annotation value for a given key such as "location:speed".
 *
 * Returns: %TRUE if @value is set, otherwise %FALSE.
 */
gboolean
catch_annotations_get_value (CatchAnnotations *annotations,
                             const gchar      *key,
                             GValue           *value)
{
   CatchAnnotationsPrivate *priv;
   const GValue *local_value;

   g_return_val_if_fail(CATCH_IS_ANNOTATIONS(annotations), FALSE);
   g_return_val_if_fail(key, FALSE);
   g_return_val_if_fail(value, FALSE);

   priv = annotations->priv;

   if ((local_value = g_hash_table_lookup(priv->values, key))) {
      g_value_copy(local_value, value);
      return TRUE;
   }

   return FALSE;
}

/**
 * catch_annotations_set_value:
 * @annotations: (in): A #CatchAnnotations.
 * @key: (in): The annotation key.
 * @value: (in) (allow-none): A #GValue.
 *
 * Sets an annotation for @key. If @value is %NULL, then the annotation
 * is removed. An annotation should be in a format similar to
 * "location:speed" or "catch:starred".
 */
void
catch_annotations_set_value (CatchAnnotations *annotations,
                             const gchar      *key,
                             const GValue     *value)
{
   CatchAnnotationsPrivate *priv;
   GValue *local_value;

   g_return_if_fail(CATCH_IS_ANNOTATIONS(annotations));
   g_return_if_fail(key);
   g_return_if_fail(!value || G_VALUE_TYPE(value));

   priv = annotations->priv;

  g_hash_table_remove(priv->values, key);

  if (value) {
     local_value = _g_value_new();
     g_value_copy(value, local_value);
     g_hash_table_insert(priv->values, g_strdup(key), local_value);
  }
}

static void
catch_annotations_finalize (GObject *object)
{
   CatchAnnotationsPrivate *priv;

   priv = CATCH_ANNOTATIONS(object)->priv;

   g_hash_table_unref(priv->values);
   priv->values = NULL;

   G_OBJECT_CLASS(catch_annotations_parent_class)->finalize(object);
}

static void
catch_annotations_class_init (CatchAnnotationsClass *klass)
{
   GObjectClass *object_class;

   object_class = G_OBJECT_CLASS(klass);
   object_class->finalize = catch_annotations_finalize;
   g_type_class_add_private(object_class, sizeof(CatchAnnotationsPrivate));
}

static void
catch_annotations_init (CatchAnnotations *annotations)
{
   annotations->priv =
      G_TYPE_INSTANCE_GET_PRIVATE(annotations,
                                  CATCH_TYPE_ANNOTATIONS,
                                  CatchAnnotationsPrivate);
   annotations->priv->values =
      g_hash_table_new_full(g_str_hash, g_str_equal, g_free, _g_value_free);
}
