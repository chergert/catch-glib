#include <catch-glib/catch-task.h>

#define FLUSH_MAIN                                                  \
   do {                                                             \
      while (g_main_context_pending(g_main_context_default()))      \
         g_main_context_iteration(g_main_context_default(), FALSE); \
   } while (0)

static GMainLoop *gMainLoop;

static void
cb1 (GObject      *object,
     GAsyncResult *result,
     gpointer      user_data)
{
   g_assert(G_IS_OBJECT(object));
   g_assert(CATCH_IS_TASK(result));
   g_assert(user_data);

   *((gboolean *)user_data) = TRUE;
   g_main_loop_quit(gMainLoop);
}

static void
create_tests (void)
{
   CatchTask *task;
   gboolean b = FALSE;
   GObject *o = g_object_new(G_TYPE_OBJECT, NULL);

   task = catch_task_new(o, cb1, &b);
   g_object_add_weak_pointer(G_OBJECT(task), (gpointer *)&task);
   catch_task_run(task);
   g_main_loop_run(gMainLoop);
   g_object_unref(o);
   g_assert_cmpint(b, ==, TRUE);
   g_assert(task == NULL);
}

static void
basic_dep_test_cb1 (CatchTask *task,
                    gboolean  *b)
{
   *b = TRUE;
}

static void
basic_dep_test (void)
{
   CatchTask *task1;
   CatchTask *task2;
   GObject *o = g_object_new(G_TYPE_OBJECT, NULL);
   gboolean b = FALSE;

   task1 = catch_task_new(o, NULL, NULL);
   task2 = catch_task_new(o, NULL, NULL);
   g_signal_connect(task2, "completed", G_CALLBACK(basic_dep_test_cb1), &b);
   catch_task_add_dependency(task2, task1);

   g_object_ref(task2);
   catch_task_run(task2);
   g_assert_cmpint(b, ==, FALSE);
   g_object_unref(task2);

   catch_task_run(task1);
   FLUSH_MAIN;

   g_assert_cmpint(b, ==, TRUE);

   g_object_unref(o);
}

static void
all_of_test_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
   gboolean *b = user_data;
   *b = TRUE;
}

static void
all_of_test (void)
{
   CatchTask *task1;
   CatchTask *task2;
   CatchTask *task3;
   CatchTask *parent;
   GObject *o = g_object_new(G_TYPE_OBJECT, NULL);
   gboolean b = FALSE;

   task1 = catch_task_new(o, NULL, NULL);
   task2 = catch_task_new(o, NULL, NULL);
   task3 = catch_task_new(o, NULL, NULL);
   parent = catch_task_new_all_of(o, all_of_test_cb, &b,
                                  task1, task2, task3,
                                  NULL);

   catch_task_run(parent);
   g_assert_cmpint(b, ==, FALSE);

   catch_task_run(task1);
   g_assert_cmpint(b, ==, FALSE);

   catch_task_run(task2);
   g_assert_cmpint(b, ==, FALSE);

   catch_task_run(task3);
   g_assert_cmpint(b, ==, FALSE);

   FLUSH_MAIN;
   g_assert_cmpint(b, ==, TRUE);

   g_object_unref(o);
}

static void
any_of_test_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
   gboolean *b = user_data;
   *b = !*b;
}

static void
any_of_test (void)
{
   CatchTask *task1;
   CatchTask *task2;
   CatchTask *task3;
   CatchTask *parent;
   GObject *o = g_object_new(G_TYPE_OBJECT, NULL);
   gboolean b = FALSE;

   task1 = catch_task_new(o, NULL, NULL);
   task2 = catch_task_new(o, NULL, NULL);
   task3 = catch_task_new(o, NULL, NULL);
   parent = catch_task_new_any_of(o, any_of_test_cb, &b,
                                  task1, task2, task3,
                                  NULL);

   catch_task_run(parent);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, FALSE);

   catch_task_run(task1);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, TRUE);

   catch_task_run(task2);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, TRUE);

   catch_task_run(task3);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, TRUE);

   g_object_unref(o);
}

static void
n_of_test_cb (GObject      *object,
                GAsyncResult *result,
                gpointer      user_data)
{
   gboolean *b = user_data;
   *b = !*b;
}

static void
n_of_test (void)
{
   CatchTask *task1;
   CatchTask *task2;
   CatchTask *task3;
   CatchTask *parent;
   GObject *o = g_object_new(G_TYPE_OBJECT, NULL);
   gboolean b = FALSE;

   task1 = catch_task_new(o, NULL, NULL);
   task2 = catch_task_new(o, NULL, NULL);
   task3 = catch_task_new(o, NULL, NULL);
   parent = catch_task_new_n_of(o, n_of_test_cb, &b, 2,
                                task1, task2, task3,
                                NULL);

   catch_task_run(parent);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, FALSE);

   catch_task_run(task1);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, FALSE);

   catch_task_run(task2);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, TRUE);

   catch_task_run(task3);
   FLUSH_MAIN;
   g_assert_cmpint(b, ==, TRUE);

   g_object_unref(o);
}

gint
main (gint   argc,
      gchar *argv[])
{
   g_test_init(&argc, &argv, NULL);
   g_type_init();
   gMainLoop = g_main_loop_new(NULL, FALSE);

   g_test_add_func("/Catch/Task/create", create_tests);
   g_test_add_func("/Catch/Task/basic", basic_dep_test);
   g_test_add_func("/Catch/Task/all_of", all_of_test);
   g_test_add_func("/Catch/Task/any_of", any_of_test);
   g_test_add_func("/Catch/Task/n_of", n_of_test);

   return g_test_run();
}
